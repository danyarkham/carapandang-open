import {SET_DATA_HOME} from '../action/ActionHome';

const initialState = {
  data: [],
};

const ReducerHome = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_HOME:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default ReducerHome;
