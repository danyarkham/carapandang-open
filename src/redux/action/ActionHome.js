export const GET_DATA_HOME = 'GET_DATA_HOME';
export const SET_DATA_HOME = 'SET_DATA_HOME';

export const getDataHome = payload => ({type: GET_DATA_HOME, payload});

export const setDataHome = payload => ({type: SET_DATA_HOME, payload});
