import {format} from 'date-fns';
import {Dimensions, Platform} from 'react-native';
import {ASSET_URL} from '../../config/Constant';
import {navigate} from '../../config/Function/Navigate';

export const asset = path => {
  return ASSET_URL + path;
};

export const getUserName = user => {
  if (user) {
    if (user?.name?.split(' ')?.length > 0) {
      return user?.name?.split(' ')[0];
    }
    return user?.name;
  }
  return '';
};

export const getFirstName = user => {
  if (user) {
    const new_user = user.split(' ');
    if (new_user.length > 1) {
      return new_user[0];
    }
    return user;
  }
  return user;
};

export const getLastName = user => {
  if (user) {
    const new_user = user.split(' ');
    console.log('getLastName', new_user);
    if (new_user.length > 1) {
      const lastname = new_user.slice(1);
      return lastname.join(' ');
    }
  }
  return '';
};

export const formatDate = (date, prefix = ' ') => {
  const time = new Date(date).getTime();
  const newDate = new Date(time);
  return format(newDate, `yyyy${prefix}MM${prefix}d`);
  // return `${newDate.getFullYear()}-${
  //   newDate.getMonth() + 1
  // }-${newDate.getDate()}`;
};

export const getFormatDate = (date, type = 'yyyy-MM-d') => {
  const time = new Date(date).getTime();
  const newDate = new Date(time);
  return format(newDate, type);
};

export const formatStringDate = (date, day = false) => {
  if (date) {
    const time = new Date(date).getTime();
    const newDate = new Date(time);

    if (day) {
      return format(newDate, 'E, d MMM yyyy');
    }
    return format(newDate, 'd MMM yyyy');
  }
  return 'Not Set';
};

export const getSchedule = schedules => {
  const days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    "Thursday'at",
    'Friday',
    'Saturday',
  ];
  let schedule = [];
  schedules &&
    schedules.map(item => {
      const newSchedule = `${days[item?._id]} (${item?.start}-${item?.end})`;
      schedule.push(newSchedule);
    });

  if (schedule && schedule.length > 1) {
    return schedule.length > 0 && schedule.join(' & ');
  }
  return schedule && schedule.length > 0 && schedule[0];
};

export const renderSelectData = arrays => {
  const data = [];
  arrays &&
    arrays.length > 0 &&
    arrays.map(item => {
      data.push({
        id: item?.id,
        title: item?.nama,
        description: item?.deskripsi,
      });
    });

  return data;
};

export const getHours = () => {
  const hours = [];

  for (let index = 0; index < 24; index++) {
    hours.push({
      label: index === 0 ? '00' : pad2(index),
      value: index === 0 ? '00' : pad2(index),
    });
  }

  return hours;
};

export const getMinutes = () => {
  const minutes = [];

  for (let index = 0; index < 60; index++) {
    minutes.push({
      label: index === 0 ? '00' : pad2(index),
      value: index === 0 ? '00' : pad2(index),
    });
  }

  return minutes;
};

const pad2 = number => {
  return (number < 10 ? '0' : '') + number;
};

export const getInputSchedule = (length = 0) => {
  const schedules = [];

  for (let index = 0; index < length; index++) {
    schedules.push(index);
  }

  return schedules;
};

export const filterScedule = (data, query) => {
  if (query === 'all') {
    return data;
  }
  return data.filter(item => item?.status?.label == query);
};

export const stringToInt = string => {
  if (typeof string === 'string') {
    return +`${string.split(',').join('')}`;
  }
  return string || 0;
};

export const formatExpiredDate = date => {
  if (date) {
    const time = new Date(date).getTime();
    const newDate = new Date(time);
    return format(newDate, 'E, d MM yyyy | hh:mm') + ' WIB';
  }
  return 'Not Set';
};

export const getDays = day => {
  const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  return days[day];
};

export const filterTransaction = (data, query = 'all', reverse = false) => {
  if (reverse) {
    return data.filter(item => item?.status?.status_code != 4);
  }
  if (query === 'all') {
    return data;
  }
  return data.filter(item => item?.status?.status_code == query);
};

export const filterHistoryClass = (data, query = 'all', reverse = false) => {
  if (reverse) {
    return data?.filter(item => item?.status !== 'skip');
  }
  if (query === 'all') {
    return data?.filter(item => item?.status === 'skip');
  }
  return data?.filter(item => item?.status === 'skip' && item?.filter == query);
};

export const filterNotifications = (data, query) => {
  if (query === 'all') {
    return data;
  }
  return data?.filter(item => item?.type == query);
};

export const numberFormat = number => {
  if (typeof number === 'number') {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    // return number.toLocaleString('id-ID');
  }
  return number || 0;
  // return parseInt(number).toLocaleString() || 0;
};

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const {height, width} = Dimensions.get('window');
export const isIPhoneX = () =>
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? (width === X_WIDTH && height === X_HEIGHT) ||
      (width === XSMAX_WIDTH && height === XSMAX_HEIGHT)
    : false;

export const validateEmail = email => {
  return new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[com]/g).test(email);
};

export const getNavigationRoute = (link, dispatch = null, params = {}) => {
  if (!link) {
    return navigate('NotificationDetail', params);
  }
  return navigate(link, params);
};

export const filterNotificationsStatus = (data, query) => {
  return data?.filter(item => item?.read == query);
};
export const getCourseStatus = (data, query, count = false) => {
  const course = data?.filter(item => item?.status == query);
  if (count) {
    return course?.length;
  }
  return count;
};

export const getDescription = (string, number = 100) => {
  if (!string) {
    return '';
  }
  if (string) {
    if (string.length < number) {
      return string.substr(0, number);
    }
    return string.substr(0, number);
  }
  return null;
};

export const filterResources = (data, query) => {
  if (query === 'all') {
    return data;
  }
  return data?.filter(item => item?.filter == query);
};

export const getFilterSchedule = (data, query) => {
  if (query) {
    return data?.filter(
      item => item?.instructor_schedule?.schedule_date == query,
    );
  }
  return data;
};

export const getCurrentDate = () => {
  const time = new Date().getTime();
  const newDate = new Date(time);

  return format(newDate, 'yyyy-MM-dd');
};

export const filterStatusClass = (datas, query = 'all', skip = false) => {
  if (!datas) {
    return [];
  }
  if (query === 'all') {
    return datas;
  }

  if (skip) {
    return datas.filter(data => data.status !== query);
  }
  return datas.filter(data => data.status === query);
};

export const renderUpcomingData = datas => {
  const newData = [];
  datas &&
    datas.map(data => {
      console.log(data);
      if (data?.type === 'Schedule Request' || data?.type === 'Group Class') {
        data?.group_item &&
          data?.group_item?.courses.map(course => {
            if (course?.status === 'upcoming') {
              newData.push({
                id: course?.id,
                language: data?.group_item?.language,
                instructor: course?.instructor,
                schedule_date: formatStringDate(course?.schedule_date, true),
                schedule: course?.schedule,
                type: data?.type,
                level: data?.group_item?.level_category?.nama,
                item: data,
                course,
              });
            }
          });
      }
    });

  return newData;
};

export const sortByDate = (datas = [], type = 'ASC') => {
  if (!datas) {
    return [];
  }
  return datas.sort(function (a, b) {
    if (type === 'ASC') {
      return (
        new Date(`${a.schedule_date} ${a.schedule?.end}`) -
        new Date(`${b.schedule_date} ${b.schedule?.end}`)
      );
    }
    return (
      new Date(`${b.schedule_date} ${b.schedule?.end}`) -
      new Date(`${a.schedule_date} ${a.schedule?.end}`)
    );
  });
};
