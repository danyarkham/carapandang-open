import {PixelRatio} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export const scaleFont = (fontSize = 12) => {
  return PixelRatio.getFontScale() * fontSize;
};

export const scaleHeight = (height = 0) => {
  return hp(`${height}%`);
};

export const scaleWidth = (width = 0) => {
  return wp(`${width}%`);
};
