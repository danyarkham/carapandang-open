import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import ArrowBackIcon from '../assets/Icon/ArrowBack.svg';
import Poppins from './Poppins';

export default function Header({
  title = 'Header title',
  back = false,
  backPress,
  navigation,
  rightIcon,
  rightPress,
  backgroundColor = null,
  children,
}) {
  return (
    <>
      <View
        style={{
          backgroundColor: backgroundColor,
          height: heightPercentageToDP(10),
          width: '100%',
          paddingHorizontal: widthPercentageToDP(6),
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        {back ? (
          <TouchableOpacity onPress={backPress}>
            <ArrowBackIcon />
          </TouchableOpacity>
        ) : null}
        <Poppins size={24}>{title}</Poppins>
        <TouchableOpacity onPress={rightPress}>{rightIcon}</TouchableOpacity>
      </View>
      <View style={{backgroundColor: backgroundColor, width: '100%'}}>
        {children}
      </View>
    </>
  );
}

const styles = StyleSheet.create({});
