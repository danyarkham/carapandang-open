import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Bookmark from '../screen/Bookmark/Bookmark';
import Home from '../screen/Home/Home';
import Notification from '../screen/Notification/Notification';
import Profile from '../screen/Profile/Profile';

const Tab = createBottomTabNavigator();

export default function BottomTab() {
  return (
    <>
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={({route, navigation}) => ({
          tabBarStyle: {
            height: heightPercentageToDP(8),
            paddingTop: heightPercentageToDP(1),
          },
          tabBarLabel: '',
        })}>
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            headerShown: false,
            tabBarIcon: ({color, size, focused}) => (
              <Entypo name={'home'} color={color} size={size / 1.2} />
            ),
          }}
        />

        <Tab.Screen
          name="Bookmark"
          component={Bookmark}
          options={{
            headerShown: false,
            tabBarIcon: ({color, size, focused}) =>
              focused ? (
                <MaterialCommunityIcons
                  name="bookmark"
                  color={color}
                  size={size / 1.2}
                />
              ) : (
                <MaterialCommunityIcons
                  name="bookmark-outline"
                  color={color}
                  size={size / 1.2}
                />
              ),
          }}
        />

        <Tab.Screen
          name="Notification"
          component={Notification}
          options={{
            headerShown: false,
            tabBarIcon: ({color, size, focused}) =>
              focused ? (
                <Ionicons
                  name="notifications"
                  color={color}
                  size={size / 1.2}
                />
              ) : (
                <Ionicons
                  name="notifications-outline"
                  color={color}
                  size={size / 1.2}
                />
              ),
          }}
        />

        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{
            headerShown: false,
            tabBarIcon: ({color, size, focused}) =>
              focused ? (
                <Ionicons name="person" color={color} size={size / 1.2} />
              ) : (
                <Ionicons
                  name="person-outline"
                  color={color}
                  size={size / 1.2}
                />
              ),
          }}
        />
      </Tab.Navigator>
    </>
  );
}
