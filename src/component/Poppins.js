import React from 'react';
import {Appearance, StyleSheet, Text} from 'react-native';
import {scaleFont} from '../Utils/Helpers/design';

const scheme = Appearance.getColorScheme();

export default function Poppins({
  children,
  type = 'Regular',
  color = scheme === 'dark' ? '#4E4E4E' : '#4E4E4E',
  // color = '#4E4E4E',
  size = 14,
  textAlign,
  textDecorationLine,
  onPress,
  numberOfLines,
  lineHeight,
  style,
}) {
  const styles = StyleSheet.create({
    textStyleRegular: {
      fontFamily: `Poppins-${type}`,
      color,
      fontSize: scaleFont(size),
      textAlign,
      textDecorationLine: textDecorationLine,
      lineHeight: lineHeight,
      ...style,
    },
  });

  return (
    <Text
      onPress={onPress}
      style={styles.textStyleRegular}
      numberOfLines={numberOfLines}>
      {children}
    </Text>
  );
}
