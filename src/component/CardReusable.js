import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Poppins from './Poppins';
import Square from './Square';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const NotificationCard = ({
  isRead = true,
  category = 'News',
  onPress,
  title = 'Notification Title',
  message = 'Quis amet sint laboris est culpa in magna esse do ullamco qui. Reprehenderit id elit occaecat est adipisicing in ullamco anim ad ad dolor excepteur excepteur commodo. Duis qui dolor labore nisi ut consectetur et consectetur amet adipisicing. Quis eiusmod aliquip et officia nulla aute. Aliqua ad commodo Lorem aliquip labore commodo id sunt. Qui commodo aliqua aliqua exercitation id exercitation ipsum Lorem.',
}) => (
  <TouchableOpacity
    activeOpacity={0.7}
    onPress={onPress}
    style={{
      backgroundColor: isRead ? 'white' : 'red',
      paddingVertical: hp(2),
      paddingHorizontal: wp(6),
      marginBottom: hp(2),
    }}>
    <Poppins color={isRead ? 'black' : 'white'}>
      {category} - {title}
    </Poppins>
    <Poppins
      color={isRead ? 'black' : 'white'}
      size={12}
      numberOfLines={3}
      textAlign="justify">
      {message}
    </Poppins>
  </TouchableOpacity>
);

export const NewsCard = ({
  isRead = true,
  category = 'News',
  onPress,
  isBookmark = true,
  onBookmarkPress,
  title = 'Unggahan Terakhir Vanessa Angel dan Suami Sebelum Tewas karena Kecelakaan di Tol Nganjuk',
  time = '15 Minutes ago',
  message = 'Quis amet sint laboris est culpa in magna esse do ullamco qui. Reprehenderit id elit occaecat est adipisicing in ullamco anim ad ad dolor excepteur excepteur commodo. Duis qui dolor labore nisi ut consectetur et consectetur amet adipisicing. Quis eiusmod aliquip et officia nulla aute. Aliqua ad commodo Lorem aliquip labore commodo id sunt. Qui commodo aliqua aliqua exercitation id exercitation ipsum Lorem.',
}) => (
  <TouchableOpacity
    activeOpacity={0.7}
    onPress={onPress}
    style={{
      flex: 1,
      backgroundColor: 'white',
      paddingHorizontal: wp(5),
      paddingVertical: hp(2),
      flexDirection: 'row',
      borderRadius: wp(3),
      marginBottom: hp(2),
      // alignItems: 'center',
    }}>
    <Square
      size={80}
      url="https://cdn0-production-images-kly.akamaized.net/F7OloiKYRdnRQfpbf1_LxGfLqt4=/1280x720/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3623385/original/086933000_1636041057-Vanessa_Angel_dan_Bibi_Ardiansyah_1.JPG"
    />
    <View
      style={{
        flex: 1,
        marginLeft: hp(2),
        justifyContent: 'space-between',
      }}>
      <Poppins size={12} numberOfLines={2} type="Medium">
        {title}
      </Poppins>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <Poppins size={10}>{time}</Poppins>
        <TouchableOpacity onPress={onBookmarkPress}>
          {isBookmark ? (
            <MaterialCommunityIcons name="bookmark" size={20} color="red" />
          ) : (
            <MaterialCommunityIcons
              name="bookmark-outline"
              size={20}
              color="red"
            />
          )}
        </TouchableOpacity>
      </View>
    </View>
  </TouchableOpacity>
);
