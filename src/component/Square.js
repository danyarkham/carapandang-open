import React from 'react';
import FastImage from 'react-native-fast-image';
import {moderateScale} from 'react-native-size-matters';

const Square = ({
  url = null,
  size = 33,
  style,
  resizeMode = FastImage.resizeMode.cover,
}) => {
  let profile = require('../assets/default.png');
  console.log(url);
  if (url) {
    profile = {
      uri: url,
      headers: {Authorization: 'someAuthToken'},
      priority: FastImage.priority.normal,
    };
  }
  return (
    <FastImage
      style={[
        style,
        {
          width: moderateScale(size),
          height: moderateScale(size),
          borderRadius: moderateScale(size / 5),
          // backgroundColor: 'red',
        },
      ]}
      source={profile}
      resizeMode={resizeMode}
    />
  );
};

export default Square;
