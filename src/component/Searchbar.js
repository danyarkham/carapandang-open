import React from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import SearchIcon from '../assets/Icon/search.svg';
import Flag from '../component/Flag';

export default function Searchbar() {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: wp(4),
        paddingRight: wp(6),
        backgroundColor: 'white',
        borderWidth: 2,
        borderRadius: moderateScale(10),
        borderColor: '#FAFAFA',
      }}>
      <Image
        style={{
          width: moderateScale(50),
          height: moderateScale(46),
          marginRight: wp(3),
        }}
        source={require('../assets/Images/logo.png')}
        resizeMode="contain"
      />

      <SearchIcon />
      <TextInput style={{marginLeft: hp(1)}} placeholder="Search here..." />
    </View>
  );
}

const styles = StyleSheet.create({});
