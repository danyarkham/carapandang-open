import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Platform, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import linking from '../../../linking';
import FilterIcon from '../../Assets/Icon/filter.svg';
import BottomTab from '../../Component/BottomTab';
import Loading from '../../Component/Loading';
import Poppins from '../../Component/Poppins';
import AboutLinquis from '../../Screen/AboutLinquis/AboutLinquis';
import PrivacyPolicy from '../../Screen/AboutLinquis/PrivacyPolicy';
import TermOfService from '../../Screen/AboutLinquis/TermOfService';
import BannerScreen from '../../Screen/BannerScreen/BannerScreen';
import BuyCredit from '../../Screen/BuyCredit/BuyCredit';
import BuyCreditDetail from '../../Screen/BuyCreditDetail/BuyCreditDetail';
import Cart from '../../Screen/Cart/Cart';
import ChangeEmail from '../../Screen/ChangeEmail/ChangeEmail';
import ChangeFullname from '../../Screen/ChangeFullname/ChangeFullname';
import ChangePassword from '../../Screen/ChangePassword/ChangePassword';
import ChangePhoneNumber from '../../Screen/ChangePhoneNumber/ChangePhoneNumber';
import ClassDetail from '../../Screen/ClassDetail/ClassDetail';
import ClassDetailPrivate from '../../Screen/ClassDetail/ClassDetailPrivate';
import DetailGroup from '../../Screen/DetailGroup/DetailGroup';
import Download from '../../Screen/Download/Download';
import EditProfile from '../../Screen/EditProfile/EditProfile';
import Group from '../../Screen/Group/Group';
import GroupDetail from '../../Screen/GroupDetail/GroupDetail';
import History from '../../Screen/History/History';
import HistoryPrivate from '../../Screen/History/HistoryPrivate';
import HistoryDetail from '../../Screen/HistoryDetail/HistoryDetail';
import HistoryDetailPrivate from '../../Screen/HistoryDetail/HistoryDetailPrivate';
import Homework from '../../Screen/Homework/Homework';
import HomeworkPrivate from '../../Screen/Homework/HomeWorkPrivate';
import HomeworkUpload from '../../Screen/HomeworkUpload/HomeworkUpload';
import HomeworkUploadPrivate from '../../Screen/HomeworkUpload/HomeworkUploadPrivate';
import HowToPay from '../../Screen/HowToPay/HowToPay';
import HowToPayCredit from '../../Screen/HowToPay/HowToPayCredit';
import Interest from '../../Screen/Interest/Interest';
import InvitationLink from '../../Screen/InvitationLink/InvitationLink';
import InvitationLinkPrivate from '../../Screen/InvitationLink/InvitationLinkPrivate';
import ChooseLanguage from '../../Screen/Language/ChooseLanguage';
import LiveChat from '../../Screen/LiveChat/LiveChat';
import Login from '../../Screen/Login/Login';
import InputParticipant from '../../Screen/Modal/InputParticipant';
import MyCashback from '../../Screen/MyCashback/MyCashback';
import MyPrivateCredit from '../../Screen/MyPrivateCredit/MyPrivateCredit';
import MyPrivateCreditDetail from '../../Screen/MyPrivateCreditDetail/MyPrivateCreditDetail';
import Notification from '../../Screen/Notification/Notification';
import NotificationDetail from '../../Screen/NotificationDetail/NotificationDetail';
import NotificationTransaction from '../../Screen/NotificationTransaction/NotificationTransaction';
import NotificationTransactionCredit from '../../Screen/NotificationTransaction/NotificationTransactionCredit';
import PaymentMethod from '../../Screen/PaymentMethod/PaymentMethod';
import PaymentMethodDetail from '../../Screen/PaymentMethodDetail/PaymentMethodDetail';
import PaymentMethodDetailCredit from '../../Screen/PaymentMethodDetail/PaymentMethodDetailCredit';
import PlacementTest from '../../Screen/PlacementTest/PlacementTest';
import PlacementTestProcess from '../../Screen/PlacementTest/PlacementTestProcess';
import InstructorSchedule from '../../Screen/Private/InstructorSchedule';
import Private from '../../Screen/Private/Private';
import PrivateClass from '../../Screen/PrivateClass/PrivateClass';
import PrivateTeacherProfile from '../../Screen/PrivateTeacherProfile/PrivateTeacherProfile';
import PurchaseDetail from '../../Screen/PurchaseDetail/PurchaseDetail';
import RatingTeacher from '../../Screen/RatingTeacher/RatingTeacher';
import ReferAndEarn from '../../Screen/ReferAndEarn/ReferAndEarn';
import ReportNCertificate from '../../Screen/ReportNCertificate/ReportNCertificate';
import Resources from '../../Screen/Resources/Resources';
import ScheduleRequest from '../../Screen/ScheduleRequest/ScheduleRequest';
import SelectSchedule from '../../Screen/ScheduleRequest/SelectSchedule';
import SelfStudy from '../../Screen/SelfStudy/SelfStudy';
import SignUp2 from '../../Screen/SignUp/SignUp2';
import TempScreen from '../../Screen/TempScreen';
import TermsAndConditions from '../../Screen/TermsAndConditions/TermsAndConditions';
import Transactions from '../../Screen/Transactions/Transactions';
import UploadTransactionCredit from '../../Screen/UploadTransactions/UploadTransactionCredit';
import UploadTransactions from '../../Screen/UploadTransactions/UploadTransactions';
import WhiteBoard from '../../Screen/WhiteBoard/WhiteBoard';
import Wishlist from '../../Screen/Wishlist/Wishlist';
import {NavigationRef} from '../Function/Navigate';
import {toggleFilterGroupClass} from '../Redux/actions/groupItemAction';

export default function PrivateRoute({firstLogin = false}) {
  const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: '#E2494B',
      accent: '#3E3E3E',
      background: '#FAFAFA',
      surface: '#3E3E3E',
      onSurface: '#E0F2E5',
    },
  };

  const dispatch = useDispatch();
  const LoadingAccount = useSelector(state => state.ReducerLogin.isLoading);

  const Stack = createStackNavigator();
  if (firstLogin) {
    return (
      <NavigationContainer linking={linking} theme={theme} ref={NavigationRef}>
        <Stack.Navigator initialRouteName={'Language'}>
          <Stack.Screen
            name="Language"
            component={ChooseLanguage}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Interest"
            component={Interest}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="SignUpDetail"
            component={SignUp2}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Home"
            component={BottomTab}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
  return (
    <NavigationContainer linking={linking} theme={theme} ref={NavigationRef}>
      <Stack.Navigator initialRouteName={'Home'}>
        {LoadingAccount ? (
          <Stack.Screen
            name="Loading"
            component={Loading}
            options={{headerShown: false}}
          />
        ) : (
          <>
            <Stack.Screen
              name="Home"
              component={BottomTab}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Login"
              component={Login}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ChooseLanguage"
              component={ChooseLanguage}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="EditProfile"
              component={EditProfile}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ChangeFullname"
              component={ChangeFullname}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ChangeEmail"
              component={ChangeEmail}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ChangePhoneNumber"
              component={ChangePhoneNumber}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ChangePassword"
              component={ChangePassword}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="AboutLinquis"
              component={AboutLinquis}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="MyCashback"
              component={MyCashback}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="TermsAndConditions"
              component={TermsAndConditions}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReferAndEarn"
              component={ReferAndEarn}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="MyPrivateCredit"
              component={MyPrivateCredit}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Transactions"
              component={Transactions}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="MyPrivateCreditDetail"
              component={MyPrivateCreditDetail}
              options={{headerShown: false}}
            />

            {/* temp screen */}
            <Stack.Screen
              name="TempScreen"
              component={TempScreen}
              // options={{headerShown: false}}
            />
            <Stack.Screen
              name="SelfStudy"
              component={SelfStudy}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="PlacementTest"
              component={PlacementTest}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Group"
              component={Group}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#E2494B'},
                headerBackTitle: 'Group Detail',
                headerTintColor: '#fff',
                headerTitle: 'Group',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#fff"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerRight: props => (
                  <TouchableOpacity
                    onPress={() => dispatch(toggleFilterGroupClass())}
                    activeOpacity={0.7}
                    style={{paddingRight: wp(6)}}>
                    <FilterIcon />
                  </TouchableOpacity>
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#fff',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="GroupDetail"
              component={GroupDetail}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#E2494B'},
                headerBackTitle: 'Group Detail',
                headerTintColor: '#fff',
                headerTitle: 'Detail Group Class',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#fff"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#fff',
                  fontWeight: 'bold',
                },
              })}
            />

            {/* Start Schedule Request */}
            <Stack.Screen
              name="ScheduleRequest"
              component={ScheduleRequest}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="SelectSchedule"
              component={SelectSchedule}
              options={{headerShown: false}}
            />
            {/* End Schedule Request */}
            <Stack.Screen
              name="PurchaseDetail"
              component={PurchaseDetail}
              options={({navigation, route}) => ({
                headerShown: Platform.OS === 'ios',
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Purchase Detail',
                headerTintColor: '#000',
                headerTitle: 'Purchase Detail',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="PaymentMethod"
              component={PaymentMethod}
              options={({navigation, route}) => ({
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Select Payment Method',
                headerTintColor: '#000',
                headerTitle: 'Select Payment Methods',
                headerLeft: props => (
                  <Ionicons
                    name="close"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
                presentation: 'modal',
              })}
            />
            <Stack.Screen
              name="InputParticipant"
              component={InputParticipant}
              options={({navigation, route}) => ({
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Add Participants Email',
                headerTintColor: '#000',
                headerTitle: 'Add Participants Email',
                headerLeft: props => (
                  <Ionicons
                    name="close"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
                presentation: 'modal',
              })}
            />
            <Stack.Screen
              name="UploadTransactions"
              component={UploadTransactions}
              options={({navigation, route}) => ({
                headerShown: Platform.OS === 'ios',
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Upload',
                headerTintColor: '#000',
                headerTitle: 'Upload',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="PaymentMethodDetail"
              component={PaymentMethodDetail}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Payment',
                headerTintColor: '#000',
                headerTitle: 'Payment',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => {
                      if (route?.params?.transaction) {
                        return navigation.replace('Transactions');
                      }
                      return navigation.replace('ScheduleRequest', {
                        screen: 'Request List',
                      });
                    }}
                  />
                ),
                headerRight: props => (
                  <Poppins
                    type={'Medium'}
                    color={'#E2494B'}
                    style={{paddingRight: wp(6)}}
                    onPress={() =>
                      navigation.navigate('UploadTransactions', {
                        ...route?.params,
                      })
                    }>
                    Upload
                  </Poppins>
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="InstructorSchedule"
              component={InstructorSchedule}
              options={({navigation, route}) => ({
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Select Schedule',
                headerTintColor: '#333',
                headerTitle: 'Select Schedule',
                headerLeft: props => (
                  <Ionicons
                    name="close"
                    style={{paddingLeft: wp(3)}}
                    color={'#333'}
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#333',
                  fontWeight: 'bold',
                },
                presentation: 'modal',
              })}
            />
            <Stack.Screen
              name="PaymentMethodDetailCredit"
              component={PaymentMethodDetailCredit}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Payment',
                headerTintColor: '#000',
                headerTitle: 'Payment',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => {
                      if (route?.params?.transaction) {
                        return navigation.replace('Transactions');
                      }
                      return navigation.replace('ScheduleRequest', {
                        screen: 'Request List',
                      });
                    }}
                  />
                ),
                headerRight: props => (
                  <Poppins
                    type={'Medium'}
                    color={'#E2494B'}
                    style={{paddingRight: wp(6)}}
                    onPress={() =>
                      navigation.navigate('UploadTransactions', {
                        ...route?.params,
                      })
                    }>
                    Upload
                  </Poppins>
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="HowToPay"
              component={HowToPay}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: route?.params.item?.payment_method,
                headerTintColor: '#000',
                headerTitle: route?.params.item?.payment_method,
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="PlacementTestProcess"
              component={PlacementTestProcess}
              options={({navigation, route}) => ({
                headerShown: false,
                headerStyle: {backgroundColor: '#fff'},
                headerBackTitle: 'Placement Test',
                headerTintColor: '#000',
                headerTitle: 'Placement Test',
                headerLeft: props => (
                  <Ionicons
                    name="arrow-back-outline"
                    style={{paddingLeft: wp(6)}}
                    color="#000"
                    size={hp(3)}
                    onPress={() => navigation.goBack()}
                  />
                ),
                headerTitleStyle: {
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontWeight: 'bold',
                },
              })}
            />
            <Stack.Screen
              name="PrivateClass"
              component={PrivateClass}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="DetailGroup"
              component={DetailGroup}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Notification"
              component={Notification}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="NotificationDetail"
              component={NotificationDetail}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Cart"
              component={Cart}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Resources"
              component={Resources}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="History"
              component={History}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HistoryPrivate"
              component={HistoryPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HistoryDetail"
              component={HistoryDetail}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HistoryDetailPrivate"
              component={HistoryDetailPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReportNCertificate"
              component={ReportNCertificate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Download"
              component={Download}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="InvitationLink"
              component={InvitationLink}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="InvitationLinkPrivate"
              component={InvitationLinkPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Homework"
              component={Homework}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HomeworkPrivate"
              component={HomeworkPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ClassDetail"
              component={ClassDetail}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ClassDetailPrivate"
              component={ClassDetailPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="NotificationTransaction"
              component={NotificationTransaction}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="NotificationTransactionCredit"
              component={NotificationTransactionCredit}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HomeworkUpload"
              component={HomeworkUpload}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HomeworkUploadPrivate"
              component={HomeworkUploadPrivate}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="WhiteBoard"
              component={WhiteBoard}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Private"
              component={Private}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="BuyCredit"
              component={BuyCredit}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="BuyCreditDetail"
              component={BuyCreditDetail}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="PrivateTeacherProfile"
              component={PrivateTeacherProfile}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="RatingTeacher"
              component={RatingTeacher}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="UploadTransactionCredit"
              component={UploadTransactionCredit}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="HowToPayCredit"
              component={HowToPayCredit}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Wishlist"
              component={Wishlist}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="LiveChat"
              component={LiveChat}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="BannerScreen"
              component={BannerScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="TermOfService"
              component={TermOfService}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="PrivacyPolicy"
              component={PrivacyPolicy}
              options={{headerShown: false}}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
