import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {Appearance} from 'react-native';
import linking from '../../../linking';
import ForgotPassword from '../../Screen/ForgotPassword/ForgotPassword';
import Login from '../../Screen/Login/Login';
import Opening from '../../Screen/Opening/Opening';
import SignUp from '../../Screen/SignUp/SignUp';
import SignUp2 from '../../Screen/SignUp/SignUp2';
import {NavigationRef} from '../Function/Navigate';

export default function PublicRoute({initialRouteName}) {
  const scheme = Appearance.getColorScheme();

  const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: '#E2494B',
      accent: '#3E3E3E',
      background: '#FAFAFA',
      surface: '#3E3E3E',
      onSurface: '#E0F2E5',
    },
  };
  const darkTheme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
      ...DefaultTheme.colors,
      primary: '#E2494B',
      accent: '#3E3E3E',
      background: 'rgb(55,57,58)',
      text: '#FAFAFA',
      surface: '#3E3E3E',
      onSurface: '#E0F2E5',
    },
  };
  const Stack = createStackNavigator();

  return (
    <NavigationContainer
      linking={linking}
      theme={scheme === 'light' ? theme : theme}
      // theme={theme}
      ref={NavigationRef}>
      <Stack.Navigator initialRouteName={'Login'}>
        <Stack.Screen
          name="Opening"
          component={Opening}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SignUp2"
          component={SignUp2}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
