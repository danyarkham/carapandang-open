import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useEffect, useState} from 'react';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import {useDispatch, useSelector} from 'react-redux';
import Opening from '../../Screen/Opening/Opening';
import SplashScreen from 'react-native-splash-screen';
import {setHeaderToken} from '../Axios/setHeaderToken';
import {setUserLogin} from '../../Screen/Login/actionLogin';

const Routes = () => {
  const dispatch = useDispatch();
  const {ReducerLogin} = useSelector(state => state);
  const accessToken = ReducerLogin.token;
  const boarding = useSelector(state => state.ReducerLogin.boarding);
  const {isFirstLogin} = useSelector(state => state.ReducerSignUp);
  const [token, setToken] = useState(accessToken);
  const [user, setUser] = useState({});
  const [firstLogin, setFirstLogin] = useState(false);
  const [onBoarding, setOnBoarding] = useState(boarding);

  const getToken = async access_token => {
    if (!access_token) {
      const new_token = await AsyncStorage.getItem('token');
      setHeaderToken(new_token);
      getFirstLogin();
      return setToken(new_token);
    }
    setHeaderToken(access_token);
    getFirstLogin();
    return setToken(access_token || accessToken);
  };

  const getUser = async user_data => {
    if (!user_data) {
      const data_user = await AsyncStorage.getItem('user');
      dispatch(setUserLogin(JSON.parse(data_user)));
      return setUser(JSON.parse(data_user));
    }
    dispatch(setUserLogin(user_data));
    return setUser(user_data);
  };

  const getFirstLogin = async first_login => {
    const firstLogin = await AsyncStorage.getItem('first_login');
    return setFirstLogin(firstLogin < 1);
  };

  const getBoardingView = async isHasBoarding => {
    if (!isHasBoarding) {
      const getBoardingValue = await AsyncStorage.getItem('isHasBoarding');
      setTimeout(() => {
        SplashScreen.hide();
      }, 2000);
      return setOnBoarding(getBoardingValue > 0);
    }
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000);

    return setOnBoarding(isHasBoarding);
  };

  useEffect(() => {
    getToken(accessToken);
    getBoardingView(onBoarding);
    if (!ReducerLogin.user) {
      getUser(ReducerLogin.user);
    }
  }, [accessToken, ReducerLogin.logout, isFirstLogin, firstLogin, boarding]);

  console.log('isFirstLogin', firstLogin);
  if (token) {
    return <PrivateRoute firstLogin={firstLogin} />;
  }

  if (!onBoarding) {
    return <Opening />;
  }
  return <PublicRoute />;
};

export default Routes;
