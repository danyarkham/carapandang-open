import createSagaMiddleware from 'redux-saga';
import {createStore, applyMiddleware} from 'redux';
import {allReducer} from './allReducer';
import {SagaWatcher} from './SagaWatcher';
import logger from 'redux-logger';
const Saga = createSagaMiddleware();
const Store = createStore(allReducer, applyMiddleware(Saga, logger));

Saga.run(SagaWatcher);
export default Store;
