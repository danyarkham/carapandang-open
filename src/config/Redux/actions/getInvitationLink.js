import {
  GET_INVITATION_LINK,
  GET_INVITATION_LINK_SUCCESS,
  GET_INVITATION_LINK_ERROR,
  GET_INVITATION_LINK_LOADING,
  GET_INVITATION_LINK_UPDATE,
} from '../../Constant';

export const getInvitationLink = payload => {
  return {
    type: GET_INVITATION_LINK,
    payload,
  };
};

export const getInvitationLinkSuccess = payload => {
  return {
    type: GET_INVITATION_LINK_SUCCESS,
    payload,
  };
};

export const getInvitationLinkError = payload => {
  return {
    type: GET_INVITATION_LINK_ERROR,
    payload,
  };
};

export const getInvitationLinkLoading = payload => {
  return {
    type: GET_INVITATION_LINK_LOADING,
    payload,
  };
};

export const getInvitationLinkUpdate = payload => {
  return {
    type: GET_INVITATION_LINK_UPDATE,
    payload,
  };
};
