import {
  UPDATE_PROFILE,
  UPDATE_PROFILE_ERROR,
  UPDATE_PROFILE_LOADING,
  UPDATE_PROFILE_SUCCESS,
} from '../../Constant';

export const updateProfile = payload => {
  return {
    type: UPDATE_PROFILE,
    payload,
  };
};

export const updateProfileSuccess = payload => {
  return {
    type: UPDATE_PROFILE_SUCCESS,
    payload,
  };
};

export const updateProfileError = payload => {
  return {
    type: UPDATE_PROFILE_ERROR,
    payload,
  };
};

export const updateProfileLoading = payload => {
  return {
    type: UPDATE_PROFILE_LOADING,
    payload,
  };
};
