import {
  GET_MASTER_GROUP,
  GET_MASTER_GROUP_ERROR,
  GET_MASTER_GROUP_LOADING,
  GET_MASTER_GROUP_SUCCESS,
} from '../../Constant';

export const getMasterGroup = payload => {
  return {
    type: GET_MASTER_GROUP,
    payload,
  };
};

export const getMasterGroupSuccess = payload => {
  return {
    type: GET_MASTER_GROUP_SUCCESS,
    payload,
  };
};

export const getMasterGroupError = payload => {
  return {
    type: GET_MASTER_GROUP_ERROR,
    payload,
  };
};

export const getMasterGroupLoading = payload => {
  return {
    type: GET_MASTER_GROUP_LOADING,
    payload,
  };
};
