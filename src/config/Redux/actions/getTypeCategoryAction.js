import {
  GET_TYPE_CATEGORY,
  GET_TYPE_CATEGORY_ERROR,
  GET_TYPE_CATEGORY_LOADING,
  GET_TYPE_CATEGORY_SUCCESS,
} from '../../Constant';

export const getTypeCategory = payload => {
  return {
    type: GET_TYPE_CATEGORY,
    payload,
  };
};

export const getTypeCategorySuccess = payload => {
  return {
    type: GET_TYPE_CATEGORY_SUCCESS,
    payload,
  };
};

export const getTypeCategoryError = payload => {
  return {
    type: GET_TYPE_CATEGORY_ERROR,
    payload,
  };
};

export const getTypeCategoryLoading = payload => {
  return {
    type: GET_TYPE_CATEGORY_LOADING,
    payload,
  };
};
