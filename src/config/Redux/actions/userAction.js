import {
  GET_USER_PROFILE,
  INTEREST_LOADING,
  SEND_WELCOME_MESSAGE,
  UPDATE_DEVICE_ID,
} from '../../Constant';

export const updateDeviceId = payload => {
  return {
    type: UPDATE_DEVICE_ID,
    payload,
  };
};

export const sendWelcomeMessage = payload => {
  return {
    type: SEND_WELCOME_MESSAGE,
    payload,
  };
};

export const getUserProfile = payload => {
  return {
    type: GET_USER_PROFILE,
    payload,
  };
};
export const loadingInterest = payload => {
  return {
    type: INTEREST_LOADING,
    payload,
  };
};
