import {
  UPDATE_PROFILE_PHOTO,
  UPDATE_PROFILE_PHOTO_ERROR,
  UPDATE_PROFILE_PHOTO_LOADING,
  UPDATE_PROFILE_PHOTO_SUCCESS,
} from '../../Constant';

export const updateProfilePhoto = payload => {
  return {
    type: UPDATE_PROFILE_PHOTO,
    payload,
  };
};

export const updateProfilePhotoSuccess = payload => {
  return {
    type: UPDATE_PROFILE_PHOTO_SUCCESS,
    payload,
  };
};

export const updateProfilePhotoError = payload => {
  return {
    type: UPDATE_PROFILE_PHOTO_ERROR,
    payload,
  };
};

export const updateProfilePhotoLoading = payload => {
  return {
    type: UPDATE_PROFILE_PHOTO_LOADING,
    payload,
  };
};
