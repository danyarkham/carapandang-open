import {
  GET_NOTIFICATIONS_LIST,
  GET_NOTIFICATIONS_LIST_ERROR,
  GET_NOTIFICATIONS_LIST_LOADING,
  GET_NOTIFICATIONS_LIST_SUCCESS,
  GET_NOTIFICATIONS_LIST_UPDATE,
} from '../../Constant';

export const getNotificationList = payload => {
  return {
    type: GET_NOTIFICATIONS_LIST,
    payload,
  };
};

export const getNotificationListSuccess = payload => {
  return {
    type: GET_NOTIFICATIONS_LIST_SUCCESS,
    payload,
  };
};

export const getNotificationListError = payload => {
  return {
    type: GET_NOTIFICATIONS_LIST_ERROR,
    payload,
  };
};

export const getNotificationListLoading = payload => {
  return {
    type: GET_NOTIFICATIONS_LIST_LOADING,
    payload,
  };
};

export const getNotificationUpdate = payload => {
  return {
    type: GET_NOTIFICATIONS_LIST_UPDATE,
    payload,
  };
};
