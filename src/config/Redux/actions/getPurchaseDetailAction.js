import {
  GET_PURCHASE_DETAIL,
  GET_PURCHASE_DETAIL_ERROR,
  GET_PURCHASE_DETAIL_LOADING,
  GET_PURCHASE_DETAIL_SELECT_PAYMENT,
  GET_PURCHASE_DETAIL_SUCCESS,
} from '../../Constant';

export const getPurchaseDetail = payload => {
  return {
    type: GET_PURCHASE_DETAIL,
    payload,
  };
};

export const getPurchaseDetailSuccess = payload => {
  return {
    type: GET_PURCHASE_DETAIL_SUCCESS,
    payload,
  };
};

export const getPurchaseDetailError = payload => {
  return {
    type: GET_PURCHASE_DETAIL_ERROR,
    payload,
  };
};

export const getPurchaseDetailSelectPayment = payload => {
  return {
    type: GET_PURCHASE_DETAIL_SELECT_PAYMENT,
    payload,
  };
};

export const getPurchaseDetailLoading = payload => {
  return {
    type: GET_PURCHASE_DETAIL_LOADING,
    payload,
  };
};
