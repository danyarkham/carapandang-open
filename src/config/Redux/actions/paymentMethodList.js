import {
  GET_PAYMENT_METHOD_LIST,
  GET_PAYMENT_METHOD_LIST_ERROR,
  GET_PAYMENT_METHOD_LIST_LOADING,
  GET_PAYMENT_METHOD_LIST_SUCCESS,
} from '../../Constant';

export const getPaymentMethodList = payload => {
  return {
    type: GET_PAYMENT_METHOD_LIST,
    payload,
  };
};

export const getPaymentMethodListSuccess = payload => {
  return {
    type: GET_PAYMENT_METHOD_LIST_SUCCESS,
    payload,
  };
};

export const getPaymentMethodListError = payload => {
  return {
    type: GET_PAYMENT_METHOD_LIST_ERROR,
    payload,
  };
};

export const getPaymentMethodListLoading = payload => {
  return {
    type: GET_PAYMENT_METHOD_LIST_LOADING,
    payload,
  };
};
