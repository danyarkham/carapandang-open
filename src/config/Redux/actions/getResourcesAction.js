import {
  GET_RESOURCES,
  GET_RESOURCES_ERROR,
  GET_RESOURCES_LOADING,
  GET_RESOURCES_SUCCESS,
} from '../../Constant';

export const getResources = payload => {
  return {
    type: GET_RESOURCES,
    payload,
  };
};

export const getResourcesSuccess = payload => {
  return {
    type: GET_RESOURCES_SUCCESS,
    payload,
  };
};

export const getResourcesError = payload => {
  return {
    type: GET_RESOURCES_ERROR,
    payload,
  };
};

export const getResourcesLoading = payload => {
  return {
    type: GET_RESOURCES_LOADING,
    payload,
  };
};
