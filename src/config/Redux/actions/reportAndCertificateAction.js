import {
  GET_CERTIFICATE,
  GET_CERTIFICATE_ERROR,
  GET_CERTIFICATE_LOADING,
  GET_CERTIFICATE_SUCCESS,
  GET_REPORTS,
  GET_REPORTS_ERROR,
  GET_REPORTS_LOADING,
  GET_REPORTS_SUCCESS,
} from '../../Constant';

export const getReports = payload => {
  return {
    type: GET_REPORTS,
    payload,
  };
};

export const getReportsSuccess = payload => {
  return {
    type: GET_REPORTS_SUCCESS,
    payload,
  };
};

export const getReportsError = payload => {
  return {
    type: GET_REPORTS_ERROR,
    payload,
  };
};

export const getReportsLoading = payload => {
  return {
    type: GET_REPORTS_LOADING,
    payload,
  };
};

// CERTIFICATE
export const getCertificate = payload => {
  return {
    type: GET_CERTIFICATE,
    payload,
  };
};

export const getCertificateSuccess = payload => {
  return {
    type: GET_CERTIFICATE_SUCCESS,
    payload,
  };
};

export const getCertificateError = payload => {
  return {
    type: GET_CERTIFICATE_ERROR,
    payload,
  };
};

export const getCertificateLoading = payload => {
  return {
    type: GET_CERTIFICATE_LOADING,
    payload,
  };
};
