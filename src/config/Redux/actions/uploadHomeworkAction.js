import {
  UPLOAD_HOMEWORK,
  UPLOAD_HOMEWORK_ERROR,
  UPLOAD_HOMEWORK_LOADING,
  UPLOAD_HOMEWORK_SUCCESS,
} from '../../Constant';

export const uploadHomework = payload => {
  return {
    type: UPLOAD_HOMEWORK,
    payload,
  };
};

export const uploadHomeworkSuccess = payload => {
  return {
    type: UPLOAD_HOMEWORK_SUCCESS,
    payload,
  };
};

export const uploadHomeworkError = payload => {
  return {
    type: UPLOAD_HOMEWORK_ERROR,
    payload,
  };
};

export const uploadHomeworkLoading = payload => {
  return {
    type: UPLOAD_HOMEWORK_LOADING,
    payload,
  };
};
