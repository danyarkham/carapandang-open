import {
  UPLOAD_CONFIRM_PAYMENT,
  UPLOAD_CONFIRM_PAYMENT_ERROR,
  UPLOAD_CONFIRM_PAYMENT_LOADING,
  UPLOAD_CONFIRM_PAYMENT_MODAL,
  UPLOAD_CONFIRM_PAYMENT_SUCCESS,
} from '../../Constant';

export const uploadConfirmPayment = payload => {
  return {
    type: UPLOAD_CONFIRM_PAYMENT,
    payload,
  };
};

export const uploadConfirmPaymentSuccess = payload => {
  return {
    type: UPLOAD_CONFIRM_PAYMENT_SUCCESS,
    payload,
  };
};

export const uploadConfirmPaymentError = payload => {
  return {
    type: UPLOAD_CONFIRM_PAYMENT_ERROR,
    payload,
  };
};

export const uploadConfirmPaymentLoading = payload => {
  return {
    type: UPLOAD_CONFIRM_PAYMENT_LOADING,
    payload,
  };
};

export const uploadConfirmPaymentModal = payload => {
  return {
    type: UPLOAD_CONFIRM_PAYMENT_MODAL,
    payload,
  };
};
