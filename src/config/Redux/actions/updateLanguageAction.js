import {
  UPDATE_LANGUAGE,
  UPDATE_LANGUAGE_ERROR,
  UPDATE_LANGUAGE_LOADING,
  UPDATE_LANGUAGE_SUCCESS,
} from '../../Constant';

export const updateLanguage = payload => {
  return {
    type: UPDATE_LANGUAGE,
    payload,
  };
};

export const updateLanguageSuccess = payload => {
  return {
    type: UPDATE_LANGUAGE_SUCCESS,
    payload,
  };
};

export const updateLanguageError = payload => {
  return {
    type: UPDATE_LANGUAGE_ERROR,
    payload,
  };
};

export const updateLanguageLoading = payload => {
  return {
    type: UPDATE_LANGUAGE_LOADING,
    payload,
  };
};
