import {
  GET_LANGUAGE,
  GET_LANGUAGE_SUCCESS,
  SET_LANGUAGE_SELECTED,
  SET_SELECTED_LANGUAGE,
} from '../../Constant';

export const getLanguage = payload => {
  return {
    type: GET_LANGUAGE,
    payload,
  };
};

export const getLanguageSuccess = payload => {
  return {
    type: GET_LANGUAGE_SUCCESS,
    payload,
  };
};

export const setSelectedLanguage = payload => {
  return {
    type: SET_SELECTED_LANGUAGE,
    payload,
  };
};
export const setLanguageSelected = payload => {
  return {
    type: SET_LANGUAGE_SELECTED,
    payload,
  };
};
