import {
  GET_BANNER,
  GET_BANNER_LOADING,
  GET_BANNER_SUCCESS,
} from '../../Constant';

export const getBanner = payload => {
  return {
    type: GET_BANNER,
    payload,
  };
};

export const getBannerSuccess = payload => {
  return {
    type: GET_BANNER_SUCCESS,
    payload,
  };
};

export const getBannerLoading = payload => {
  return {
    type: GET_BANNER_LOADING,
    payload,
  };
};
