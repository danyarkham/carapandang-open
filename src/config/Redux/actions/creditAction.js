import {
  CONFIRM_CREDIT_TRANSACTION,
  CONFIRM_CREDIT_TRANSACTION_ERROR,
  CONFIRM_CREDIT_TRANSACTION_LOADING,
  CONFIRM_CREDIT_TRANSACTION_SUCCESS,
  CREATE_CREDIT_TRANSACTION,
  CREATE_CREDIT_TRANSACTION_ERROR,
  CREATE_CREDIT_TRANSACTION_LOADING,
  CREATE_CREDIT_TRANSACTION_SUCCESS,
  GET_CREDIT_AVAILABLE,
  GET_CREDIT_AVAILABLE_ERROR,
  GET_CREDIT_AVAILABLE_LOADING,
  GET_CREDIT_AVAILABLE_SUCCESS,
  GET_HISTORY_CREDIT,
  GET_HISTORY_CREDIT_ERROR,
  GET_HISTORY_CREDIT_LOADING,
  GET_HISTORY_CREDIT_SUCCESS,
  GET_MASTER_CREDIT,
  GET_MASTER_CREDIT_ERROR,
  GET_MASTER_CREDIT_LOADING,
  GET_MASTER_CREDIT_SUCCESS,
  GET_MY_PRIVATE_CREDIT,
  GET_MY_PRIVATE_CREDIT_ERROR,
  GET_MY_PRIVATE_CREDIT_EXPIRED,
  GET_MY_PRIVATE_CREDIT_EXPIRED_ERROR,
  GET_MY_PRIVATE_CREDIT_EXPIRED_LOADING,
  GET_MY_PRIVATE_CREDIT_EXPIRED_SUCCESS,
  GET_MY_PRIVATE_CREDIT_LOADING,
  GET_MY_PRIVATE_CREDIT_SUCCESS,
} from '../../Constant';

export const getMasterCredit = payload => {
  return {
    type: GET_MASTER_CREDIT,
    payload,
  };
};

export const getMasterCreditSuccess = payload => {
  return {
    type: GET_MASTER_CREDIT_SUCCESS,
    payload,
  };
};

export const getMasterCreditError = payload => {
  return {
    type: GET_MASTER_CREDIT_ERROR,
    payload,
  };
};

export const getMasterCreditLoading = payload => {
  return {
    type: GET_MASTER_CREDIT_LOADING,
    payload,
  };
};

// transaction
export const createCreditTransaction = payload => {
  return {
    type: CREATE_CREDIT_TRANSACTION,
    payload,
  };
};

export const createCreditTransactionSuccess = payload => {
  return {
    type: CREATE_CREDIT_TRANSACTION_SUCCESS,
    payload,
  };
};

export const createCreditTransactionError = payload => {
  return {
    type: CREATE_CREDIT_TRANSACTION_ERROR,
    payload,
  };
};

export const createCreditTransactionLoading = payload => {
  return {
    type: CREATE_CREDIT_TRANSACTION_LOADING,
    payload,
  };
};

// CONFIRM PAYMENT
export const confirmCreditTransaction = payload => {
  return {
    type: CONFIRM_CREDIT_TRANSACTION,
    payload,
  };
};

export const confirmCreditTransactionSuccess = payload => {
  return {
    type: CONFIRM_CREDIT_TRANSACTION_SUCCESS,
    payload,
  };
};

export const confirmCreditTransactionError = payload => {
  return {
    type: CONFIRM_CREDIT_TRANSACTION_ERROR,
    payload,
  };
};

export const confirmCreditTransactionLoading = payload => {
  return {
    type: CONFIRM_CREDIT_TRANSACTION_LOADING,
    payload,
  };
};

// GET MY PRIVATE CREDIT
export const getMyPrivateCredit = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT,
    payload,
  };
};

export const getMyPrivateCreditSuccess = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_SUCCESS,
    payload,
  };
};

export const getMyPrivateCreditError = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_ERROR,
    payload,
  };
};

export const getMyPrivateCreditLoading = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_LOADING,
    payload,
  };
};

// EXPIRED
export const getMyPrivateCreditExpired = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_EXPIRED,
    payload,
  };
};

export const getMyPrivateCreditExpiredSuccess = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_EXPIRED_SUCCESS,
    payload,
  };
};

export const getMyPrivateCreditExpiredError = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_EXPIRED_ERROR,
    payload,
  };
};

export const getMyPrivateCreditExpiredLoading = payload => {
  return {
    type: GET_MY_PRIVATE_CREDIT_EXPIRED_LOADING,
    payload,
  };
};

// Credit History Transaction
export const getMyHistoryCredit = payload => {
  return {
    type: GET_HISTORY_CREDIT,
    payload,
  };
};

export const getMyHistoryCreditSuccess = payload => {
  return {
    type: GET_HISTORY_CREDIT_SUCCESS,
    payload,
  };
};

export const getMyHistoryCreditError = payload => {
  return {
    type: GET_HISTORY_CREDIT_ERROR,
    payload,
  };
};

export const getMyHistoryCreditLoading = payload => {
  return {
    type: GET_HISTORY_CREDIT_LOADING,
    payload,
  };
};

// GET CREDIT AVAILABLE
export const getCreditAvailable = payload => {
  return {
    type: GET_CREDIT_AVAILABLE,
    payload,
  };
};

export const getCreditAvailableSuccess = payload => {
  return {
    type: GET_CREDIT_AVAILABLE_SUCCESS,
    payload,
  };
};

export const getCreditAvailableError = payload => {
  return {
    type: GET_CREDIT_AVAILABLE_ERROR,
    payload,
  };
};

export const getCreditAvailableLoading = payload => {
  return {
    type: GET_CREDIT_AVAILABLE_LOADING,
    payload,
  };
};
