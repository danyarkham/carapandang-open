import {
  GET_LIST_GROUP,
  GET_LIST_GROUP_DETAIL,
  GET_LIST_GROUP_DETAIL_ERROR,
  GET_LIST_GROUP_DETAIL_LOADING,
  GET_LIST_GROUP_DETAIL_SUCCESS,
  GET_LIST_GROUP_ERROR,
  GET_LIST_GROUP_HISTORY,
  GET_LIST_GROUP_HISTORY_ERROR,
  GET_LIST_GROUP_HISTORY_LOADING,
  GET_LIST_GROUP_HISTORY_SUCCESS,
  GET_LIST_GROUP_LOADING,
  GET_LIST_GROUP_SUCCESS,
} from '../../Constant';

export const getListGroup = payload => {
  return {
    type: GET_LIST_GROUP,
    payload,
  };
};

export const getListGroupSuccess = payload => {
  return {
    type: GET_LIST_GROUP_SUCCESS,
    payload,
  };
};

export const getListGroupError = payload => {
  return {
    type: GET_LIST_GROUP_ERROR,
    payload,
  };
};

export const getListGroupLoading = payload => {
  return {
    type: GET_LIST_GROUP_LOADING,
    payload,
  };
};

// DETAIL
export const getListGroupDetail = payload => {
  return {
    type: GET_LIST_GROUP_DETAIL,
    payload,
  };
};

export const getListGroupDetailSuccess = payload => {
  return {
    type: GET_LIST_GROUP_DETAIL_SUCCESS,
    payload,
  };
};

export const getListGroupDetailError = payload => {
  return {
    type: GET_LIST_GROUP_DETAIL_ERROR,
    payload,
  };
};

export const getListGroupDetailLoading = payload => {
  return {
    type: GET_LIST_GROUP_DETAIL_LOADING,
    payload,
  };
};

// HISTORY
export const getListGroupHistory = payload => {
  return {
    type: GET_LIST_GROUP_HISTORY,
    payload,
  };
};

export const getListGroupHistorySuccess = payload => {
  return {
    type: GET_LIST_GROUP_HISTORY_SUCCESS,
    payload,
  };
};

export const getListGroupHistoryError = payload => {
  return {
    type: GET_LIST_GROUP_HISTORY_ERROR,
    payload,
  };
};

export const getListGroupHistoryLoading = payload => {
  return {
    type: GET_LIST_GROUP_HISTORY_LOADING,
    payload,
  };
};
