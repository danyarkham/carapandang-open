import {
  ADD_INSTRUCTOR_TO_WHITELIST,
  ADD_INSTRUCTOR_TO_WHITELIST_ERROR,
  ADD_INSTRUCTOR_TO_WHITELIST_LOADING,
  ADD_INSTRUCTOR_TO_WHITELIST_SUCCESS,
  GET_INSTRUCTOR,
  GET_INSTRUCTOR_ERROR,
  GET_INSTRUCTOR_FILTERED,
  GET_INSTRUCTOR_FILTER_MODAL_VISIBLE,
  GET_INSTRUCTOR_LOADING,
  GET_INSTRUCTOR_SCHEDULE,
  GET_INSTRUCTOR_SCHEDULE_ERROR,
  GET_INSTRUCTOR_SCHEDULE_LOADING,
  GET_INSTRUCTOR_SCHEDULE_SUCCESS,
  GET_INSTRUCTOR_SUCCESS,
  GET_WHITE_LIST,
  GET_WHITE_LIST_ERROR,
  GET_WHITE_LIST_LOADING,
  GET_WHITE_LIST_SUCCESS,
  RATE_INSTRUCTOR,
  RATE_INSTRUCTOR_ERROR,
  RATE_INSTRUCTOR_LOADING,
  RATE_INSTRUCTOR_SUCCESS,
  REMOVE_INSTRUCTOR_FROM_WHITELIST,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_ERROR,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_LOADING,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_SUCCESS,
  SELECTED_SCHEDULE_INSTRUCTOR,
  SET_HISTORY_DETAIL,
  SHOW_INSTRUCTOR_FILTERED,
  UPDATE_INSTRUCTOR_LIST,
} from '../../Constant';

export const getInstructor = payload => {
  return {
    type: GET_INSTRUCTOR,
    payload,
  };
};

export const getInstructorSuccess = payload => {
  return {
    type: GET_INSTRUCTOR_SUCCESS,
    payload,
  };
};

export const getInstructorError = payload => {
  return {
    type: GET_INSTRUCTOR_ERROR,
    payload,
  };
};

export const getInstructorLoading = payload => {
  return {
    type: GET_INSTRUCTOR_LOADING,
    payload,
  };
};

export const updateInstructorList = payload => {
  return {
    type: UPDATE_INSTRUCTOR_LIST,
    payload,
  };
};

export const getInstructorFiltered = payload => {
  return {
    type: GET_INSTRUCTOR_FILTERED,
    payload,
  };
};

// Schedule instructor
export const getInstructorSchedule = payload => {
  return {
    type: GET_INSTRUCTOR_SCHEDULE,
    payload,
  };
};

export const getInstructorScheduleSuccess = payload => {
  return {
    type: GET_INSTRUCTOR_SCHEDULE_SUCCESS,
    payload,
  };
};

export const getInstructorScheduleError = payload => {
  return {
    type: GET_INSTRUCTOR_SCHEDULE_ERROR,
    payload,
  };
};

export const getInstructorScheduleLoading = payload => {
  return {
    type: GET_INSTRUCTOR_SCHEDULE_LOADING,
    payload,
  };
};

export const getInstructorModalFilterVisible = payload => {
  return {
    type: GET_INSTRUCTOR_FILTER_MODAL_VISIBLE,
    payload,
  };
};

export const showFilterInstructor = payload => {
  return {
    type: SHOW_INSTRUCTOR_FILTERED,
    payload,
  };
};

/**
 *
 * WHITELIST START
 */

// GET WHITE LIST
export const getWhiteList = payload => {
  return {
    type: GET_WHITE_LIST,
    payload,
  };
};

export const getWhiteListSuccess = payload => {
  return {
    type: GET_WHITE_LIST_SUCCESS,
    payload,
  };
};

export const getWhiteListError = payload => {
  return {
    type: GET_WHITE_LIST_ERROR,
    payload,
  };
};

export const getWhiteListLoading = payload => {
  return {
    type: GET_WHITE_LIST_LOADING,
    payload,
  };
};

// ADD INDTRUCTOR TO WHITELIST
export const addInstructorToWhitelist = payload => {
  return {
    type: ADD_INSTRUCTOR_TO_WHITELIST,
    payload,
  };
};

export const addInstructorToWhitelistSuccess = payload => {
  return {
    type: ADD_INSTRUCTOR_TO_WHITELIST_SUCCESS,
    payload,
  };
};

export const addInstructorToWhitelistError = payload => {
  return {
    type: ADD_INSTRUCTOR_TO_WHITELIST_ERROR,
    payload,
  };
};

export const addInstructorToWhitelistLoading = payload => {
  return {
    type: ADD_INSTRUCTOR_TO_WHITELIST_LOADING,
    payload,
  };
};

// REMOVE INSTRUCTOR FROM WHITELIST
export const removeInstructorFromWhitelist = payload => {
  return {
    type: REMOVE_INSTRUCTOR_FROM_WHITELIST,
    payload,
  };
};

export const removeInstructorFromWhitelistSuccess = payload => {
  return {
    type: REMOVE_INSTRUCTOR_FROM_WHITELIST_SUCCESS,
    payload,
  };
};

export const removeInstructorFromWhitelistError = payload => {
  return {
    type: REMOVE_INSTRUCTOR_FROM_WHITELIST_ERROR,
    payload,
  };
};

export const removeInstructorFromWhitelistLoading = payload => {
  return {
    type: REMOVE_INSTRUCTOR_FROM_WHITELIST_LOADING,
    payload,
  };
};

// RATE INSTRUCTOR
export const rateInstructor = payload => {
  return {
    type: RATE_INSTRUCTOR,
    payload,
  };
};

export const rateInstructorSuccess = payload => {
  return {
    type: RATE_INSTRUCTOR_SUCCESS,
    payload,
  };
};

export const rateInstructorError = payload => {
  return {
    type: RATE_INSTRUCTOR_ERROR,
    payload,
  };
};

export const rateInstructorLoading = payload => {
  return {
    type: RATE_INSTRUCTOR_LOADING,
    payload,
  };
};

export const setHistoryDetail = payload => {
  return {
    type: SET_HISTORY_DETAIL,
    payload,
  };
};

export const setSelectedSchedule = payload => {
  return {
    type: SELECTED_SCHEDULE_INSTRUCTOR,
    payload,
  };
};
