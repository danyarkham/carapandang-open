import {
  GET_INVITATION_LINK_PRIVATE,
  GET_INVITATION_LINK_PRIVATE_ERROR,
  GET_INVITATION_LINK_PRIVATE_LOADING,
  GET_INVITATION_LINK_PRIVATE_SUCCESS,
  GET_PRIVATE_HOMEWORK,
  GET_PRIVATE_HOMEWORK_ERROR,
  GET_PRIVATE_HOMEWORK_LOADING,
  GET_PRIVATE_HOMEWORK_SUCCESS,
  PRIVATE_TRANSACTION,
  PRIVATE_TRANSACTION_CLASS,
  PRIVATE_TRANSACTION_CLASS_ERROR,
  PRIVATE_TRANSACTION_CLASS_LOADING,
  PRIVATE_TRANSACTION_CLASS_SUCCESS,
  PRIVATE_TRANSACTION_ERROR,
  PRIVATE_TRANSACTION_LOADING,
  PRIVATE_TRANSACTION_MODAL_VISIBLE,
  PRIVATE_TRANSACTION_SUCCESS,
  SET_PARTICIPANT,
  UPLOAD_HOMEWORK_PRIVATE,
  UPLOAD_HOMEWORK_PRIVATE_ERROR,
  UPLOAD_HOMEWORK_PRIVATE_LOADING,
  UPLOAD_HOMEWORK_PRIVATE_SUCCESS,
} from '../../Constant';

export const privateTransaction = payload => {
  return {
    type: PRIVATE_TRANSACTION,
    payload,
  };
};

export const privateTransactionSuccess = payload => {
  return {
    type: PRIVATE_TRANSACTION_SUCCESS,
    payload,
  };
};

export const privateTransactionError = payload => {
  return {
    type: PRIVATE_TRANSACTION_ERROR,
    payload,
  };
};

export const privateTransactionLoading = payload => {
  return {
    type: PRIVATE_TRANSACTION_LOADING,
    payload,
  };
};

export const privateTransactionModalVisible = payload => {
  return {
    type: PRIVATE_TRANSACTION_MODAL_VISIBLE,
    payload,
  };
};

// private class
export const privateTransactionClass = payload => {
  return {
    type: PRIVATE_TRANSACTION_CLASS,
    payload,
  };
};

export const privateTransactionClassSuccess = payload => {
  return {
    type: PRIVATE_TRANSACTION_CLASS_SUCCESS,
    payload,
  };
};

export const privateTransactionClassError = payload => {
  return {
    type: PRIVATE_TRANSACTION_CLASS_ERROR,
    payload,
  };
};

export const privateTransactionClassLoading = payload => {
  return {
    type: PRIVATE_TRANSACTION_CLASS_LOADING,
    payload,
  };
};

// INVITATION LINK
export const getInvitationLinkPrivate = payload => {
  return {
    type: GET_INVITATION_LINK_PRIVATE,
    payload,
  };
};

export const getInvitationLinkPrivateSuccess = payload => {
  return {
    type: GET_INVITATION_LINK_PRIVATE_SUCCESS,
    payload,
  };
};

export const getInvitationLinkPrivateError = payload => {
  return {
    type: GET_INVITATION_LINK_PRIVATE_ERROR,
    payload,
  };
};

export const getInvitationLinkPrivateLoading = payload => {
  return {
    type: GET_INVITATION_LINK_PRIVATE_LOADING,
    payload,
  };
};

// PRIVATE HOMEWORK
export const getPrivateHomework = payload => {
  return {
    type: GET_PRIVATE_HOMEWORK,
    payload,
  };
};

export const getPrivateHomeworkSuccess = payload => {
  return {
    type: GET_PRIVATE_HOMEWORK_SUCCESS,
    payload,
  };
};

export const getPrivateHomeworkError = payload => {
  return {
    type: GET_PRIVATE_HOMEWORK_ERROR,
    payload,
  };
};

export const getPrivateHomeworkLoading = payload => {
  return {
    type: GET_PRIVATE_HOMEWORK_LOADING,
    payload,
  };
};

// UPLOAD HOMEWORK PRIVATE
export const uploadPrivateHomework = payload => {
  return {
    type: UPLOAD_HOMEWORK_PRIVATE,
    payload,
  };
};

export const uploadPrivateHomeworkSuccess = payload => {
  return {
    type: UPLOAD_HOMEWORK_PRIVATE_SUCCESS,
    payload,
  };
};

export const uploadPrivateHomeworkError = payload => {
  return {
    type: UPLOAD_HOMEWORK_PRIVATE_ERROR,
    payload,
  };
};

export const uploadPrivateHomeworkLoading = payload => {
  return {
    type: UPLOAD_HOMEWORK_PRIVATE_LOADING,
    payload,
  };
};

export const setParticipant = payload => {
  return {
    type: SET_PARTICIPANT,
    payload,
  };
};
