import {
  GET_LEVEL_CATEGORY,
  GET_LEVEL_CATEGORY_ERROR,
  GET_LEVEL_CATEGORY_LOADING,
  GET_LEVEL_CATEGORY_SUCCESS,
} from '../../Constant';

export const getLevelCategory = payload => {
  return {
    type: GET_LEVEL_CATEGORY,
    payload,
  };
};

export const getLevelCategorySuccess = payload => {
  return {
    type: GET_LEVEL_CATEGORY_SUCCESS,
    payload,
  };
};

export const getLevelCategoryError = payload => {
  return {
    type: GET_LEVEL_CATEGORY_ERROR,
    payload,
  };
};

export const getLevelCategoryLoading = payload => {
  return {
    type: GET_LEVEL_CATEGORY_LOADING,
    payload,
  };
};
