import {
  GET_ALL_LEVEL,
  GET_ALL_LEVEL_ERROR,
  GET_ALL_LEVEL_LOADING,
  GET_ALL_LEVEL_SUCCESS,
  GET_ALL_TYPE,
  GET_ALL_TYPE_ERROR,
  GET_ALL_TYPE_LOADING,
  GET_ALL_TYPE_SUCCESS,
} from '../../Constant';

export const typeCategory = payload => {
  return {
    type: GET_ALL_TYPE,
    payload,
  };
};

export const typeCategorySuccess = payload => {
  return {
    type: GET_ALL_TYPE_SUCCESS,
    payload,
  };
};

export const typeCategoryError = payload => {
  return {
    type: GET_ALL_TYPE_ERROR,
    payload,
  };
};

export const typeCategoryLoading = payload => {
  return {
    type: GET_ALL_TYPE_LOADING,
    payload,
  };
};

export const levelCategory = payload => {
  return {
    type: GET_ALL_LEVEL,
    payload,
  };
};

export const levelCategorySuccess = payload => {
  return {
    type: GET_ALL_LEVEL_SUCCESS,
    payload,
  };
};

export const levelCategoryError = payload => {
  return {
    type: GET_ALL_LEVEL_ERROR,
    payload,
  };
};

export const levelCategoryLoading = payload => {
  return {
    type: GET_ALL_LEVEL_LOADING,
    payload,
  };
};
