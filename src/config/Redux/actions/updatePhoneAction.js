import {
  UPDATE_PHONE_NUMBER,
  UPDATE_PHONE_NUMBER_ERROR,
  UPDATE_PHONE_NUMBER_LOADING,
  UPDATE_PHONE_NUMBER_MODAL,
  UPDATE_PHONE_NUMBER_SUCCESS,
  VERIFY_PHONE_NUMBER,
  VERIFY_PHONE_NUMBER_ERROR,
  VERIFY_PHONE_NUMBER_LOADING,
  VERIFY_PHONE_NUMBER_SUCCESS,
} from '../../Constant';

export const updatePhoneNumber = payload => {
  return {
    type: UPDATE_PHONE_NUMBER,
    payload,
  };
};

export const updatePhoneNumberSuccess = payload => {
  return {
    type: UPDATE_PHONE_NUMBER_SUCCESS,
    payload,
  };
};

export const updatePhoneNumberError = payload => {
  return {
    type: UPDATE_PHONE_NUMBER_ERROR,
    payload,
  };
};

export const updatePhoneNumberLoading = payload => {
  return {
    type: UPDATE_PHONE_NUMBER_LOADING,
    payload,
  };
};

export const updatePhoneNumberModal = payload => {
  return {
    type: UPDATE_PHONE_NUMBER_MODAL,
    payload,
  };
};

// Verify Phone Number

export const verifyPhoneNumber = payload => {
  return {
    type: VERIFY_PHONE_NUMBER,
    payload,
  };
};

export const verifyPhoneNumberSuccess = payload => {
  return {
    type: VERIFY_PHONE_NUMBER_SUCCESS,
    payload,
  };
};

export const verifyPhoneNumberError = payload => {
  return {
    type: VERIFY_PHONE_NUMBER_ERROR,
    payload,
  };
};

export const verifyPhoneNumberLoading = payload => {
  return {
    type: VERIFY_PHONE_NUMBER_LOADING,
    payload,
  };
};
