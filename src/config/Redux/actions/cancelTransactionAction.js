import {
  CANCEL_TRANSACTION,
  CANCEL_TRANSACTION_ERROR,
  CANCEL_TRANSACTION_LOADING,
  CANCEL_TRANSACTION_SUCCESS,
} from '../../Constant';

export const cancelTransaction = payload => {
  return {
    type: CANCEL_TRANSACTION,
    payload,
  };
};

export const cancelTransactionSuccess = payload => {
  return {
    type: CANCEL_TRANSACTION_SUCCESS,
    payload,
  };
};

export const cancelTransactionError = payload => {
  return {
    type: CANCEL_TRANSACTION_ERROR,
    payload,
  };
};

export const cancelTransactionLoading = payload => {
  return {
    type: CANCEL_TRANSACTION_LOADING,
    payload,
  };
};
