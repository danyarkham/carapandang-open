import {
  UPDATE_EMAIL,
  UPDATE_EMAIL_ERROR,
  UPDATE_EMAIL_LOADING,
  UPDATE_EMAIL_MODAL,
  UPDATE_EMAIL_SUCCESS,
} from '../../Constant';

export const updateEmail = payload => {
  return {
    type: UPDATE_EMAIL,
    payload,
  };
};

export const updateEmailSuccess = payload => {
  return {
    type: UPDATE_EMAIL_SUCCESS,
    payload,
  };
};

export const updateEmailError = payload => {
  return {
    type: UPDATE_EMAIL_ERROR,
    payload,
  };
};

export const updateEmailLoading = payload => {
  return {
    type: UPDATE_EMAIL_LOADING,
    payload,
  };
};

export const updateEmailModal = payload => {
  return {
    type: UPDATE_EMAIL_MODAL,
    payload,
  };
};
