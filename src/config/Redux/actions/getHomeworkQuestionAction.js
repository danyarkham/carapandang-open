import {
  GET_HOMEWORK_QUESTION,
  GET_HOMEWORK_QUESTION_ERROR,
  GET_HOMEWORK_QUESTION_LOADING,
  GET_HOMEWORK_QUESTION_SUCCESS,
} from '../../Constant';

export const getHomeworkQuestion = payload => {
  return {
    type: GET_HOMEWORK_QUESTION,
    payload,
  };
};

export const getHomeworkQuestionSuccess = payload => {
  return {
    type: GET_HOMEWORK_QUESTION_SUCCESS,
    payload,
  };
};

export const getHomeworkQuestionError = payload => {
  return {
    type: GET_HOMEWORK_QUESTION_ERROR,
    payload,
  };
};

export const getHomeworkQuestionLoading = payload => {
  return {
    type: GET_HOMEWORK_QUESTION_LOADING,
    payload,
  };
};
