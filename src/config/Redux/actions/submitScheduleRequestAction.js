import {
  CANCEL_SCHEDULE_REQUEST,
  CANCEL_SCHEDULE_REQUEST_ERROR,
  CANCEL_SCHEDULE_REQUEST_LOADING,
  CANCEL_SCHEDULE_REQUEST_SUCCESS,
  MODAL_VISIBLE_SCHEDULE,
  SCHEDULE_REQUEST_SUBMIT,
  SCHEDULE_REQUEST_SUBMIT_ERROR,
  SCHEDULE_REQUEST_SUBMIT_LOADING,
  SCHEDULE_REQUEST_SUBMIT_SUCCESS,
} from '../../Constant';

export const submitScheduleRequest = payload => {
  return {
    type: SCHEDULE_REQUEST_SUBMIT,
    payload,
  };
};

export const submitScheduleRequestSuccess = payload => {
  return {
    type: SCHEDULE_REQUEST_SUBMIT_SUCCESS,
    payload,
  };
};

export const submitScheduleRequestError = payload => {
  return {
    type: SCHEDULE_REQUEST_SUBMIT_ERROR,
    payload,
  };
};

export const submitScheduleRequestLoading = payload => {
  return {
    type: SCHEDULE_REQUEST_SUBMIT_LOADING,
    payload,
  };
};

export const submitScheduleRequestModal = payload => {
  return {
    type: MODAL_VISIBLE_SCHEDULE,
    payload,
  };
};

// Cancel Schedule Request
export const cancelScheduleRequest = payload => {
  return {
    type: CANCEL_SCHEDULE_REQUEST,
    payload,
  };
};

export const cancelScheduleRequestSuccess = payload => {
  return {
    type: CANCEL_SCHEDULE_REQUEST_SUCCESS,
    payload,
  };
};

export const cancelScheduleRequestError = payload => {
  return {
    type: CANCEL_SCHEDULE_REQUEST_ERROR,
    payload,
  };
};

export const cancelScheduleRequestLoading = payload => {
  return {
    type: CANCEL_SCHEDULE_REQUEST_LOADING,
    payload,
  };
};
