import {
  ON_GOING_CLASS,
  SHOW_MODAL_PAYMENT_COMPLETE,
  UPDATE_PAYMENT_COMPLETE,
} from '../../Constant';

export const updatePaymentComplete = payload => {
  return {
    type: UPDATE_PAYMENT_COMPLETE,
    payload,
  };
};

export const showModalPaymentComplete = payload => {
  return {
    type: SHOW_MODAL_PAYMENT_COMPLETE,
    payload,
  };
};

export const onGoingClass = payload => {
  return {
    type: ON_GOING_CLASS,
    payload,
  };
};
