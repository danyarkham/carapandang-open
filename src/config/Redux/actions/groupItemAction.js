import {
  GET_GROUP_CLASS,
  GET_GROUP_CLASS_DETAIL,
  GET_GROUP_CLASS_ERROR,
  GET_GROUP_CLASS_FILTER,
  GET_GROUP_CLASS_LOADING,
  GET_GROUP_CLASS_SUCCESS,
  GET_GROUP_CLASS_TOGGLE_FILTER,
  GET_INVITATION_GROUP_LINK,
  GET_INVITATION_GROUP_LINK_ERROR,
  GET_INVITATION_GROUP_LINK_LOADING,
  GET_INVITATION_GROUP_LINK_SUCCESS,
} from '../../Constant';

export const getGroupItem = payload => {
  return {
    type: GET_GROUP_CLASS,
    payload,
  };
};

export const getGroupItemDetail = payload => {
  return {
    type: GET_GROUP_CLASS_DETAIL,
    payload,
  };
};

export const getGroupItemSuccess = payload => {
  return {
    type: GET_GROUP_CLASS_SUCCESS,
    payload,
  };
};

export const getGroupItemError = payload => {
  return {
    type: GET_GROUP_CLASS_ERROR,
    payload,
  };
};

export const getGroupItemLoading = payload => {
  return {
    type: GET_GROUP_CLASS_LOADING,
    payload,
  };
};

export const toggleFilterGroupClass = payload => {
  return {
    type: GET_GROUP_CLASS_TOGGLE_FILTER,
    payload,
  };
};

export const getGroupClassFiltered = payload => {
  return {
    type: GET_GROUP_CLASS_FILTER,
    payload,
  };
};

export const getGroupInvitationLink = payload => {
  return {
    type: GET_INVITATION_GROUP_LINK,
    payload,
  };
};

export const getGroupInvitationLinkLoading = payload => {
  return {
    type: GET_INVITATION_GROUP_LINK_LOADING,
    payload,
  };
};
export const getGroupInvitationLinkSuccess = payload => {
  return {
    type: GET_INVITATION_GROUP_LINK_SUCCESS,
    payload,
  };
};

export const getGroupInvitationLinkError = payload => {
  return {
    type: GET_INVITATION_GROUP_LINK_ERROR,
    payload,
  };
};
