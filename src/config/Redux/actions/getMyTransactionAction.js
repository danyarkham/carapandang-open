import {
  GET_MY_TRANSACTION,
  GET_MY_TRANSACTION_DETAIL,
  GET_MY_TRANSACTION_DETAIL_ERROR,
  GET_MY_TRANSACTION_DETAIL_SUCCESS,
  GET_MY_TRANSACTION_ERROR,
  GET_MY_TRANSACTION_LOADING,
  GET_MY_TRANSACTION_MODAL,
  GET_MY_TRANSACTION_SUCCESS,
  GET_MY_TRANSACTION_UNSUCCESS,
} from '../../Constant';

export const getMyTransaction = payload => {
  return {
    type: GET_MY_TRANSACTION,
    payload,
  };
};

export const getMyTransactionSuccess = payload => {
  return {
    type: GET_MY_TRANSACTION_SUCCESS,
    payload,
  };
};

export const getMyTransactionUnSuccess = payload => {
  return {
    type: GET_MY_TRANSACTION_UNSUCCESS,
    payload,
  };
};

export const getMyTransactionError = payload => {
  return {
    type: GET_MY_TRANSACTION_ERROR,
    payload,
  };
};

export const getMyTransactionLoading = payload => {
  return {
    type: GET_MY_TRANSACTION_LOADING,
    payload,
  };
};
export const getMyTransactionModal = payload => {
  return {
    type: GET_MY_TRANSACTION_MODAL,
    payload,
  };
};

export const getMyTransactionDetail = payload => {
  return {
    type: GET_MY_TRANSACTION_DETAIL,
    payload,
  };
};
export const getMyTransactionDetailSuccess = payload => {
  return {
    type: GET_MY_TRANSACTION_DETAIL_SUCCESS,
    payload,
  };
};
export const getMyTransactionDetailError = payload => {
  return {
    type: GET_MY_TRANSACTION_DETAIL_ERROR,
    payload,
  };
};
