import {
  UPDATE_PASSWORD,
  UPDATE_PASSWORD_ERROR,
  UPDATE_PASSWORD_LOADING,
  UPDATE_PASSWORD_SUCCESS,
} from '../../Constant';

export const updatePassword = payload => {
  return {
    type: UPDATE_PASSWORD,
    payload,
  };
};

export const updatePasswordSuccess = payload => {
  return {
    type: UPDATE_PASSWORD_SUCCESS,
    payload,
  };
};

export const updatePasswordError = payload => {
  return {
    type: UPDATE_PASSWORD_ERROR,
    payload,
  };
};

export const updatePasswordLoading = payload => {
  return {
    type: UPDATE_PASSWORD_LOADING,
    payload,
  };
};
