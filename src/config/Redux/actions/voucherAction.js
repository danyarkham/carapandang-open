import {
  APPLY_VOUCHER,
  APPLY_VOUCHER_ERROR,
  APPLY_VOUCHER_LOADING,
  APPLY_VOUCHER_SUCCESS,
  GET_VOUCHER,
  GET_VOUCHER_ERROR,
  GET_VOUCHER_LOADING,
  GET_VOUCHER_SUCCESS,
  VOUCHER_REFERN_EARN,
  VOUCHER_REFERN_EARN_ERROR,
  VOUCHER_REFERN_EARN_LOADING,
  VOUCHER_REFERN_EARN_SUCCESS,
} from '../../Constant';

export const applyVoucher = payload => {
  return {
    type: APPLY_VOUCHER,
    payload,
  };
};

export const applyVoucherSuccess = payload => {
  return {
    type: APPLY_VOUCHER_SUCCESS,
    payload,
  };
};

export const applyVoucherError = payload => {
  return {
    type: APPLY_VOUCHER_ERROR,
    payload,
  };
};

export const applyVoucherLoading = payload => {
  return {
    type: APPLY_VOUCHER_LOADING,
    payload,
  };
};

// GET VOUCHER
export const getVoucher = payload => {
  return {
    type: GET_VOUCHER,
    payload,
  };
};

export const getVoucherSuccess = payload => {
  return {
    type: GET_VOUCHER_SUCCESS,
    payload,
  };
};

export const getVoucherError = payload => {
  return {
    type: GET_VOUCHER_ERROR,
    payload,
  };
};

export const getVoucherLoading = payload => {
  return {
    type: GET_VOUCHER_LOADING,
    payload,
  };
};

// GET VOUCHER REFFERN EARN
export const getVoucherRefernEarn = payload => {
  return {
    type: VOUCHER_REFERN_EARN,
    payload,
  };
};

export const getVoucherRefernEarnSuccess = payload => {
  return {
    type: VOUCHER_REFERN_EARN_SUCCESS,
    payload,
  };
};

export const getVoucherRefernEarnError = payload => {
  return {
    type: VOUCHER_REFERN_EARN_ERROR,
    payload,
  };
};

export const getVoucherRefernEarnLoading = payload => {
  return {
    type: VOUCHER_REFERN_EARN_LOADING,
    payload,
  };
};
