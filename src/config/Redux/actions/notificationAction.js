import {
  GET_NOTIFICATION_CREDIT,
  GET_NOTIFICATION_CREDIT_ERROR,
  GET_NOTIFICATION_CREDIT_LOADING,
  GET_NOTIFICATION_CREDIT_SUCCESS,
} from '../../Constant';

export const getNotificationCredit = payload => {
  return {
    type: GET_NOTIFICATION_CREDIT,
    payload,
  };
};

export const getNotificationCreditSuccess = payload => {
  return {
    type: GET_NOTIFICATION_CREDIT_SUCCESS,
    payload,
  };
};

export const getNotificationCreditError = payload => {
  return {
    type: GET_NOTIFICATION_CREDIT_ERROR,
    payload,
  };
};

export const getNotificationCreditLoading = payload => {
  return {
    type: GET_NOTIFICATION_CREDIT_LOADING,
    payload,
  };
};
