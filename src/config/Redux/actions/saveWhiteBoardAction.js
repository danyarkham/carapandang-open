import {
  GET_PRIVATE_WHITEBOARD,
  GET_WHITEBOARD,
  GET_WHITEBOARD_ERROR,
  GET_WHITEBOARD_LOADING,
  GET_WHITEBOARD_SUCCESS,
  SAVE_WHITE_BOARD,
  SAVE_WHITE_BOARD_ERROR,
  SAVE_WHITE_BOARD_LOADING,
  SAVE_WHITE_BOARD_SUCCESS,
} from '../../Constant';

export const saveWhiteBoard = payload => {
  return {
    type: SAVE_WHITE_BOARD,
    payload,
  };
};

export const saveWhiteBoardSuccess = payload => {
  return {
    type: SAVE_WHITE_BOARD_SUCCESS,
    payload,
  };
};

export const saveWhiteBoardError = payload => {
  return {
    type: SAVE_WHITE_BOARD_ERROR,
    payload,
  };
};

export const saveWhiteBoardLoading = payload => {
  return {
    type: SAVE_WHITE_BOARD_LOADING,
    payload,
  };
};

export const getWhiteBoard = payload => {
  return {
    type: GET_WHITEBOARD,
    payload,
  };
};
export const getPrivateWhiteBoard = payload => {
  return {
    type: GET_PRIVATE_WHITEBOARD,
    payload,
  };
};

export const getWhiteBoardSuccess = payload => {
  return {
    type: GET_WHITEBOARD_SUCCESS,
    payload,
  };
};

export const getWhiteBoardError = payload => {
  return {
    type: GET_WHITEBOARD_ERROR,
    payload,
  };
};

export const getWhiteBoardLoading = payload => {
  return {
    type: GET_WHITEBOARD_LOADING,
    payload,
  };
};
