import {
  GET_APPLICATION_LEVEL,
  GET_APPLICATION_LEVEL_SUCCESS,
  POST_APPLICATION_LEVEL,
} from '../../Constant';

export const getApplicationLevel = payload => {
  return {
    type: GET_APPLICATION_LEVEL,
    payload,
  };
};

export const getApplicationLevelSuccess = payload => {
  return {
    type: GET_APPLICATION_LEVEL_SUCCESS,
    payload,
  };
};

export const postApplicationLevel = payload => {
  return {
    type: POST_APPLICATION_LEVEL,
    payload,
  };
};
