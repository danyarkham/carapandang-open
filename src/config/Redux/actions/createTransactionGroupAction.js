import {
  CREATE_TRANSACTION_GROUP,
  CREATE_TRANSACTION_GROUP_ERROR,
  CREATE_TRANSACTION_GROUP_LOADING,
  CREATE_TRANSACTION_GROUP_SUCCESS,
} from '../../Constant';

export const createTransactionGroup = payload => {
  return {
    type: CREATE_TRANSACTION_GROUP,
    payload,
  };
};

export const createTransactionGroupSuccess = payload => {
  return {
    type: CREATE_TRANSACTION_GROUP_SUCCESS,
    payload,
  };
};

export const createTransactionGroupError = payload => {
  return {
    type: CREATE_TRANSACTION_GROUP_ERROR,
    payload,
  };
};

export const createTransactionGroupLoading = payload => {
  return {
    type: CREATE_TRANSACTION_GROUP_LOADING,
    payload,
  };
};
