import {
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_VISIBLE,
} from '../../Constant';

export const postEmail = payload => {
  return {
    type: FORGOT_PASSWORD,
    payload,
  };
};

export const setLoading = payload => {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    payload,
  };
};

export const toggleModal = payload => {
  return {
    type: FORGOT_PASSWORD_VISIBLE,
    payload,
  };
};

export const sendEmailSuccess = payload => {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    payload,
  };
};

export const sendEmailError = payload => {
  return {
    type: FORGOT_PASSWORD_ERROR,
    payload,
  };
};
