import {
  GET_BANNER_DETAIL,
  GET_BANNER_DETAIL_LOADING,
  GET_BANNER_DETAIL_SUCCESS,
} from '../../Constant';

export const getBannerDetail = payload => {
  return {
    type: GET_BANNER_DETAIL,
    payload,
  };
};

export const getBannerDetailSuccess = payload => {
  return {
    type: GET_BANNER_DETAIL_SUCCESS,
    payload,
  };
};

export const getBannerDetailLoading = payload => {
  return {
    type: GET_BANNER_DETAIL_LOADING,
    payload,
  };
};
