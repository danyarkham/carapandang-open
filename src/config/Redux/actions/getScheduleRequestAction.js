import {
  GET_SCHEDULE_REQUEST,
  GET_SCHEDULE_REQUEST_ERROR,
  GET_SCHEDULE_REQUEST_LOADING,
  GET_SCHEDULE_REQUEST_SUCCESS,
} from '../../Constant';

export const getScheduleRequest = payload => {
  return {
    type: GET_SCHEDULE_REQUEST,
    payload,
  };
};

export const getScheduleRequestSuccess = payload => {
  return {
    type: GET_SCHEDULE_REQUEST_SUCCESS,
    payload,
  };
};

export const getScheduleRequestError = payload => {
  return {
    type: GET_SCHEDULE_REQUEST_ERROR,
    payload,
  };
};

export const getScheduleRequestLoading = payload => {
  return {
    type: GET_SCHEDULE_REQUEST_LOADING,
    payload,
  };
};
