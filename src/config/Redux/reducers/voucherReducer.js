import {
  APPLY_VOUCHER_ERROR,
  APPLY_VOUCHER_LOADING,
  APPLY_VOUCHER_SUCCESS,
  GET_VOUCHER_ERROR,
  GET_VOUCHER_LOADING,
  GET_VOUCHER_SUCCESS,
  VOUCHER_REFERN_EARN_ERROR,
  VOUCHER_REFERN_EARN_LOADING,
  VOUCHER_REFERN_EARN_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  voucher: null,
  vouchers: null,
  refernEarn: {},
};

const voucherReducer = (state = initialState, action) => {
  switch (action.type) {
    case APPLY_VOUCHER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        voucher: action.payload,
      };
    case APPLY_VOUCHER_ERROR:
      return {
        ...state,
        isLoading: false,
        voucher: null,
      };

    case APPLY_VOUCHER_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // voucher list
    case GET_VOUCHER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        vouchers: action.payload,
      };
    case GET_VOUCHER_ERROR:
      return {
        ...state,
        isLoading: false,
        vouchers: null,
      };

    case GET_VOUCHER_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // voucher refern earn
    case VOUCHER_REFERN_EARN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        refernEarn: action.payload,
      };
    case VOUCHER_REFERN_EARN_ERROR:
      return {
        ...state,
        isLoading: false,
        refernEarn: {},
      };

    case VOUCHER_REFERN_EARN_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return {
        ...state,
      };
  }
};

export default voucherReducer;
