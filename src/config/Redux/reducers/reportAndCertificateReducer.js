import {
  GET_CERTIFICATE_ERROR,
  GET_CERTIFICATE_LOADING,
  GET_CERTIFICATE_SUCCESS,
  GET_REPORTS_ERROR,
  GET_REPORTS_LOADING,
  GET_REPORTS_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  reports: [],
  certificates: [],
};

const reportAndCertificateReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REPORTS_SUCCESS:
      return {
        ...state,
        reports: action.payload,
        isLoading: false,
      };
    case GET_REPORTS_ERROR:
      return {
        ...state,
        reports: [],
        isLoading: false,
      };

    case GET_REPORTS_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case GET_CERTIFICATE_SUCCESS:
      return {
        ...state,
        certificates: action.payload,
        isLoading: false,
      };
    case GET_CERTIFICATE_ERROR:
      return {
        ...state,
        certificates: [],
        isLoading: false,
      };

    case GET_CERTIFICATE_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default reportAndCertificateReducer;
