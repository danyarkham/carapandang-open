import {
  UPDATE_EMAIL_ERROR,
  UPDATE_EMAIL_LOADING,
  UPDATE_EMAIL_MODAL,
  UPDATE_EMAIL_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  modal: false,
};

const updateEmailReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_EMAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        modal: false,
      };
    case UPDATE_EMAIL_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case UPDATE_EMAIL_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case UPDATE_EMAIL_MODAL:
      return {
        ...state,
        modal: action.payload,
      };

    default:
      return {
        ...state,
      };
  }
};

export default updateEmailReducer;
