import {
  CONFIRM_CREDIT_TRANSACTION_ERROR,
  CONFIRM_CREDIT_TRANSACTION_LOADING,
  CONFIRM_CREDIT_TRANSACTION_SUCCESS,
  CREATE_CREDIT_TRANSACTION_ERROR,
  CREATE_CREDIT_TRANSACTION_LOADING,
  CREATE_CREDIT_TRANSACTION_SUCCESS,
  GET_CREDIT_AVAILABLE_ERROR,
  GET_CREDIT_AVAILABLE_SUCCESS,
  GET_HISTORY_CREDIT_ERROR,
  GET_HISTORY_CREDIT_LOADING,
  GET_HISTORY_CREDIT_SUCCESS,
  GET_MASTER_CREDIT_ERROR,
  GET_MASTER_CREDIT_LOADING,
  GET_MASTER_CREDIT_SUCCESS,
  GET_MY_PRIVATE_CREDIT_ERROR,
  GET_MY_PRIVATE_CREDIT_EXPIRED_ERROR,
  GET_MY_PRIVATE_CREDIT_EXPIRED_LOADING,
  GET_MY_PRIVATE_CREDIT_EXPIRED_SUCCESS,
  GET_MY_PRIVATE_CREDIT_LOADING,
  GET_MY_PRIVATE_CREDIT_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  master_credits: [],
  my_private_credits: [],
  my_private_expired: [],
  creditHistories: [],
  creditHistoryLoading: false,
  transaction: null,
  creditAvailable: 0,
};

const creditReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MASTER_CREDIT_SUCCESS:
      return {
        ...state,
        master_credits: action.payload,
        isLoading: false,
      };

    case GET_MASTER_CREDIT_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case GET_MASTER_CREDIT_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // create transaction
    case CREATE_CREDIT_TRANSACTION_SUCCESS:
      return {
        ...state,
        transaction: action.payload,
        isLoading: false,
      };

    case CREATE_CREDIT_TRANSACTION_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case CREATE_CREDIT_TRANSACTION_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // confirm payment
    case CONFIRM_CREDIT_TRANSACTION_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };

    case CONFIRM_CREDIT_TRANSACTION_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case CONFIRM_CREDIT_TRANSACTION_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // MY PRIVATE CREDIT
    case GET_MY_PRIVATE_CREDIT_SUCCESS:
      return {
        ...state,
        my_private_credits: action.payload,
        isLoading: false,
      };

    case GET_MY_PRIVATE_CREDIT_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case GET_MY_PRIVATE_CREDIT_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // EXPIRED CREDIT
    case GET_MY_PRIVATE_CREDIT_EXPIRED_SUCCESS:
      return {
        ...state,
        my_private_expired: action.payload,
        isLoading: false,
      };

    case GET_MY_PRIVATE_CREDIT_EXPIRED_ERROR:
      return {
        ...state,
        isLoading: false,
        my_private_expired: [],
      };

    case GET_MY_PRIVATE_CREDIT_EXPIRED_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // CREDIT HISTORY
    case GET_HISTORY_CREDIT_SUCCESS:
      return {
        ...state,
        creditHistories: action.payload,
        creditHistoryLoading: false,
      };

    case GET_HISTORY_CREDIT_ERROR:
      return {
        ...state,
        creditHistoryLoading: false,
      };

    case GET_HISTORY_CREDIT_LOADING:
      return {
        ...state,
        creditHistoryLoading: true,
      };

    // credit available
    case GET_CREDIT_AVAILABLE_SUCCESS:
      return {
        ...state,
        creditAvailable: action.payload,
      };

    case GET_CREDIT_AVAILABLE_ERROR:
      return {
        ...state,
        creditAvailable: 0,
      };
    default:
      return state;
  }
};

export default creditReducer;
