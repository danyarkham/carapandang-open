import {
  GET_LANGUAGE_SUCCESS,
  SET_LANGUAGE_SELECTED,
  SET_SELECTED_LANGUAGE,
  UPDATE_LANGUAGE_ERROR,
  UPDATE_LANGUAGE_LOADING,
  UPDATE_LANGUAGE_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  languages: [],
  language_id: null,
  language: null,
};

const languageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LANGUAGE_SUCCESS:
      return {
        ...state,
        languages: action.payload,
      };

    case SET_SELECTED_LANGUAGE:
      return {
        ...state,
        language_id: action.payload,
      };

    case UPDATE_LANGUAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case UPDATE_LANGUAGE_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case UPDATE_LANGUAGE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case SET_LANGUAGE_SELECTED:
      return {
        ...state,
        language: action.payload,
      };

    default:
      return state;
  }
};

export default languageReducer;
