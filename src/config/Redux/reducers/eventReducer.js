import {
  ON_GOING_CLASS,
  SHOW_MODAL_PAYMENT_COMPLETE,
  UPDATE_PAYMENT_COMPLETE,
} from '../../Constant';

const initialState = {
  modal_payment_complete: false,
  on_going_class: false,
};

const eventReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PAYMENT_COMPLETE:
      return {
        ...state,
        modal_payment_complete: true,
      };

    case SHOW_MODAL_PAYMENT_COMPLETE:
      return {
        ...state,
        modal_payment_complete: action.payload,
      };
    case ON_GOING_CLASS:
      return {
        ...state,
        on_going_class: !state.on_going_class,
      };

    default:
      return state;
  }
};

export default eventReducer;
