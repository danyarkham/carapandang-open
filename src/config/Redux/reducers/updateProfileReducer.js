import {
  INTEREST_LOADING,
  UPDATE_PROFILE_ERROR,
  UPDATE_PROFILE_LOADING,
  UPDATE_PROFILE_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  interestLoading: false,
};

const updateProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case UPDATE_PROFILE_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case UPDATE_PROFILE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case INTEREST_LOADING:
      return {
        ...state,
        interestLoading: action.payload,
      };

    default:
      return state;
  }
};

export default updateProfileReducer;
