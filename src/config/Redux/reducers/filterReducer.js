import {
  GET_ALL_LEVEL_ERROR,
  GET_ALL_LEVEL_LOADING,
  GET_ALL_LEVEL_SUCCESS,
  GET_ALL_TYPE_ERROR,
  GET_ALL_TYPE_LOADING,
  GET_ALL_TYPE_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  types: [],
  levels: [],
};

const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_TYPE_SUCCESS:
      return {
        ...state,
        types: action.payload,
        isLoading: false,
      };
    case GET_ALL_TYPE_ERROR:
      return {
        ...state,
        types: [],
        isLoading: false,
      };

    case GET_ALL_TYPE_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case GET_ALL_LEVEL_SUCCESS:
      return {
        ...state,
        levels: action.payload,
        isLoading: false,
      };
    case GET_ALL_LEVEL_ERROR:
      return {
        ...state,
        levels: [],
        isLoading: false,
      };

    case GET_ALL_LEVEL_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default filterReducer;
