import {
  GET_PAYMENT_METHOD_LIST_ERROR,
  GET_PAYMENT_METHOD_LIST_LOADING,
  GET_PAYMENT_METHOD_LIST_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  payments: [],
};

const paymentListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PAYMENT_METHOD_LIST_SUCCESS:
      return {
        ...state,
        payments: action.payload,
        isLoading: false,
      };

    case GET_PAYMENT_METHOD_LIST_ERROR:
      return {
        ...state,
        payments: [],
        isLoading: false,
      };

    case GET_PAYMENT_METHOD_LIST_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default paymentListReducer;
