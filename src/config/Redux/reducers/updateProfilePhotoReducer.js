import {
  UPDATE_PROFILE_PHOTO_ERROR,
  UPDATE_PROFILE_PHOTO_LOADING,
  UPDATE_PROFILE_PHOTO_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
};

const updateProfilePhotoReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE_PHOTO_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case UPDATE_PROFILE_PHOTO_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case UPDATE_PROFILE_PHOTO_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return {
        ...state,
      };
  }
};

export default updateProfilePhotoReducer;
