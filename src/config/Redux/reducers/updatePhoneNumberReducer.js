import {
  UPDATE_PHONE_NUMBER_ERROR,
  UPDATE_PHONE_NUMBER_LOADING,
  UPDATE_PHONE_NUMBER_MODAL,
  UPDATE_PHONE_NUMBER_SUCCESS,
  VERIFY_PHONE_NUMBER_ERROR,
  VERIFY_PHONE_NUMBER_LOADING,
  VERIFY_PHONE_NUMBER_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  modal: false,
};

const updatePhoneNumberReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PHONE_NUMBER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        modal: false,
      };
    case UPDATE_PHONE_NUMBER_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case UPDATE_PHONE_NUMBER_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case UPDATE_PHONE_NUMBER_MODAL:
      return {
        ...state,
        modal: action.payload,
      };

    case VERIFY_PHONE_NUMBER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        modal: false,
      };
    case VERIFY_PHONE_NUMBER_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case VERIFY_PHONE_NUMBER_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return {
        ...state,
      };
  }
};

export default updatePhoneNumberReducer;
