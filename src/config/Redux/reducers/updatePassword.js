import {
  UPDATE_PASSWORD_ERROR,
  UPDATE_PASSWORD_LOADING,
  UPDATE_PASSWORD_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  isSuccess: false,
};

const updatePasswordReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isSuccess: true,
      };
    case UPDATE_PASSWORD_ERROR:
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
      };

    case UPDATE_PASSWORD_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default updatePasswordReducer;
