import {
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_LOADING,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_VISIBLE,
} from '../../Constant';

const initialState = {
  isLoading: false,
  modal: false,
};

const passwordResetReducer = (state = initialState, action) => {
  switch (action.type) {
    case FORGOT_PASSWORD_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading,
      };
    case FORGOT_PASSWORD_VISIBLE:
      return {
        ...state,
        modal: !state.modal,
      };

    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        modal: true,
      };

    case FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default passwordResetReducer;
