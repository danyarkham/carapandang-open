import {
  ADD_INSTRUCTOR_TO_WHITELIST_ERROR,
  ADD_INSTRUCTOR_TO_WHITELIST_LOADING,
  ADD_INSTRUCTOR_TO_WHITELIST_SUCCESS,
  GET_INSTRUCTOR_ERROR,
  GET_INSTRUCTOR_FILTERED,
  GET_INSTRUCTOR_FILTER_MODAL_VISIBLE,
  GET_INSTRUCTOR_LOADING,
  GET_INSTRUCTOR_SCHEDULE_ERROR,
  GET_INSTRUCTOR_SCHEDULE_LOADING,
  GET_INSTRUCTOR_SCHEDULE_SUCCESS,
  GET_INSTRUCTOR_SUCCESS,
  GET_WHITE_LIST_ERROR,
  GET_WHITE_LIST_LOADING,
  GET_WHITE_LIST_SUCCESS,
  RATE_INSTRUCTOR_ERROR,
  RATE_INSTRUCTOR_LOADING,
  RATE_INSTRUCTOR_SUCCESS,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_ERROR,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_LOADING,
  REMOVE_INSTRUCTOR_FROM_WHITELIST_SUCCESS,
  SELECTED_SCHEDULE_INSTRUCTOR,
  SET_HISTORY_DETAIL,
  SHOW_INSTRUCTOR_FILTERED,
  UPDATE_INSTRUCTOR_LIST,
} from '../../Constant';

const initialState = {
  isLoading: false,
  instructors: [],
  instructor_filters: [],
  instructor_schedules: [],
  modalFilterVisible: false,
  showFilter: false,
  whitelistLoading: false,
  whitelists: [],
  rateLoading: false,
  historyDetail: null,
  selectedSchedule: null,
};

const instructorReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        instructors: action.payload,
        selectedSchedule: null,
        instructor_filters: action.payload,
        isLoading: false,
      };

    case GET_INSTRUCTOR_ERROR:
      return {
        ...state,
        isLoading: false,
        instructors: [],
        selectedSchedule: null,
        instructor_filters: [],
      };

    case GET_INSTRUCTOR_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    case UPDATE_INSTRUCTOR_LIST:
      return {
        ...state,
        instructor_filters: action.payload,
        isLoading: false,
      };
    case GET_INSTRUCTOR_SCHEDULE_SUCCESS:
      return {
        ...state,
        instructor_schedules: action.payload,
        isLoading: false,
      };
    case GET_INSTRUCTOR_FILTERED:
      return {
        ...state,
        instructor_filters: action.payload,
        isLoading: false,
      };

    case GET_INSTRUCTOR_SCHEDULE_ERROR:
      return {
        ...state,
        isLoading: false,
        instructor_schedules: [],
      };

    case GET_INSTRUCTOR_SCHEDULE_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case GET_INSTRUCTOR_FILTER_MODAL_VISIBLE:
      return {
        ...state,
        modalFilterVisible: !state.modalFilterVisible,
      };
    case SHOW_INSTRUCTOR_FILTERED:
      return {
        ...state,
        showFilter: action.payload,
      };

    // whitelist
    case GET_WHITE_LIST_SUCCESS:
      return {
        ...state,
        whitelists: action.payload,
        whitelistLoading: false,
      };

    case GET_WHITE_LIST_ERROR:
      return {
        ...state,
        whitelistLoading: false,
        whitelists: [],
      };

    case GET_WHITE_LIST_LOADING:
      return {
        ...state,
        whitelistLoading: true,
      };

    // add instructor to whitelist
    case ADD_INSTRUCTOR_TO_WHITELIST_SUCCESS:
      return {
        ...state,
        whitelists: [...state.whitelists, action.payload],
        whitelistLoading: false,
      };

    case ADD_INSTRUCTOR_TO_WHITELIST_ERROR:
      return {
        ...state,
        whitelistLoading: false,
      };

    case ADD_INSTRUCTOR_TO_WHITELIST_LOADING:
      return {
        ...state,
        whitelistLoading: true,
      };

    // remove instructor from whitelist
    case REMOVE_INSTRUCTOR_FROM_WHITELIST_SUCCESS:
      return {
        ...state,
        whitelists: action.payload,
        whitelistLoading: false,
      };

    case REMOVE_INSTRUCTOR_FROM_WHITELIST_ERROR:
      return {
        ...state,
        whitelistLoading: false,
      };

    case REMOVE_INSTRUCTOR_FROM_WHITELIST_LOADING:
      return {
        ...state,
        whitelistLoading: true,
      };

    // RATE INSTRUCTOR
    case RATE_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        rateLoading: false,
      };

    case RATE_INSTRUCTOR_ERROR:
      return {
        ...state,
        rateLoading: false,
      };

    case RATE_INSTRUCTOR_LOADING:
      return {
        ...state,
        rateLoading: true,
      };
    case SET_HISTORY_DETAIL:
      return {
        ...state,
        historyDetail: action.payload,
      };

    case SELECTED_SCHEDULE_INSTRUCTOR:
      return {
        ...state,
        selectedSchedule: action.payload,
      };

    default:
      return state;
  }
};

export default instructorReducer;
