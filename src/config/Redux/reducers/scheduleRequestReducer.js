import {
  CANCEL_SCHEDULE_REQUEST_ERROR,
  CANCEL_SCHEDULE_REQUEST_LOADING,
  CANCEL_SCHEDULE_REQUEST_SUCCESS,
  GET_LEVEL_CATEGORY_ERROR,
  GET_LEVEL_CATEGORY_LOADING,
  GET_LEVEL_CATEGORY_SUCCESS,
  GET_MASTER_GROUP_ERROR,
  GET_MASTER_GROUP_LOADING,
  GET_MASTER_GROUP_SUCCESS,
  GET_SCHEDULE_REQUEST_ERROR,
  GET_SCHEDULE_REQUEST_LOADING,
  GET_SCHEDULE_REQUEST_SUCCESS,
  GET_TYPE_CATEGORY_ERROR,
  GET_TYPE_CATEGORY_LOADING,
  GET_TYPE_CATEGORY_SUCCESS,
  MODAL_VISIBLE_SCHEDULE,
  SCHEDULE_REQUEST_LOADING_OF,
  SCHEDULE_REQUEST_SUBMIT_ERROR,
  SCHEDULE_REQUEST_SUBMIT_LOADING,
  SCHEDULE_REQUEST_SUBMIT_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  schedules: [],
  types: [],
  level: [],
  group_item: null,
  showModal: false,
};

const scheduleRequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case SCHEDULE_REQUEST_LOADING_OF:
      return {
        ...state,
        isLoading: false,
      };

    // Get Type Category
    case GET_TYPE_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        types: action.payload,
      };
    case GET_TYPE_CATEGORY_ERROR:
      return {
        ...state,
        isLoading: false,
        types: [],
      };

    case GET_TYPE_CATEGORY_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // Get Level Category
    case GET_LEVEL_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        levels: action.payload,
      };
    case GET_LEVEL_CATEGORY_ERROR:
      return {
        ...state,
        isLoading: false,
        levels: [],
      };

    case GET_LEVEL_CATEGORY_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // Get Group Data
    case GET_MASTER_GROUP_SUCCESS:
      return {
        ...state,
        isLoading: false,
        group_item: action.payload,
      };
    case GET_MASTER_GROUP_ERROR:
      return {
        ...state,
        isLoading: false,
        group_item: null,
      };

    case GET_MASTER_GROUP_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // submit schedule
    case SCHEDULE_REQUEST_SUBMIT_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case SCHEDULE_REQUEST_SUBMIT_ERROR:
      return {
        ...state,
        isLoading: false,
      };
    case MODAL_VISIBLE_SCHEDULE:
      return {
        ...state,
        showModal: !state.showModal,
      };

    case SCHEDULE_REQUEST_SUBMIT_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // schedule request list
    case GET_SCHEDULE_REQUEST_SUCCESS:
      return {
        ...state,
        schedules: action.payload,
        isLoading: false,
      };
    case GET_SCHEDULE_REQUEST_ERROR:
      return {
        ...state,
        schedules: [],
        isLoading: false,
      };

    case GET_SCHEDULE_REQUEST_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // Cancel Order
    case CANCEL_SCHEDULE_REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case CANCEL_SCHEDULE_REQUEST_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case CANCEL_SCHEDULE_REQUEST_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default scheduleRequestReducer;
