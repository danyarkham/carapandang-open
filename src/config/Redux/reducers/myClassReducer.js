import {
  PRIVATE_TRANSACTION_CLASS_ERROR,
  PRIVATE_TRANSACTION_CLASS_LOADING,
  PRIVATE_TRANSACTION_CLASS_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  private_class: [],
};

const myClassReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRIVATE_TRANSACTION_CLASS_SUCCESS:
      return {
        ...state,
        private_class: action.payload,
        isLoading: false,
      };

    case PRIVATE_TRANSACTION_CLASS_ERROR:
      return {
        ...state,
        isLoading: false,
        private_class: [],
      };

    case PRIVATE_TRANSACTION_CLASS_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default myClassReducer;
