import {GET_APPLICATION_LEVEL_SUCCESS} from '../../Constant';

const initialState = {
  levels: [],
};

const applicationLevelReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_APPLICATION_LEVEL_SUCCESS:
      return {
        ...state,
        levels: action.payload,
      };
    default:
      return state;
  }
};

export default applicationLevelReducer;
