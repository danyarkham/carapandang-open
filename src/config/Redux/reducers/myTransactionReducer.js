import {
  CANCEL_TRANSACTION_ERROR,
  CANCEL_TRANSACTION_LOADING,
  CANCEL_TRANSACTION_SUCCESS,
  GET_INVITATION_LINK,
  GET_INVITATION_LINK_PRIVATE_ERROR,
  GET_INVITATION_LINK_PRIVATE_LOADING,
  GET_INVITATION_LINK_PRIVATE_SUCCESS,
  GET_MY_TRANSACTION_DETAIL_ERROR,
  GET_MY_TRANSACTION_DETAIL_SUCCESS,
  GET_MY_TRANSACTION_ERROR,
  GET_MY_TRANSACTION_LOADING,
  GET_MY_TRANSACTION_MODAL,
  GET_MY_TRANSACTION_SUCCESS,
  GET_MY_TRANSACTION_UNSUCCESS,
  PRIVATE_TRANSACTION_ERROR,
  PRIVATE_TRANSACTION_LOADING,
  PRIVATE_TRANSACTION_MODAL_VISIBLE,
  PRIVATE_TRANSACTION_SUCCESS,
  SET_PARTICIPANT,
} from '../../Constant';

const initialState = {
  isLoading: false,
  transactions: [],
  transaction_need_payments: [],
  transaction: {},
  modal: false,
  inviteLink: null,
  privateLoading: false,
  privateSuccess: false,
  privateError: false,
  invitationLinkPrivate: null,
  participants: [],
  cancelTransactionLoading: false,
  cancelTransactionSuccess: false,
};

const myTransactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MY_TRANSACTION_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading,
        modal: false,
      };

    case GET_MY_TRANSACTION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        transactions: action.payload,
        modal: false,
      };
    case GET_MY_TRANSACTION_UNSUCCESS:
      return {
        ...state,
        isLoading: false,
        transaction_need_payments: action.payload,
        modal: false,
      };

    case GET_MY_TRANSACTION_ERROR:
      return {
        ...state,
        isLoading: false,
        transactions: [],
      };
    case GET_MY_TRANSACTION_DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        transaction: action.payload,
      };

    case GET_MY_TRANSACTION_DETAIL_ERROR:
      return {
        ...state,
        isLoading: false,
        transaction: {},
      };
    case GET_MY_TRANSACTION_MODAL:
      return {
        ...state,
        modal: action.payload,
      };

    case GET_INVITATION_LINK:
      return {
        ...state,
        inviteLink: action.payload,
      };

    case PRIVATE_TRANSACTION_LOADING:
      return {
        ...state,
        privateLoading: !state.privateLoading,
        privateSuccess: false,
      };

    case PRIVATE_TRANSACTION_SUCCESS:
      return {
        ...state,
        privateLoading: false,
        privateError: false,
        privateSuccess: true,
      };

    case PRIVATE_TRANSACTION_ERROR:
      return {
        ...state,
        privateLoading: false,
        privateSuccess: false,
        privateError: true,
      };
    case PRIVATE_TRANSACTION_MODAL_VISIBLE:
      return {
        ...state,
        privateSuccess: false,
        privateError: false,
      };

    // private invitation link
    case GET_INVITATION_LINK_PRIVATE_LOADING:
      return {
        ...state,
        privateLoading: !state.privateLoading,
      };

    case GET_INVITATION_LINK_PRIVATE_SUCCESS:
      return {
        ...state,
        privateLoading: false,
        invitationLinkPrivate: action.payload,
      };

    case GET_INVITATION_LINK_PRIVATE_ERROR:
      return {
        ...state,
        privateLoading: false,
        invitationLinkPrivate: null,
      };
    case SET_PARTICIPANT:
      return {
        ...state,
        participants: action.payload,
      };

    // cancel transaction
    case CANCEL_TRANSACTION_SUCCESS:
      return {
        ...state,
        cancelTransactionLoading: false,
        cancelTransactionSuccess: action.payload,
      };

    case CANCEL_TRANSACTION_ERROR:
      return {
        ...state,
        cancelTransactionLoading: false,
        cancelTransactionSuccess: false,
      };
    case CANCEL_TRANSACTION_LOADING:
      return {
        ...state,
        cancelTransactionLoading: true,
      };

    default:
      return state;
  }
};

export default myTransactionReducer;
