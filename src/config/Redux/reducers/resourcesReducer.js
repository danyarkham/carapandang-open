import {
  GET_RESOURCES_ERROR,
  GET_RESOURCES_LOADING,
  GET_RESOURCES_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  resources: [],
  resource_id: null,
};

const resourcesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RESOURCES_SUCCESS:
      return {
        ...state,
        resources: action.payload,
        isLoading: false,
      };

    case GET_RESOURCES_ERROR:
      return {
        ...state,
        isLoading: false,
        resources: [],
      };

    case GET_RESOURCES_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default resourcesReducer;
