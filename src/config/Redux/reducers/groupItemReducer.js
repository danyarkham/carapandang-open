import {
  GET_GROUP_CLASS_DETAIL,
  GET_GROUP_CLASS_ERROR,
  GET_GROUP_CLASS_FILTER,
  GET_GROUP_CLASS_LOADING,
  GET_GROUP_CLASS_SUCCESS,
  GET_GROUP_CLASS_TOGGLE_FILTER,
  GET_INVITATION_GROUP_LINK_ERROR,
  GET_INVITATION_GROUP_LINK_LOADING,
  GET_INVITATION_GROUP_LINK_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  groups: [],
  groups_filter: [],
  group_invitation_link: [],
  group_detail: {},
  filterVisible: false,
};

const groupItemReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_GROUP_CLASS_DETAIL:
      return {
        ...state,
        group_detail: action.payload,
        isLoading: false,
      };
    case GET_GROUP_CLASS_SUCCESS:
      return {
        ...state,
        groups: action.payload,
        groups_filter: action.payload,
        isLoading: false,
      };
    case GET_GROUP_CLASS_FILTER:
      return {
        ...state,
        groups_filter: action.payload,
        isLoading: false,
      };
    case GET_GROUP_CLASS_ERROR:
      return {
        ...state,
        groups: [],
        groups_filter: [],
        isLoading: false,
      };

    case GET_GROUP_CLASS_LOADING || GET_INVITATION_GROUP_LINK_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case GET_GROUP_CLASS_TOGGLE_FILTER:
      return {
        ...state,
        filterVisible: !state.filterVisible,
      };
    case GET_INVITATION_GROUP_LINK_SUCCESS:
      return {
        ...state,
        group_invitation_link: action.payload,
        isLoading: false,
      };
    case GET_INVITATION_GROUP_LINK_ERROR:
      return {
        ...state,
        group_invitation_link: [],
        isLoading: false,
      };

    default:
      return state;
  }
};

export default groupItemReducer;
