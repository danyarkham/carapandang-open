import {
  GET_LIST_GROUP_DETAIL_ERROR,
  GET_LIST_GROUP_DETAIL_LOADING,
  GET_LIST_GROUP_DETAIL_SUCCESS,
  GET_LIST_GROUP_ERROR,
  GET_LIST_GROUP_HISTORY_ERROR,
  GET_LIST_GROUP_HISTORY_LOADING,
  GET_LIST_GROUP_HISTORY_SUCCESS,
  GET_LIST_GROUP_LOADING,
  GET_LIST_GROUP_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  groups: [],
  group_detail: null,
  group_history: null,
};

const groupClassReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_GROUP_LOADING ||
      GET_LIST_GROUP_DETAIL_LOADING ||
      GET_LIST_GROUP_HISTORY_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case GET_LIST_GROUP_SUCCESS:
      return {
        ...state,
        groups: action.payload,
        isLoading: false,
      };
    case GET_LIST_GROUP_ERROR:
      return {
        ...state,
        groups: [],
        isLoading: false,
      };

    case GET_LIST_GROUP_DETAIL_SUCCESS:
      return {
        ...state,
        group_detail: action.payload,
        isLoading: false,
      };
    case GET_LIST_GROUP_DETAIL_ERROR:
      return {
        ...state,
        group_detail: null,
        isLoading: false,
      };

    case GET_LIST_GROUP_HISTORY_SUCCESS:
      return {
        ...state,
        group_history: action.payload,
        isLoading: false,
      };
    case GET_LIST_GROUP_HISTORY_ERROR:
      return {
        ...state,
        group_history: null,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default groupClassReducer;
