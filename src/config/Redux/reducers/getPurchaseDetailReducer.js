import {
  CREATE_TRANSACTION_GROUP_ERROR,
  CREATE_TRANSACTION_GROUP_LOADING,
  CREATE_TRANSACTION_GROUP_SUCCESS,
  GET_PURCHASE_DETAIL_ERROR,
  GET_PURCHASE_DETAIL_LOADING,
  GET_PURCHASE_DETAIL_SELECT_PAYMENT,
  GET_PURCHASE_DETAIL_SUCCESS,
  UPLOAD_CONFIRM_PAYMENT_ERROR,
  UPLOAD_CONFIRM_PAYMENT_LOADING,
  UPLOAD_CONFIRM_PAYMENT_MODAL,
  UPLOAD_CONFIRM_PAYMENT_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  purchase: {},
  payment_method: null,
  transaction_detail: null,
  transaction: false,
  showModal: false,
};

const getPurchaseDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PURCHASE_DETAIL_SUCCESS:
      return {
        ...state,
        purchase: action.payload,
        isLoading: false,
      };
    case GET_PURCHASE_DETAIL_ERROR:
      return {
        ...state,
        purchase: null,
        isLoading: false,
      };

    case GET_PURCHASE_DETAIL_SELECT_PAYMENT:
      return {
        ...state,
        payment_method: action.payload,
      };
    case GET_PURCHASE_DETAIL_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    // payment code
    case CREATE_TRANSACTION_GROUP_SUCCESS:
      return {
        ...state,
        transaction_detail: action.payload,
        isLoading: false,
        transaction: true,
      };
    case CREATE_TRANSACTION_GROUP_ERROR:
      return {
        ...state,
        transaction_detail: null,
        isLoading: false,
        transaction: false,
      };
    case CREATE_TRANSACTION_GROUP_LOADING:
      return {
        ...state,
        isLoading: true,
        transaction: false,
      };

    // Confirm Payment
    case UPLOAD_CONFIRM_PAYMENT_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case UPLOAD_CONFIRM_PAYMENT_ERROR:
      return {
        ...state,
        isLoading: false,
      };
    case UPLOAD_CONFIRM_PAYMENT_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case UPLOAD_CONFIRM_PAYMENT_MODAL:
      return {
        ...state,
        showModal: !state.showModal,
      };

    default:
      return state;
  }
};

export default getPurchaseDetailReducer;
