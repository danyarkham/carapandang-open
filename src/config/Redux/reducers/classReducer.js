import {
  GET_WHITEBOARD_ERROR,
  GET_WHITEBOARD_LOADING,
  GET_WHITEBOARD_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  whiteBoard: null,
};

const classReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_WHITEBOARD_SUCCESS:
      return {
        ...state,
        whiteBoard: action.payload,
        isLoading: false,
      };
    case GET_WHITEBOARD_ERROR:
      return {
        ...state,
        whiteBoard: null,
        isLoading: false,
      };

    case GET_WHITEBOARD_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default classReducer;
