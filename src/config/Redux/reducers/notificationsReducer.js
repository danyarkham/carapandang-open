import {
  GET_NOTIFICATIONS_LIST_ERROR,
  GET_NOTIFICATIONS_LIST_LOADING,
  GET_NOTIFICATIONS_LIST_SUCCESS,
  GET_NOTIFICATION_CREDIT_ERROR,
  GET_NOTIFICATION_CREDIT_LOADING,
  GET_NOTIFICATION_CREDIT_SUCCESS,
} from '../../Constant';

const initialState = {
  isLoading: false,
  notifications: [],
  notification_credit: {},
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTIFICATIONS_LIST_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading,
      };

    case GET_NOTIFICATIONS_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        notifications: action.payload,
      };

    case GET_NOTIFICATIONS_LIST_ERROR:
      return {
        ...state,
        isLoading: false,
        notifications: [],
      };

    // detail
    case GET_NOTIFICATION_CREDIT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        notification_credit: action.payload,
      };

    case GET_NOTIFICATION_CREDIT_ERROR:
      return {
        ...state,
        isLoading: false,
        notification_credit: {},
      };

    case GET_NOTIFICATION_CREDIT_LOADING:
      return {
        ...state,
        isLoading: true,
      };

    default:
      return state;
  }
};

export default notificationReducer;
