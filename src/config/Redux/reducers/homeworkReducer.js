import {
  GET_HOMEWORK_QUESTION,
  GET_HOMEWORK_QUESTION_ERROR,
  GET_HOMEWORK_QUESTION_LOADING,
  GET_HOMEWORK_QUESTION_SUCCESS,
  GET_PRIVATE_HOMEWORK_ERROR,
  GET_PRIVATE_HOMEWORK_LOADING,
  GET_PRIVATE_HOMEWORK_SUCCESS,
  UPLOAD_HOMEWORK_ERROR,
  UPLOAD_HOMEWORK_LOADING,
  UPLOAD_HOMEWORK_PRIVATE_ERROR,
  UPLOAD_HOMEWORK_PRIVATE_LOADING,
  UPLOAD_HOMEWORK_PRIVATE_SUCCESS,
  UPLOAD_HOMEWORK_SUCCESS,
} from '../../Constant';

const initialState = {
  data: [],
  homeworkData: null,
  homeworkDataPrivate: null,
  isLoading: false,
  homeworkLoading: false,
  homework: null,
  homeworkPrivate: null,
};

const homeworkReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_HOMEWORK_QUESTION:
      return {
        ...state,
        data: action.payload,
      };

    case GET_HOMEWORK_QUESTION_SUCCESS:
      return {
        ...state,
        homeworkData: action.payload,
        isLoading: false,
      };
    case GET_HOMEWORK_QUESTION_LOADING:
      return {
        ...state,
        homeworkData: null,
        isLoading: true,
      };
    case GET_HOMEWORK_QUESTION_ERROR:
      return {
        ...state,
        homeworkData: null,
        isLoading: false,
      };

    case GET_PRIVATE_HOMEWORK_SUCCESS:
      return {
        ...state,
        homeworkDataPrivate: action.payload,
        isLoading: false,
      };
    case GET_PRIVATE_HOMEWORK_LOADING:
      return {
        ...state,
        homeworkDataPrivate: null,
        isLoading: true,
      };
    case GET_PRIVATE_HOMEWORK_ERROR:
      return {
        ...state,
        homeworkDataPrivate: null,
        isLoading: false,
      };

    case UPLOAD_HOMEWORK_SUCCESS:
      return {
        ...state,
        homework: action.payload,
        homeworkLoading: false,
      };
    case UPLOAD_HOMEWORK_LOADING:
      return {
        ...state,
        homework: null,
        homeworkLoading: true,
      };
    case UPLOAD_HOMEWORK_ERROR:
      return {
        ...state,
        homework: null,
        homeworkLoading: false,
      };

    // uplaod private homework
    case UPLOAD_HOMEWORK_PRIVATE_SUCCESS:
      return {
        ...state,
        homeworkPrivate: action.payload,
        homeworkLoading: false,
      };
    case UPLOAD_HOMEWORK_PRIVATE_LOADING:
      return {
        ...state,
        homeworkPrivate: null,
        homeworkLoading: true,
      };
    case UPLOAD_HOMEWORK_PRIVATE_ERROR:
      return {
        ...state,
        homeworkPrivate: null,
        homeworkLoading: false,
      };

    default:
      return state;
  }
};

export default homeworkReducer;
