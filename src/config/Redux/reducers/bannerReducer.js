import {
  GET_BANNER_DETAIL_LOADING,
  GET_BANNER_DETAIL_SUCCESS,
  GET_BANNER_LOADING,
  GET_BANNER_SUCCESS,
} from '../../Constant';

const initialState = {
  banners: [],
  banners_detail: null,
  banner_loading: false,
};

const bannerReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BANNER_SUCCESS:
      return {
        ...state,
        banners: action.payload,
        banner_loading: false,
      };

    case GET_BANNER_LOADING:
      return {
        ...state,
        banner_loading: action.payload,
      };

    case GET_BANNER_DETAIL_SUCCESS:
      return {
        ...state,
        banners_detail: action.payload,
        banner_loading: false,
      };

    case GET_BANNER_DETAIL_LOADING:
      return {
        ...state,
        banner_loading: action.payload,
      };

    default:
      return state;
  }
};

export default bannerReducer;
