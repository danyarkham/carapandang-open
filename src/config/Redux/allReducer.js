import {combineReducers} from 'redux';
import ReducerLogin from '../../Screen/Login/reducerLogin';
import ReducerSignUp from '../../Screen/SignUp/reducerSignUp';
import ReducerHome from '../../Screen/Home/reducerHome';
import ReducerMyClass from '../../Screen/MyClass/reducerMyClass';
import ReducerForgotPassword from './reducers/passwordResetReducer';
import LanguageReducer from './reducers/languageReducer';
import ApplicationReducer from './reducers/applicationLevelReducer';
import ReducerBanner from './reducers/bannerReducer';
import ProfileReducer from './reducers/updateProfileReducer';
import PhoneNumberReducer from './reducers/updatePhoneNumberReducer';
import PasswordReducer from './reducers/updatePassword';
import EmailReducer from './reducers/updateEmailReducer';
import ProfilePhotoReducer from './reducers/updateProfilePhotoReducer';
import GroupItemReducer from './reducers/groupItemReducer';
import ScheduleRequestReducer from './reducers/scheduleRequestReducer';
import GetPurchaseDetailReducer from './reducers/getPurchaseDetailReducer';
import FilterReducer from './reducers/filterReducer';
import MyTransactionReducer from './reducers/myTransactionReducer';
import NotificationReducer from './reducers/notificationsReducer';
import ResourcesReducer from './reducers/resourcesReducer';
import homeworkReducer from './reducers/homeworkReducer';
import classReducer from './reducers/classReducer';
import creditReducer from './reducers/creditReducer';
import instructorReducer from './reducers/instructorReducer';
import reportAndCertificateReducer from './reducers/reportAndCertificateReducer';
import myClassReducer from './reducers/myClassReducer';
import voucherReducer from './reducers/voucherReducer';
import paymentListReducer from './reducers/paymentListReducer';
import groupClassReducer from './reducers/groupClassReducer';
import eventReducer from './reducers/eventReducer';

export const allReducer = combineReducers({
  ReducerLogin,
  ReducerSignUp,
  ReducerHome,
  ReducerMyClass,
  ReducerForgotPassword,
  LanguageReducer,
  ApplicationReducer,
  ReducerBanner,
  ProfileReducer,
  PhoneNumberReducer,
  PasswordReducer,
  EmailReducer,
  ProfilePhotoReducer,
  groupItem: GroupItemReducer,
  scheduleRequest: ScheduleRequestReducer,
  purchaseDetail: GetPurchaseDetailReducer,
  filter: FilterReducer,
  transaction: MyTransactionReducer,
  notification: NotificationReducer,
  resource: ResourcesReducer,
  homework: homeworkReducer,
  class: classReducer,
  credit: creditReducer,
  instructor: instructorReducer,
  report: reportAndCertificateReducer,
  private: myClassReducer,
  voucher: voucherReducer,
  payment: paymentListReducer,
  groupClass: groupClassReducer,
  event: eventReducer,
});
