import {all} from 'redux-saga/effects';
//saga screen here
import sagaLogin from '../../Screen/Login/sagaLogin';
import sagaSignUp from '../../Screen/SignUp/sagaSignUp';
import applicationLevel from './sagas/applicationLevelSaga';
import bannerSaga from './sagas/bannerSagas';
import getGroupItemSaga from './sagas/getGroupItemSaga';
import getLevelCategorySaga from './sagas/getLevelCategorySagas';
import getMasterGroupSaga from './sagas/getMasterGroupSaga';
import createTransactionGroupSaga from './sagas/createTransactionGroupSaga';
import getPurchaseDetailSaga from './sagas/getPurchaseDetailSaga';
import getScheduleRequestSaga from './sagas/getScheduleRequestSaga';
import getLanguageSelectedSaga from './sagas/getSelectedLanguageSagas';
import getTypeCategorySaga from './sagas/getTypeCategorySaga';
import languageSaga from './sagas/laguageSagas';
import passwordResetSaga from './sagas/passwordResetSaga';
import submitScheduleRequestSaga from './sagas/submitScheduleRequestSaga';
import updateEmailSaga from './sagas/updateEmailSaga';
import updatePasswordSaga from './sagas/updatePasswordSaga';
import updatePhoneNumberSaga from './sagas/updatePhoneNumber';
import updateProfile from './sagas/updateProfile';
import updateProfilePhotoSaga from './sagas/updateProfilePhotoSaga';
import uploadConfirmPaymentSaga from './sagas/uploadConfirmPaymentSaga';
import levelCategorySaga from './sagas/filter/levelCategorySaga';
import typeCategorySaga from './sagas/filter/typeCategorySaga';
import getMyTransactionSaga from './sagas/getMyTransactionSaga';
import cancelScheduleRequestSaga from './sagas/cancelScheduleRequestSaga';
import updateLanguageSaga from './sagas/updateLanguageSaga';
import updateDeviceIdSaga from './sagas/user/updateDeviceIdSaga';
import sendWelcomeMessageSaga from './sagas/user/sendWelcomeMessageSaga';
import getUserProfileSaga from './sagas/user/getUserProfileSaga';
import notificationSaga from './sagas/notificationSaga';
import notificationUpdateSaga from './sagas/notificationUpdateSaga';
import getMyTransactionDetailSaga from './sagas/getMyTransactionDetailSaga';
import getGroupInvitationLinkSaga from './sagas/group/getGroupInvitationLinkSaga';
import getHomeworkQuestionSaga from './sagas/getHomeworkQuestionSaga';
import saveWhiteBoardSaga from './sagas/saveWhiteBoardSaga';
import getResourcesSaga from './sagas/getResourcesSaga';
import getWhiteBoardSaga from './sagas/getWhiteBoardSaga';
import uploadHomeworkSaga from './sagas/uploadHomeworkSaga';
import verifyPhoneNumberSaga from './sagas/verifyPhoneNumberSaga';
import getMasterCreditSaga from './sagas/credit/masterCreditSaga';
import getInstructorSaga from './sagas/instructor/getInstructorSaga';
import getInstructorScheduleSaga from './sagas/instructor/getInstructorScheduleSaga';
import createCreditTransactionSaga from './sagas/credit/createCreditTransactionSaga';
import confirmCreditTransactionSaga from './sagas/credit/confirmCreditTransactionSaga';
import getMyPrivateCreditSaga from './sagas/credit/getMyPrivateCreditSaga';
import getWhitelistSaga from './sagas/instructor/getWhitelistSaga';
import addInstructorToWhitelistSaga from './sagas/instructor/addInstructorToWhitelistSaga';
import removeInstructorFromWhitelistSaga from './sagas/instructor/removeInstructorFromWhitelistSaga';
import getReportsSaga from './sagas/reportAndCertificate/getReportsSaga';
import getCertificateSaga from './sagas/reportAndCertificate/getCertificateSaga';
import privateTransactionSaga from './sagas/privateTransactionSaga';
import getHistoryCreditSaga from './sagas/credit/getHistoryCreditSaga';
import getCreditAvailableSaga from './sagas/credit/getCreditAvailableSaga';
import getMyPrivateCreditExpiredSaga from './sagas/credit/getMyPrivateCreditExpiredSaga';
import getPrivateClassSaga from './sagas/myClass/getPrivateClassSaga';
import getPrivateWhiteboardSaga from './sagas/getPrivateWhiteboardSaga';
import rateInstructorSaga from './sagas/instructor/rateInstructorSaga';
import getInvitationLinkPrivateSaga from './sagas/getInvitationLinkPrivateSaga';
import getPrivateHomeworkSaga from './sagas/getPrivateHomeworkSaga';
import uploadPrivateHomeworkSaga from './sagas/uploadHomeworkPrivateSaga';
import getNotificationCreditSaga from './sagas/Notification/getNotificationCreditSaga';
import applyVoucherSaga from './sagas/voucher/applyVoucherSaga';
import getVoucherSaga from './sagas/voucher/getVoucherSaga';
import getPaymentMethodListSaga from './sagas/Payment/getPaymentMethodListSaga';
import getListGroupSaga from './sagas/myClass/Group/getListGroupSaga';
import getListGroupDetailSaga from './sagas/myClass/Group/getListGroupDetailSaga';
import getListGroupHistorySaga from './sagas/myClass/Group/getListGroupHistorySaga';
import cancelTransactionSaga from './sagas/cancelTransactionSaga';
import bannerDetailSaga from './sagas/bannerDetailSagas';
import getVoucherRefernEarnSaga from './sagas/voucher/getVoucherRefernEarnSaga';

export function* SagaWatcher() {
  yield all([
    sagaLogin(),
    sagaSignUp(),
    passwordResetSaga(),
    languageSaga(),
    applicationLevel(),
    bannerSaga(),
    getLanguageSelectedSaga(),
    updateProfile(),
    updatePhoneNumberSaga(),
    updatePasswordSaga(),
    updateEmailSaga(),
    updateProfilePhotoSaga(),
    verifyPhoneNumberSaga(),
    getGroupItemSaga(),
    getTypeCategorySaga(),
    getLevelCategorySaga(),
    getMasterGroupSaga(),
    submitScheduleRequestSaga(),
    getScheduleRequestSaga(),
    getPurchaseDetailSaga(),
    createTransactionGroupSaga(),
    uploadConfirmPaymentSaga(),
    typeCategorySaga(),
    levelCategorySaga(),
    getMyTransactionSaga(),
    cancelScheduleRequestSaga(),
    updateLanguageSaga(),
    updateDeviceIdSaga(),
    sendWelcomeMessageSaga(),
    getUserProfileSaga(),
    notificationSaga(),
    notificationUpdateSaga(),
    getMyTransactionDetailSaga(),
    getGroupInvitationLinkSaga(),
    getHomeworkQuestionSaga(),
    saveWhiteBoardSaga(),
    getResourcesSaga(),
    getWhiteBoardSaga(),
    getPrivateWhiteboardSaga(),
    uploadHomeworkSaga(),
    getMasterCreditSaga(),
    getInstructorSaga(),
    getInstructorScheduleSaga(),
    createCreditTransactionSaga(),
    confirmCreditTransactionSaga(),
    getHistoryCreditSaga(),
    getMyPrivateCreditSaga(),
    getMyPrivateCreditExpiredSaga(),
    getWhitelistSaga(),
    addInstructorToWhitelistSaga(),
    removeInstructorFromWhitelistSaga(),
    getReportsSaga(),
    getCertificateSaga(),
    privateTransactionSaga(),
    getCreditAvailableSaga(),
    getPrivateClassSaga(),
    rateInstructorSaga(),
    getInvitationLinkPrivateSaga(),
    getPrivateHomeworkSaga(),
    uploadPrivateHomeworkSaga(),
    getNotificationCreditSaga(),
    getVoucherSaga(),
    applyVoucherSaga(),
    getPaymentMethodListSaga(),
    getListGroupSaga(),
    getListGroupDetailSaga(),
    getListGroupHistorySaga(),
    cancelTransactionSaga(),
    bannerDetailSaga(),
    getVoucherRefernEarnSaga(),
  ]);
}
