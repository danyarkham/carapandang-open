import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_RESOURCES} from '../../Constant';
import {
  getResourcesError,
  getResourcesLoading,
  getResourcesSuccess,
} from '../actions/getResourcesAction';

// get all language
function* getResourcesData({payload}) {
  yield put(getResourcesLoading());
  try {
    const res = yield axios.get(`${API_URL}/${payload}`);
    console.log(res);
    yield put(getResourcesSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getResourcesError());
  }
}

function* getResourcesSaga() {
  yield takeLatest(GET_RESOURCES, getResourcesData);
}

export default getResourcesSaga;
