import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {updateUserLogin} from '../../../Screen/Login/actionLogin';
import {API_URL, VERIFY_PHONE_NUMBER} from '../../Constant';
import {
  verifyPhoneNumberError,
  verifyPhoneNumberLoading,
  verifyPhoneNumberSuccess,
} from '../actions/updatePhoneAction';

const setProfile = async user => {
  await AsyncStorage.setItem('user', JSON.stringify(user));
};

function* verifyPhoneNumberPhoto({payload}) {
  yield put(verifyPhoneNumberLoading(true));
  try {
    const res = yield axios.post(`${API_URL}/user/phone-verify`, payload);
    console.log(res);
    setProfile(res.data.data);
    yield put(verifyPhoneNumberSuccess(res.data.data));
    yield put(
      updateUserLogin({phone_verified_at: res.data?.data?.phone_verified_at}),
    );
    // navigate('EditProfile');
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(verifyPhoneNumberError());
  }
}

function* verifyPhoneNumberSaga() {
  yield takeLatest(VERIFY_PHONE_NUMBER, verifyPhoneNumberPhoto);
}

export default verifyPhoneNumberSaga;
