import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_NOTIFICATIONS_LIST} from '../../Constant';
import {
  getNotificationListError,
  getNotificationListLoading,
  getNotificationListSuccess,
} from '../actions/getNotificationsAction';

// get all language
function* notificationData() {
  yield put(getNotificationListLoading());
  try {
    const res = yield axios.get(`${API_URL}/notifications/all`);
    yield put(getNotificationListSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getNotificationListError());
  }
}

function* notificationSaga() {
  yield takeLatest(GET_NOTIFICATIONS_LIST, notificationData);
}

export default notificationSaga;
