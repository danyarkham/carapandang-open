import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, PRIVATE_TRANSACTION} from '../../Constant';
import {getCreditAvailable} from '../actions/creditAction';
import {
  privateTransactionError,
  privateTransactionLoading,
  privateTransactionSuccess,
} from '../actions/privateTransactionAction';

// get all language
function* privateTransactionData({payload}) {
  yield put(privateTransactionLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/private_transaction/book-class`,
      payload,
    );
    console.log('privateTransactionData', res);
    yield put(privateTransactionSuccess(res.data.data));
    yield put(getCreditAvailable());
  } catch (e) {
    console.log('privateTransactionData', e.response);
    yield put(privateTransactionError());
  }
}

function* privateTransactionSaga() {
  yield takeLatest(PRIVATE_TRANSACTION, privateTransactionData);
}

export default privateTransactionSaga;
