import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_PRIVATE_HOMEWORK} from '../../Constant';
import {
  getPrivateHomeworkError,
  getPrivateHomeworkLoading,
  getPrivateHomeworkSuccess,
} from '../actions/privateTransactionAction';

// get all language
function* getPrivateHomeworkData({payload}) {
  yield put(getPrivateHomeworkLoading());
  try {
    const res = yield axios.get(`${API_URL}/homework/private`);
    console.log(res);
    yield put(getPrivateHomeworkSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getPrivateHomeworkError());
  }
}

function* getPrivateHomeworkSaga() {
  yield takeLatest(GET_PRIVATE_HOMEWORK, getPrivateHomeworkData);
}

export default getPrivateHomeworkSaga;
