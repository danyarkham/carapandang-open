import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_LEVEL_CATEGORY} from '../../Constant';
import {
  getLevelCategoryError,
  getLevelCategoryLoading,
  getLevelCategorySuccess,
} from '../actions/getLevelCategoryAction';

// get all language
function* getLevelCategory({payload}) {
  yield put(getLevelCategoryLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/schedule_request/get_level_category`,
      payload,
    );
    if (res.status === 200) {
      yield put(getLevelCategorySuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
    yield put(getLevelCategoryError());
  }
}

function* getLevelCategorySaga() {
  yield takeLatest(GET_LEVEL_CATEGORY, getLevelCategory);
}

export default getLevelCategorySaga;
