import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPLOAD_CONFIRM_PAYMENT} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {getMyTransaction} from '../actions/getMyTransactionAction';
import {getScheduleRequest} from '../actions/getScheduleRequestAction';
import {
  uploadConfirmPaymentError,
  uploadConfirmPaymentLoading,
  uploadConfirmPaymentModal,
  uploadConfirmPaymentSuccess,
} from '../actions/uploadConfirmPayment';

function* uploadConfirmPaymentData({payload}) {
  yield put(uploadConfirmPaymentLoading(true));
  try {
    const header = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    const res = yield axios.post(
      `${API_URL}/confirm_payment/insert`,
      payload.form,
      header,
    );
    yield put(uploadConfirmPaymentSuccess(res.data.data));
    yield put(uploadConfirmPaymentModal());
    yield put(getScheduleRequest());
    yield put(getMyTransaction());
    // if (payload.transaction) {

    // } else {
    //   navigate('ScheduleRequest', {screen: 'Request List'});
    // }
    // Snackbar.show({
    //   text: res?.data?.message,
    //   duration: Snackbar.LENGTH_LONG,
    //   backgroundColor: '#27ae60',
    // });
    console.log(res);
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(uploadConfirmPaymentError());
  }
}

function* uploadConfirmPaymentSaga() {
  yield takeLatest(UPLOAD_CONFIRM_PAYMENT, uploadConfirmPaymentData);
}

export default uploadConfirmPaymentSaga;
