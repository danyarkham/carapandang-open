import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_BANNER} from '../../Constant';
import {getBannerLoading, getBannerSuccess} from '../actions/bannerAction';

function* getBannerData() {
  yield put(getBannerLoading(true));
  try {
    const res = yield axios.get(`${API_URL}/banner/all`);
    console.log(res);
    if (res.status === 200) {
      yield put(getBannerSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
    yield put(getBannerLoading(false));
  }
}

function* bannerSaga() {
  yield takeLatest(GET_BANNER, getBannerData);
}

export default bannerSaga;
