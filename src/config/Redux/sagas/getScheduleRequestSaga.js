import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_SCHEDULE_REQUEST} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  getScheduleRequestError,
  getScheduleRequestLoading,
  getScheduleRequestSuccess,
} from '../actions/getScheduleRequestAction';

// get all language
function* getScheduleRequestData() {
  yield put(getScheduleRequestLoading());
  try {
    const res = yield axios.get(`${API_URL}/schedule_request/all`);
    if (res.status === 200) {
      yield put(getScheduleRequestSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
    // Snackbar.show({
    //   text: 'Something when wrong, please try again',
    //   duration: Snackbar.LENGTH_LONG,
    //   backgroundColor: '#E2494B',
    // });
    yield put(getScheduleRequestError());
  }
}

function* getScheduleRequestSaga() {
  yield takeLatest(GET_SCHEDULE_REQUEST, getScheduleRequestData);
}

export default getScheduleRequestSaga;
