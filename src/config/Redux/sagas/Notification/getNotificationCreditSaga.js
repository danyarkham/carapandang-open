import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_NOTIFICATION_CREDIT} from '../../../Constant';
import {
  getNotificationCreditError,
  getNotificationCreditLoading,
  getNotificationCreditSuccess,
} from '../../actions/notificationAction';

// get all language
function* getNotificationCreditData({payload}) {
  yield put(getNotificationCreditLoading());
  try {
    const res = yield axios.get(`${API_URL}/credit/detail/${payload}`);
    console.log(res);
    yield put(getNotificationCreditSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getNotificationCreditError());
  }
}

function* getNotificationCreditSaga() {
  yield takeLatest(GET_NOTIFICATION_CREDIT, getNotificationCreditData);
}

export default getNotificationCreditSaga;
