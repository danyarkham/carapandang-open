import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPDATE_PHONE_NUMBER} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  updatePhoneNumberError,
  updatePhoneNumberLoading,
  updatePhoneNumberModal,
  updatePhoneNumberSuccess,
} from '../actions/updatePhoneAction';

function* updateUserPhoneNumber(data) {
  yield put(updatePhoneNumberLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/user/update-phone-number`,
      data.payload,
    );
    console.log(res);
    yield put(updatePhoneNumberSuccess(res.data.data));
    yield put(updatePhoneNumberModal(false));
    // navigate('EditProfile');
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(updatePhoneNumberError());
  }
}

function* updatePhoneNumberSaga() {
  yield takeLatest(UPDATE_PHONE_NUMBER, updateUserPhoneNumber);
}

export default updatePhoneNumberSaga;
