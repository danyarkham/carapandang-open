import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_INVITATION_LINK_PRIVATE} from '../../Constant';
import {
  getInvitationLinkPrivateError,
  getInvitationLinkPrivateLoading,
  getInvitationLinkPrivateSuccess,
} from '../actions/privateTransactionAction';

function* getInvitationLinkPrivateData() {
  yield put(getInvitationLinkPrivateLoading());
  try {
    const res = yield axios.get(
      `${API_URL}/private_transaction/invitation_link`,
    );
    console.log(res);
    yield put(getInvitationLinkPrivateSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getInvitationLinkPrivateError());
  }
}

function* getInvitationLinkPrivateSaga() {
  yield takeLatest(GET_INVITATION_LINK_PRIVATE, getInvitationLinkPrivateData);
}

export default getInvitationLinkPrivateSaga;
