import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_PAYMENT_METHOD_LIST} from '../../../Constant';
import {
  getPaymentMethodListError,
  getPaymentMethodListLoading,
  getPaymentMethodListSuccess,
} from '../../actions/paymentMethodList';

// get all language
function* getPaymentMethodListData() {
  yield put(getPaymentMethodListLoading());
  try {
    const res = yield axios.get(`${API_URL}/payment-method/list`);
    console.log(res);
    yield put(getPaymentMethodListSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getPaymentMethodListError());
  }
}

function* getPaymentMethodListSaga() {
  yield takeLatest(GET_PAYMENT_METHOD_LIST, getPaymentMethodListData);
}

export default getPaymentMethodListSaga;
