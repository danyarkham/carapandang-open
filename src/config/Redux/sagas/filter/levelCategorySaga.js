import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_ALL_LEVEL} from '../../../Constant';
import {
  levelCategoryError,
  levelCategoryLoading,
  levelCategorySuccess,
} from '../../actions/filterAction';

// get all language
function* levelCategoryData() {
  yield put(levelCategoryLoading());
  try {
    const res = yield axios.get(`${API_URL}/filter/level`);
    console.log(res);
    yield put(levelCategorySuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(levelCategoryError());
  }
}

function* levelCategorySaga() {
  yield takeLatest(GET_ALL_LEVEL, levelCategoryData);
}

export default levelCategorySaga;
