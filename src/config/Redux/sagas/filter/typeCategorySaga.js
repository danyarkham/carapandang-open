import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_ALL_TYPE} from '../../../Constant';
import {
  typeCategoryError,
  typeCategoryLoading,
  typeCategorySuccess,
} from '../../actions/filterAction';

// get all language
function* typeCategoryData() {
  yield put(typeCategoryLoading());
  try {
    const res = yield axios.get(`${API_URL}/filter/type`);
    console.log(res);
    yield put(typeCategorySuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(typeCategoryError());
  }
}

function* typeCategorySaga() {
  yield takeLatest(GET_ALL_TYPE, typeCategoryData);
}

export default typeCategorySaga;
