import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest, takeEvery} from 'redux-saga/effects';
import {API_URL, GET_WHITEBOARD} from '../../Constant';
import {
  getWhiteBoardError,
  getWhiteBoardLoading,
  getWhiteBoardSuccess,
} from '../actions/saveWhiteBoardAction';

// get all language
function* getWhiteBoardData({payload}) {
  yield put(getWhiteBoardLoading());
  try {
    const res = yield axios.get(
      `${API_URL}/my_transaction/whiteboard/${payload}`,
    );
    console.log(res);
    yield put(getWhiteBoardSuccess(res.data.data.whiteboard));
  } catch (e) {
    console.log(e.response);
    yield put(getWhiteBoardError());
  }
}

function* getWhiteBoardSaga() {
  yield takeEvery(GET_WHITEBOARD, getWhiteBoardData);
}

export default getWhiteBoardSaga;
