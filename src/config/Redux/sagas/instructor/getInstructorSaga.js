import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_INSTRUCTOR} from '../../../Constant';
import {
  getInstructorError,
  getInstructorLoading,
  getInstructorSuccess,
} from '../../actions/instructorAction';

// get all language
function* getInstructorData() {
  yield put(getInstructorLoading());
  try {
    const res = yield axios.get(`${API_URL}/instructor/all`);
    console.log(res);
    yield put(getInstructorSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getInstructorError());
  }
}

function* getInstructorSaga() {
  yield takeLatest(GET_INSTRUCTOR, getInstructorData);
}

export default getInstructorSaga;
