import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {ADD_INSTRUCTOR_TO_WHITELIST, API_URL} from '../../../Constant';
import {
  addInstructorToWhitelistError,
  addInstructorToWhitelistLoading,
  addInstructorToWhitelistSuccess,
} from '../../actions/instructorAction';

// get all language
function* addInstructorToWhitelistData({payload}) {
  yield put(addInstructorToWhitelistLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/instructor/add-whitelist`,
      payload,
    );
    console.log(res);
    yield put(addInstructorToWhitelistSuccess(res.data.data));
    Snackbar.show({
      text:
        res?.data?.message === 'Successfully add instructor to whitelist'
          ? 'Instructor Successfully Saved in Wishlist!'
          : res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(addInstructorToWhitelistError());
    if (e.response) {
      Snackbar.show({
        text: e.response?.data?.message,
        duration: 3000,
        backgroundColor: '#E2494B',
      });
    }
  }
}

function* addInstructorToWhitelistSaga() {
  yield takeLatest(ADD_INSTRUCTOR_TO_WHITELIST, addInstructorToWhitelistData);
}

export default addInstructorToWhitelistSaga;
