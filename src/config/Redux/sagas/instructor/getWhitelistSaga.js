import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_WHITE_LIST} from '../../../Constant';
import {
  getWhiteListError,
  getWhiteListLoading,
  getWhiteListSuccess,
} from '../../actions/instructorAction';

// get all language
function* getWhitelistData() {
  yield put(getWhiteListLoading());
  try {
    const res = yield axios.get(`${API_URL}/instructor/whitelist`);
    console.log(res);
    yield put(getWhiteListSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getWhiteListError());
  }
}

function* getWhitelistSaga() {
  yield takeLatest(GET_WHITE_LIST, getWhitelistData);
}

export default getWhitelistSaga;
