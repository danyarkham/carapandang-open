import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_INSTRUCTOR_SCHEDULE} from '../../../Constant';
import {
  getInstructorScheduleError,
  getInstructorScheduleLoading,
  getInstructorScheduleSuccess,
} from '../../actions/instructorAction';

// get all language
function* getInstructorScheduleData() {
  yield put(getInstructorScheduleLoading());
  try {
    const res = yield axios.get(`${API_URL}/instructor_schedule/schedule`);
    console.log(res);
    yield put(getInstructorScheduleSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getInstructorScheduleError());
  }
}

function* getInstructorScheduleSaga() {
  yield takeLatest(GET_INSTRUCTOR_SCHEDULE, getInstructorScheduleData);
}

export default getInstructorScheduleSaga;
