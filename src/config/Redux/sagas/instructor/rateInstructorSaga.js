import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, RATE_INSTRUCTOR} from '../../../Constant';
import {
  rateInstructorError,
  rateInstructorLoading,
  rateInstructorSuccess,
} from '../../actions/instructorAction';

// get all language
function* rateInstructorData({payload}) {
  yield put(rateInstructorLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/instructor/comment/insert`,
      payload,
    );
    console.log(res);
    yield put(rateInstructorSuccess(res.data.data));
  } catch (e) {
    console.log('rateInstructorData', e);
    console.log('rateInstructorData', e.response);
    yield put(rateInstructorError());
  }
}

function* rateInstructorSaga() {
  yield takeLatest(RATE_INSTRUCTOR, rateInstructorData);
}

export default rateInstructorSaga;
