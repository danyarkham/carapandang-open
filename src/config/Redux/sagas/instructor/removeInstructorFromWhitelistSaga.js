import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, REMOVE_INSTRUCTOR_FROM_WHITELIST} from '../../../Constant';
import {
  removeInstructorFromWhitelistError,
  removeInstructorFromWhitelistLoading,
  removeInstructorFromWhitelistSuccess,
} from '../../actions/instructorAction';

// get all language
function* removeInstructorFromWhitelistData({payload}) {
  yield put(removeInstructorFromWhitelistLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/instructor/delete-whitelist`,
      payload,
    );
    console.log(res.data.message);
    yield put(removeInstructorFromWhitelistSuccess(res.data.data));
    Snackbar.show({
      text:
        res?.data?.message === 'Successfully delete instructor from whitelist'
          ? 'You Removed Instructor from Wishlist!'
          : res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(removeInstructorFromWhitelistError());
    if (e.response) {
      Snackbar.show({
        text: e.response?.data?.message,
        duration: 3000,
        backgroundColor: '#E2494B',
      });
    }
  }
}

function* removeInstructorFromWhitelistSaga() {
  yield takeLatest(
    REMOVE_INSTRUCTOR_FROM_WHITELIST,
    removeInstructorFromWhitelistData,
  );
}

export default removeInstructorFromWhitelistSaga;
