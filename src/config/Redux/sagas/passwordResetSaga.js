import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {FORGOT_PASSWORD} from '../../Constant';
import {
  sendEmailError,
  sendEmailSuccess,
  setLoading,
} from '../actions/passwordResetAction';

function* sendEmail(action) {
  console.log(action);
  yield put(setLoading());
  try {
    const res = yield axios({
      method: 'post',
      url: 'https://linquis.com/api/web-app-linquis/api/auth/forgotpassword',
      data: action.payload,
    });
    if (res.status === 200) {
      yield put(sendEmailSuccess(res.data));
    }
  } catch (e) {
    yield put(sendEmailError(false));
    if (e.response) {
      console.log(e.response);
      const {error} = e.response.data;
      Snackbar.show({
        text: error,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
      return;
    }
    Snackbar.show({
      text: 'Email not found',
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#E2494B',
    });
  }
}

function* passwordResetSaga() {
  yield takeLatest(FORGOT_PASSWORD, sendEmail);
}

export default passwordResetSaga;
