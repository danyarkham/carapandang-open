import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPDATE_PASSWORD} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  updatePasswordError,
  updatePasswordLoading,
  updatePasswordSuccess,
} from '../actions/updatePasswordAction';

function* updateUserPassword(data) {
  yield put(updatePasswordLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/user/update-password`,
      data.payload,
    );
    console.log(res);
    yield put(updatePasswordSuccess(res.data.data));
    navigate('Home', {screen: 'Account'});
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(updatePasswordError());
  }
}

function* updatePasswordSaga() {
  yield takeLatest(UPDATE_PASSWORD, updateUserPassword);
}

export default updatePasswordSaga;
