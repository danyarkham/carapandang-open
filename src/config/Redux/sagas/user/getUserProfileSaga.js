import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {setLogin, setUserLogin} from '../../../../Screen/Login/actionLogin';
import {API_URL, GET_USER_PROFILE} from '../../../Constant';
import {getLanguage} from '../../actions/languageAction';
import {sendWelcomeMessage} from '../../actions/userAction';

const setItem = async (key, item) => {
  await AsyncStorage.setItem(key, item);
};

function* getUserProfileData() {
  try {
    const res = yield axios.get(`${API_URL}/user/profile`);
    console.log(res);
    const {user} = res.data;
    if (user?.welcome_message) {
      yield put(sendWelcomeMessage());
    }
    yield setItem('user', JSON.stringify(user));
    yield put(setLogin(res.data));
    yield put(setUserLogin(user));
    yield put(getLanguage());
  } catch (e) {
    console.log(e.response);
  }
}

function* getUserProfileSaga() {
  yield takeLatest(GET_USER_PROFILE, getUserProfileData);
}

export default getUserProfileSaga;
