import axios from 'axios';
import {takeLatest} from 'redux-saga/effects';
import {API_URL, SEND_WELCOME_MESSAGE} from '../../../Constant';

function* sendWelcomeMessageData() {
  try {
    const res = yield axios.post(`${API_URL}/user/welcome-message`, {});
    console.log(res);
  } catch (e) {
    console.log(e.response);
  }
}

function* sendWelcomeMessageSaga() {
  yield takeLatest(SEND_WELCOME_MESSAGE, sendWelcomeMessageData);
}

export default sendWelcomeMessageSaga;
