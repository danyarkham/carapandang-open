import axios from 'axios';
import {takeLatest} from 'redux-saga/effects';
import {API_URL, UPDATE_DEVICE_ID} from '../../../Constant';

function* updateDeviceIdData(data) {
  try {
    const res = yield axios.post(
      `${API_URL}/user/update-device-id`,
      data.payload,
    );
  } catch (e) {
    console.log(e.response);
  }
}

function* updateDeviceIdSaga() {
  yield takeLatest(UPDATE_DEVICE_ID, updateDeviceIdData);
}

export default updateDeviceIdSaga;
