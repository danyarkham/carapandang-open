import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {API_URL, GET_INVITATION_LINK} from '../../Constant';

function* getInvitationLinkData() {
  try {
    const res = yield axios.get(`${API_URL}/group_item/all`);
    console.log(res);
    // yield put(getGroupItemSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    // yield put(getGroupItemError());
  }
}

function* getInvitationLinkSaga() {
  yield takeLatest(GET_INVITATION_LINK, getInvitationLinkData);
}
