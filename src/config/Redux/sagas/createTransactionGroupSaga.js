import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest, delay} from 'redux-saga/effects';
import {API_URL, CREATE_TRANSACTION_GROUP} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  createTransactionGroupError,
  createTransactionGroupLoading,
  createTransactionGroupSuccess,
} from '../actions/createTransactionGroupAction';
import {getMyTransaction} from '../actions/getMyTransactionAction';
import {getPurchaseDetail} from '../actions/getPurchaseDetailAction';
import {getScheduleRequest} from '../actions/getScheduleRequestAction';

// get all language
function* createTransactionGroupData({payload}) {
  yield put(createTransactionGroupLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/confirm_payment/create_transaction`,
      payload.data,
    );
    console.log(res, 'CREATE TRANSACTION GROUP');
    yield delay(3000);
    yield put(createTransactionGroupSuccess(res.data.data));
    yield put(getPurchaseDetail(payload?.data?.group_item_id));
    yield put(getMyTransaction());
    if (payload.data.book) {
      payload.data;
    }
    yield put(getScheduleRequest());
    navigate('PaymentMethodDetail', {
      item: res?.data?.data?.group_item,
      item_id: payload?.data?.group_item_id,
      transaction: true,
      message: res?.data?.data?.message,
    });
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield delay(2000);
    if (e.response) {
      Snackbar.show({
        text: e.response?.data?.message,
        duration: 3000,
        backgroundColor: '#E2494B',
      });
    }
    yield put(createTransactionGroupError());
  }
}

function* createTransactionGroupSaga() {
  yield takeLatest(CREATE_TRANSACTION_GROUP, createTransactionGroupData);
}

export default createTransactionGroupSaga;
