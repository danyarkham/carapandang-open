import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_HOMEWORK_QUESTION} from '../../Constant';
import {
  getHomeworkQuestionError,
  getHomeworkQuestionLoading,
  getHomeworkQuestionSuccess,
} from '../actions/getHomeworkQuestionAction';

function* getHomeworkQuestionData({payload}) {
  yield put(getHomeworkQuestionLoading());
  try {
    const res = yield axios.get(`${API_URL}/homework/group/${payload}`);
    console.log(res);
    yield put(getHomeworkQuestionSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getHomeworkQuestionError());
  }
}

function* getHomeworkQuestionSaga() {
  yield takeLatest(GET_HOMEWORK_QUESTION, getHomeworkQuestionData);
}

export default getHomeworkQuestionSaga;
