import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_BANNER_DETAIL} from '../../Constant';
import {
  getBannerDetailLoading,
  getBannerDetailSuccess,
} from '../actions/bannerDetailAction';

function* getBannerDetailData({payload}) {
  yield put(getBannerDetailLoading(true));
  try {
    // console.log(payload, 'dany arkham');
    const res = yield axios.get(`${API_URL}/banner/detail/${payload}`);
    console.log(res);
    if (res.status === 200) {
      yield put(getBannerDetailSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
    yield put(getBannerDetailLoading(false));
  }
}

function* bannerDetailSaga() {
  yield takeLatest(GET_BANNER_DETAIL, getBannerDetailData);
}

export default bannerDetailSaga;
