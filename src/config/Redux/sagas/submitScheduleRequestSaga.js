import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, SCHEDULE_REQUEST_SUBMIT} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  getLevelCategoryError,
  getLevelCategorySuccess,
} from '../actions/getLevelCategoryAction';
import {
  getMasterGroupError,
  getMasterGroupSuccess,
} from '../actions/getMasterGroupAction';
import {getScheduleRequest} from '../actions/getScheduleRequestAction';
import {getTypeCategoryError} from '../actions/getTypeCategoryAction';
import {
  submitScheduleRequest,
  submitScheduleRequestError,
  submitScheduleRequestLoading,
  submitScheduleRequestModal,
  submitScheduleRequestSuccess,
} from '../actions/submitScheduleRequestAction';

// get all language
function* submitScheduleRequestData({payload}) {
  yield put(submitScheduleRequestLoading());

  try {
    const res = yield axios.post(`${API_URL}/schedule_request/insert`, payload);
    console.log(res?.data?.message, 'RESPONSE SNACKBAR SCHEDULE REQUEST');
    yield put(submitScheduleRequestSuccess(res.data.data));
    yield put(getLevelCategorySuccess([]));
    yield put(getMasterGroupSuccess(null));
    yield put(getScheduleRequest());
    navigate('ScheduleRequest', {screen: 'Request List'});
    Snackbar.show({
      text:
        res?.data?.message === 'Schedule request has been saved'
          ? 'Schedule request successfully submitted!'
          : res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
    console.log('ScheduleRequest', res);
  } catch (e) {
    console.log('ScheduleRequest', e.response);
    yield put(getLevelCategorySuccess([]));
    yield put(getMasterGroupSuccess(null));
    yield put(submitScheduleRequestError());
    return yield put(submitScheduleRequestModal());
  }
}

function* submitScheduleRequestSaga() {
  yield takeLatest(SCHEDULE_REQUEST_SUBMIT, submitScheduleRequestData);
}

export default submitScheduleRequestSaga;
