import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {delay, put, takeLatest} from 'redux-saga/effects';
import {API_URL, CANCEL_TRANSACTION} from '../../Constant';
import {
  cancelTransactionError,
  cancelTransactionLoading,
  cancelTransactionSuccess,
} from '../actions/cancelTransactionAction';
import {getMyTransaction} from '../actions/getMyTransactionAction';

// get all language
function* cancelTransactionData({payload}) {
  yield put(cancelTransactionLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/${payload.path}/cancelTransaction`,
      payload,
    );
    yield put(cancelTransactionSuccess(true));
    yield put(getMyTransaction(true));
  } catch (e) {
    console.log(e.response);
    yield delay(2000);
    if (e.response) {
      Snackbar.show({
        text: e.response?.data?.message,
        duration: 3000,
        backgroundColor: '#E2494B',
      });
    }
    yield put(cancelTransactionError());
  }
}

function* cancelTransactionSaga() {
  yield takeLatest(CANCEL_TRANSACTION, cancelTransactionData);
}

export default cancelTransactionSaga;
