import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {API_URL, GET_INVITATION_GROUP_LINK} from '../../../Constant';
import {
  getGroupInvitationLinkError,
  getGroupInvitationLinkLoading,
  getGroupInvitationLinkSuccess,
} from '../../actions/groupItemAction';

function* getGroupInvitationLinkData({payload}) {
  yield put(getGroupInvitationLinkLoading());
  try {
    const res = yield axios.get(
      `${API_URL}/my_transaction/invitation/${payload}`,
    );
    yield put(getGroupInvitationLinkSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getGroupInvitationLinkError());
  }
}

function* getGroupInvitationLinkSaga() {
  yield takeLatest(GET_INVITATION_GROUP_LINK, getGroupInvitationLinkData);
}

export default getGroupInvitationLinkSaga;
