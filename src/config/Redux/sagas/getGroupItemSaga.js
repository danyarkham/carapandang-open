import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_GROUP_CLASS} from '../../Constant';
import {
  getGroupItemError,
  getGroupItemLoading,
  getGroupItemSuccess,
} from '../actions/groupItemAction';

// get all language
function* getGroupItemData() {
  yield put(getGroupItemLoading());
  try {
    const res = yield axios.get(`${API_URL}/group_item/all`);
    console.log(res);
    yield put(getGroupItemSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getGroupItemError());
  }
}

function* getGroupItemSaga() {
  yield takeLatest(GET_GROUP_CLASS, getGroupItemData);
}

export default getGroupItemSaga;
