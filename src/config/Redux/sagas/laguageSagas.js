import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {
  getSelectedLanguage,
  GET_SELECTED_LANGUAGE,
} from '../../../Screen/Home/actionHome';
import {API_URL, GET_LANGUAGE} from '../../Constant';
import {getLanguageSuccess} from '../actions/languageAction';

// get all language
function* getLanguageData() {
  try {
    const res = yield axios.get(`${API_URL}/language/all`);
    if (res.status === 200) {
      yield put(getLanguageSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
  }
}

function* languageSaga() {
  yield takeLatest(GET_LANGUAGE, getLanguageData);
}

export default languageSaga;
