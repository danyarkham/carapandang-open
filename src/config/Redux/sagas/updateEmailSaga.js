import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPDATE_EMAIL} from '../../Constant';
import {
  updateEmailError,
  updateEmailLoading,
  updateEmailModal,
  updateEmailSuccess,
} from '../actions/updateEmailAction';

function* updateUserEmail(data) {
  yield put(updateEmailLoading());
  try {
    const res = yield axios.post(`${API_URL}/user/update-email`, data.payload);
    console.log(res);
    yield put(updateEmailSuccess(res.data.data));
    yield put(updateEmailModal(false));
    // navigate('EditProfile');
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(updateEmailError());
  }
}

function* updateEmailSaga() {
  yield takeLatest(UPDATE_EMAIL, updateUserEmail);
}

export default updateEmailSaga;
