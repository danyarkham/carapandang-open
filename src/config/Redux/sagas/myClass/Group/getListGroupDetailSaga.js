import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_LIST_GROUP_DETAIL} from '../../../../Constant';
import {
  getListGroupDetailError,
  getListGroupDetailLoading,
  getListGroupDetailSuccess,
} from '../../../actions/myClassAction';

// get all language
function* getListGroupDetailData({payload}) {
  yield put(getListGroupDetailLoading());
  try {
    const res = yield axios.get(`${API_URL}/my_class/group/detail/${payload}`);
    yield put(getListGroupDetailSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getListGroupDetailError());
  }
}

function* getListGroupDetailSaga() {
  yield takeLatest(GET_LIST_GROUP_DETAIL, getListGroupDetailData);
}

export default getListGroupDetailSaga;
