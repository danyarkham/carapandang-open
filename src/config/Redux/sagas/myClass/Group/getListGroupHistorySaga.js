import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_LIST_GROUP_HISTORY} from '../../../../Constant';
import {
  getListGroupHistoryError,
  getListGroupHistoryLoading,
  getListGroupHistorySuccess,
} from '../../../actions/myClassAction';

// get all language
function* getListGroupHistoryData({payload}) {
  yield put(getListGroupHistoryLoading());
  try {
    const res = yield axios.get(`${API_URL}/my_class/group/history/${payload}`);
    yield put(getListGroupHistorySuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getListGroupHistoryError());
  }
}

function* getListGroupHistorySaga() {
  yield takeLatest(GET_LIST_GROUP_HISTORY, getListGroupHistoryData);
}

export default getListGroupHistorySaga;
