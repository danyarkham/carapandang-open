import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {filterTransaction} from '../../../../../Utils/Helpers/helper';
import {API_URL, GET_LIST_GROUP} from '../../../../Constant';
import {
  getMyTransactionModal,
  getMyTransactionUnSuccess,
} from '../../../actions/getMyTransactionAction';
import {
  getListGroupLoading,
  getListGroupSuccess,
} from '../../../actions/myClassAction';

// get all language
function* getListGroupData({payload}) {
  yield put(getListGroupLoading());
  try {
    const res = yield axios.get(`${API_URL}/my_class/group/all`);
    yield put(getListGroupSuccess(res.data.data));

    const transaction_unsuccess = filterTransaction(res.data.data, 1);
    yield put(getMyTransactionUnSuccess(transaction_unsuccess));
    if (res?.data?.data?.length < 1) {
      yield put(getMyTransactionModal(true));
    }
  } catch (e) {
    console.log(e.response);
    yield put(getMyTransactionModal(true));
    yield put(getListGroupError());
  }
}

function* getListGroupSaga() {
  yield takeLatest(GET_LIST_GROUP, getListGroupData);
}

export default getListGroupSaga;
