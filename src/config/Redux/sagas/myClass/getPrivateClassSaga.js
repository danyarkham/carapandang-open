import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, PRIVATE_TRANSACTION_CLASS} from '../../../Constant';
import {
  privateTransactionClassError,
  privateTransactionClassLoading,
  privateTransactionClassSuccess,
} from '../../actions/privateTransactionAction';

// get all language
function* privateTransactionClassData() {
  yield put(privateTransactionClassLoading());
  try {
    const res = yield axios.get(`${API_URL}/private_transaction/all`);
    console.log(res);
    yield put(privateTransactionClassSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(privateTransactionClassError());
  }
}

function* getPrivateClassSaga() {
  yield takeLatest(PRIVATE_TRANSACTION_CLASS, privateTransactionClassData);
}

export default getPrivateClassSaga;
