import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {setUserLogin, updateUserLogin} from '../../../Screen/Login/actionLogin';
import {API_URL, UPDATE_LANGUAGE} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {getGroupItem} from '../actions/groupItemAction';
import {privateTransactionClass} from '../actions/privateTransactionAction';
import {
  updateLanguageError,
  updateLanguageLoading,
  updateLanguageSuccess,
} from '../actions/updateLanguageAction';

const updateLanguage = async language => {
  const userDetail = await AsyncStorage.getItem('user');
  const newUser = JSON.parse(userDetail);

  const userData = {...newUser, language};
  put(setUserLogin(userData));
  await AsyncStorage.setItem('user', JSON.stringify(userData));
  console.log(userData);
};

function* updateLanguageData({payload}) {
  yield put(updateLanguageLoading(true));
  try {
    const res = yield axios.post(`${API_URL}/user/update-language`, payload);
    yield put(updateLanguageSuccess(res.data.data));
    updateLanguage(res.data.data);
    yield put(updateUserLogin({language: res.data.data}));
    yield put(getGroupItem());
    yield put(privateTransactionClass());
    navigate('Home');
    Snackbar.show({
      text:
        res?.data?.message === 'Succesfuly update language'
          ? 'Successfully update language'
          : res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    // console.log(e?.response?.data?.message);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(updateLanguageError());
  }
}

function* updateLanguageSaga() {
  yield takeLatest(UPDATE_LANGUAGE, updateLanguageData);
}

export default updateLanguageSaga;
