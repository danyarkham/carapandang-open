import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {setUserLogin, updateUserLogin} from '../../../Screen/Login/actionLogin';
import {asset} from '../../../Utils/Helpers/helper';
import {API_URL, UPDATE_PROFILE_PHOTO} from '../../Constant';
import {
  updateProfilePhotoError,
  updateProfilePhotoLoading,
  updateProfilePhotoSuccess,
} from '../actions/updateProfilePhoto';

const setProfile = async profile_image => {
  const userDetail = await AsyncStorage.getItem('user');
  const newUser = JSON.parse(userDetail);

  const userData = {...newUser, profile_image};
  put(setUserLogin(userData));
  await AsyncStorage.setItem('user', JSON.stringify(userData));
};

function* updateUserProfilePhoto(data) {
  yield put(updateProfilePhotoLoading(true));
  try {
    const header = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    const res = yield axios.post(
      `${API_URL}/user/update-profile-photo`,
      data.payload,
      header,
    );
    yield put(updateProfilePhotoSuccess(res.data.data));
    setProfile(res.data.result.profile_image);
    yield put(updateUserLogin({profile_image: res.data.result.profile_image}));
    // navigate('EditProfile');
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(updateProfilePhotoError());
  }
}

function* updateProfilePhotoSaga() {
  yield takeLatest(UPDATE_PROFILE_PHOTO, updateUserProfilePhoto);
}

export default updateProfilePhotoSaga;
