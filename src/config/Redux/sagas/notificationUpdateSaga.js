import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_NOTIFICATIONS_LIST_UPDATE} from '../../Constant';
import {
  getNotificationList,
  getNotificationListError,
} from '../actions/getNotificationsAction';

// get all language
function* notificationUpdateData({payload}) {
  try {
    const res = yield axios.get(`${API_URL}/notifications/update/${payload}`);
    yield put(getNotificationList());
  } catch (e) {
    console.log(e.response);
    yield put(getNotificationListError());
  }
}

function* notificationUpdateSaga() {
  yield takeLatest(GET_NOTIFICATIONS_LIST_UPDATE, notificationUpdateData);
}

export default notificationUpdateSaga;
