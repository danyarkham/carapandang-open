import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {UPDATE_PROFILE} from '../../Constant';
import {
  updateProfileError,
  updateProfileLoading,
  updateProfileSuccess,
} from '../actions/updateProfileAction';
import {API_URL} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import Snackbar from 'react-native-snackbar';
import {getUserProfile} from '../actions/userAction';

function* updateUserProfile(data) {
  yield put(updateProfileLoading());
  try {
    const res = yield axios.post(`${API_URL}/user/update-data`, data.payload);
    console.log(res);
    yield put(updateProfileSuccess(res.data.data));
    yield put(getUserProfile());
    navigate('EditProfile');
    Snackbar.show({
      text: 'Profile succesfully updated',
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(updateProfileError());
  }
}

function* updateProfile() {
  yield takeLatest(UPDATE_PROFILE, updateUserProfile);
}

export default updateProfile;
