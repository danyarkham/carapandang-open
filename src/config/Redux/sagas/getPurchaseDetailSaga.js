import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {numberFormat, stringToInt} from '../../../Utils/Helpers/helper';
import {API_URL, GET_PURCHASE_DETAIL} from '../../Constant';
import {
  getPurchaseDetailError,
  getPurchaseDetailLoading,
  getPurchaseDetailSuccess,
} from '../actions/getPurchaseDetailAction';

// get all language
function* getPurchaseDetailData({payload}) {
  yield put(getPurchaseDetailLoading());
  try {
    const res = yield axios.get(`${API_URL}/group_item/detail/${payload}`);
    console.log(res);
    const {data} = res.data;
    const participant = data?.transaction?.total_participant || 0;
    let total_price =
      stringToInt(data?.final_price) +
      stringToInt(data?.final_price) * participant;
    if (data?.transaction?.discount) {
      total_price -= data?.transaction?.discount;
    }
    const newData = {
      ...data,
      total_price:
        participant > 0 ? numberFormat(total_price) : data?.final_price,
      total_participant: participant || 0,
    };
    yield put(getPurchaseDetailSuccess(newData));
  } catch (e) {
    console.log(e.response);
    yield put(getPurchaseDetailError());
  }
}

function* getPurchaseDetailSaga() {
  yield takeLatest(GET_PURCHASE_DETAIL, getPurchaseDetailData);
}

export default getPurchaseDetailSaga;
