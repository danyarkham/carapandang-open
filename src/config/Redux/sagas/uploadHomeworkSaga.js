import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPLOAD_HOMEWORK} from '../../Constant';
import {getHomeworkQuestion} from '../actions/getHomeworkQuestionAction';
import {getPrivateHomework} from '../actions/privateTransactionAction';
import {
  uploadHomeworkError,
  uploadHomeworkLoading,
  uploadHomeworkSuccess,
} from '../actions/uploadHomeworkAction';

function* uploadHomeworkData({payload}) {
  yield put(uploadHomeworkLoading(true));
  try {
    const header = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    const res = yield axios.post(
      `${API_URL}/homework/insert`,
      payload.formData,
      header,
    );
    yield put(uploadHomeworkSuccess(res.data.data));
    if (payload?.type === 'group') {
      yield put(getHomeworkQuestion(payload.item_id));
    } else {
      yield put(getPrivateHomework(payload.item_id));
    }
    // navigate('EditProfile');
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(uploadHomeworkError());
  }
}

function* uploadHomeworkSaga() {
  yield takeLatest(UPLOAD_HOMEWORK, uploadHomeworkData);
}

export default uploadHomeworkSaga;
