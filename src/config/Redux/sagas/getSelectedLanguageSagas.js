import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {
  getSelectedLanguage,
  getSelectedLanguageData,
  GET_SELECTED_LANGUAGE,
} from '../../../Screen/Home/actionHome';
import {API_URL} from '../../Constant';

// get language detail
function* getLanguageSelected({payload}) {
  try {
    const res = yield axios.get(`${API_URL}/language/detail/${payload}`);
    console.log(res);
    if (res.status === 200) {
      yield put(getSelectedLanguageData(res.data.data));
      return 0;
    }
  } catch (e) {
    console.log(e.response);
  }
}

function* getLanguageSelectedSaga() {
  yield takeLatest(GET_SELECTED_LANGUAGE, getLanguageSelected);
}

export default getLanguageSelectedSaga;
