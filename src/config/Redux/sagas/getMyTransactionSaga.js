import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {filterTransaction} from '../../../Utils/Helpers/helper';
import {API_URL, GET_MY_TRANSACTION} from '../../Constant';
import {
  getMyTransactionError,
  getMyTransactionLoading,
  getMyTransactionModal,
  getMyTransactionSuccess,
  getMyTransactionUnSuccess,
} from '../actions/getMyTransactionAction';

// get all language
function* getMyTransactionData({payload}) {
  yield put(getMyTransactionLoading());
  try {
    const res = yield axios.get(`${API_URL}/my_transaction/all`);
    yield put(getMyTransactionSuccess(res.data.data));

    const transaction_unsuccess = filterTransaction(res.data.data, 1);
    yield put(getMyTransactionUnSuccess(transaction_unsuccess));
    if (filterTransaction(res.data.data, 4)?.length < 1) {
      if (payload === 'MyClass') {
        // yield put(getMyTransactionUnSuccess([]));
        yield put(getMyTransactionModal(true));
      }
    }
  } catch (e) {
    console.log(e.response);
    if (payload === 'MyClass') {
      yield put(getMyTransactionModal(true));
    }
    // yield put(getMyTransactionModal(true));
    yield put(getMyTransactionError());
  }
}

function* getMyTransactionSaga() {
  yield takeLatest(GET_MY_TRANSACTION, getMyTransactionData);
}

export default getMyTransactionSaga;
