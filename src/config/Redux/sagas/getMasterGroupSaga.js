import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_MASTER_GROUP} from '../../Constant';
import {
  getMasterGroupError,
  getMasterGroupLoading,
  getMasterGroupSuccess,
} from '../actions/getMasterGroupAction';

// get all language
function* getMasterGroup({payload}) {
  yield put(getMasterGroupLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/schedule_request/get_group_item`,
      payload,
    );
    if (res.status === 200) {
      yield put(getMasterGroupSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
    yield put(getMasterGroupError());
  }
}

function* getMasterGroupSaga() {
  yield takeLatest(GET_MASTER_GROUP, getMasterGroup);
}

export default getMasterGroupSaga;
