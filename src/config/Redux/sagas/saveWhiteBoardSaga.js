import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, SAVE_WHITE_BOARD} from '../../Constant';
import {
  saveWhiteBoardError,
  saveWhiteBoardLoading,
  saveWhiteBoardSuccess,
} from '../actions/saveWhiteBoardAction';

// get all language
function* saveWhiteBoardData({payload}) {
  yield put(saveWhiteBoardLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/my_transaction/save_whiteboard`,
      payload,
    );
    console.log(res);
    yield put(saveWhiteBoardSuccess(res.data.data));
    Snackbar.show({
      text: 'whiteboard saved',
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(saveWhiteBoardError());
  }
}

function* saveWhiteBoardSaga() {
  yield takeLatest(SAVE_WHITE_BOARD, saveWhiteBoardData);
}

export default saveWhiteBoardSaga;
