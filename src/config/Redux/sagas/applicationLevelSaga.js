import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_APPLICATION_LEVEL} from '../../Constant';
import {getApplicationLevelSuccess} from '../actions/applicationLevelAction';

function* getApplicationLevelData({payload}) {
  try {
    const res = yield axios.get(`${API_URL}/course_purpose/lang/${payload}`);
    if (res.status === 200) {
      yield put(getApplicationLevelSuccess(res.data.data));
    }
  } catch (e) {
    console.log(e.response);
  }
}

function* applicationLevel() {
  yield takeLatest(GET_APPLICATION_LEVEL, getApplicationLevelData);
}

export default applicationLevel;
