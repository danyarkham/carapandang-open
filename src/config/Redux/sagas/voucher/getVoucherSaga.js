import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_VOUCHER} from '../../../Constant';
import {
  getVoucherError,
  getVoucherLoading,
  getVoucherSuccess,
} from '../../actions/voucherAction';

// get all language
function* getVoucherData() {
  yield put(getVoucherLoading());
  try {
    const res = yield axios.get(`${API_URL}/voucher/all`);
    console.log(res);
    yield put(getVoucherSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getVoucherError());
  }
}

function* getVoucherSaga() {
  yield takeLatest(GET_VOUCHER, getVoucherData);
}

export default getVoucherSaga;
