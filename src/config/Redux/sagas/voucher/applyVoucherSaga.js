import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, APPLY_VOUCHER} from '../../../Constant';
import {getPurchaseDetailSuccess} from '../../actions/getPurchaseDetailAction';
import {
  applyVoucherError,
  applyVoucherLoading,
  applyVoucherSuccess,
} from '../../actions/voucherAction';

// get all language
function* applyVoucherData({payload}) {
  yield put(applyVoucherLoading());
  try {
    const res = yield axios.post(`${API_URL}/voucher/apply`, payload.data);
    console.log(res);
    const newData = {
      ...payload.purchase,
      transaction: {
        ...payload.purchase.transaction,
        discount: res.data.data?.amount_discount,
        voucher_id: res.data.data?.voucher_id,
        type_voucher: res.data.data?.type_voucher || 'normal',
      },
    };
    yield put(getPurchaseDetailSuccess(newData));
    yield put(applyVoucherSuccess(res.data.data));

    Snackbar.show({
      text:
        res?.data?.message === 'Yeay your voucher has been successfully applied'
          ? 'Yeay!!! You have successfully added the voucher'
          : res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text:
          e.response?.data?.message === 'You cannot applyed this voucher'
            ? 'Sorry, check your voucher typing once again..'
            : e.response?.data?.message,
        duration: 3000,
        backgroundColor: '#E2494B',
      });
    }
    yield put(applyVoucherError());
  }
}

function* applyVoucherSaga() {
  yield takeLatest(APPLY_VOUCHER, applyVoucherData);
}

export default applyVoucherSaga;
