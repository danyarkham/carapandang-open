import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, VOUCHER_REFERN_EARN} from '../../../Constant';
import {
  getVoucherRefernEarnError,
  getVoucherRefernEarnLoading,
  getVoucherRefernEarnSuccess,
} from '../../actions/voucherAction';

// get all language
function* getVoucherRefernEarnData() {
  yield put(getVoucherRefernEarnLoading());
  try {
    const res = yield axios.get(`${API_URL}/voucher/refern-earn`);
    console.log(res);
    yield put(getVoucherRefernEarnSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getVoucherRefernEarnError());
  }
}

function* getVoucherRefernEarnSaga() {
  yield takeLatest(VOUCHER_REFERN_EARN, getVoucherRefernEarnData);
}

export default getVoucherRefernEarnSaga;
