import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_CERTIFICATE} from '../../../Constant';
import {
  getCertificateError,
  getCertificateLoading,
  getCertificateSuccess,
} from '../../actions/reportAndCertificateAction';

// get all language
function* getCertificateData({payload}) {
  yield put(getCertificateLoading());
  try {
    const res = yield axios.get(`${API_URL}/report/${payload}`);
    console.log(res);
    yield put(getCertificateSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getCertificateError());
  }
}

function* getCertificateSaga() {
  yield takeLatest(GET_CERTIFICATE, getCertificateData);
}

export default getCertificateSaga;
