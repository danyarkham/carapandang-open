import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_REPORTS} from '../../../Constant';
import {
  getReportsError,
  getReportsLoading,
  getReportsSuccess,
} from '../../actions/reportAndCertificateAction';

// get all language
function* getReportsData({payload}) {
  yield put(getReportsLoading());
  try {
    const res = yield axios.get(`${API_URL}/report/${payload}`);
    console.log(res);
    yield put(getReportsSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getReportsError());
  }
}

function* getReportsSaga() {
  yield takeLatest(GET_REPORTS, getReportsData);
}

export default getReportsSaga;
