import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, UPLOAD_HOMEWORK_PRIVATE} from '../../Constant';
import {
  getPrivateHomework,
  uploadPrivateHomeworkError,
  uploadPrivateHomeworkLoading,
  uploadPrivateHomeworkSuccess,
} from '../actions/privateTransactionAction';

function* uploadPrivateHomeworkData({payload}) {
  yield put(uploadPrivateHomeworkLoading(true));
  try {
    const header = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    const res = yield axios.post(
      `${API_URL}/private_transaction/save_homework`,
      payload.formData,
      header,
    );

    const homework = {...payload.homework, submision: res.data.data};
    yield put(uploadPrivateHomeworkSuccess(homework));
    yield put(getPrivateHomework(payload.item_id));
    // navigate('EditProfile');

    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(uploadPrivateHomeworkError());
  }
}

function* uploadPrivateHomeworkSaga() {
  yield takeLatest(UPLOAD_HOMEWORK_PRIVATE, uploadPrivateHomeworkData);
}

export default uploadPrivateHomeworkSaga;
