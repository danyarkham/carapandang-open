import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_MASTER_CREDIT} from '../../../Constant';
import {
  getMasterCreditError,
  getMasterCreditLoading,
  getMasterCreditSuccess,
} from '../../actions/creditAction';

// get all language
function* getMasterCreditData() {
  yield put(getMasterCreditLoading());
  try {
    const res = yield axios.get(`${API_URL}/master_credit/all`);
    console.log(res);
    yield put(getMasterCreditSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getMasterCreditError());
  }
}

function* getMasterCreditSaga() {
  yield takeLatest(GET_MASTER_CREDIT, getMasterCreditData);
}

export default getMasterCreditSaga;
