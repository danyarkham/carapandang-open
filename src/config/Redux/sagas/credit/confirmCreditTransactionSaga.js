import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, CONFIRM_CREDIT_TRANSACTION} from '../../../Constant';
import {
  confirmCreditTransactionError,
  confirmCreditTransactionLoading,
  confirmCreditTransactionSuccess,
} from '../../actions/creditAction';
import {getMyTransaction} from '../../actions/getMyTransactionAction';
import {uploadConfirmPaymentModal} from '../../actions/uploadConfirmPayment';

function* confirmCreditTransactionData({payload}) {
  yield put(confirmCreditTransactionLoading(true));
  try {
    const header = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    const res = yield axios.post(
      `${API_URL}/credit/confirm`,
      payload.form,
      header,
    );
    yield put(confirmCreditTransactionSuccess(res.data.data));
    yield put(uploadConfirmPaymentModal());
    yield put(getMyTransaction());
    // if (payload.transaction) {

    // } else {
    //   navigate('ScheduleRequest', {screen: 'Request List'});
    // }
    // Snackbar.show({
    //   text: res?.data?.message,
    //   duration: Snackbar.LENGTH_LONG,
    //   backgroundColor: '#27ae60',
    // });
    // console.log(res);
  } catch (e) {
    console.log(e.response);
    if (e.response) {
      Snackbar.show({
        text: e?.response?.data?.message,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: '#E2494B',
      });
    }
    yield put(confirmCreditTransactionError());
  }
}

function* confirmCreditTransactionSaga() {
  yield takeLatest(CONFIRM_CREDIT_TRANSACTION, confirmCreditTransactionData);
}

export default confirmCreditTransactionSaga;
