import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_HISTORY_CREDIT} from '../../../Constant';
import {
  getMyHistoryCreditError,
  getMyHistoryCreditLoading,
  getMyHistoryCreditSuccess,
} from '../../actions/creditAction';

// get all language
function* getHistoryCreditData({payload = 1}) {
  console.log('payload', payload);
  yield put(getMyHistoryCreditLoading());
  try {
    const res = yield axios.get(`${API_URL}/credit/history/${payload}`);
    console.log(res);
    yield put(getMyHistoryCreditSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getMyHistoryCreditError());
  }
}

function* getHistoryCreditSaga() {
  yield takeLatest(GET_HISTORY_CREDIT, getHistoryCreditData);
}

export default getHistoryCreditSaga;
