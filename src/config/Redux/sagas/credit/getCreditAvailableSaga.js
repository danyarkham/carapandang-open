import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_CREDIT_AVAILABLE} from '../../../Constant';
import {
  getCreditAvailableError,
  getCreditAvailableSuccess,
} from '../../actions/creditAction';

// get all language
function* getCreditAvailableData({payload = 1}) {
  console.log('payload', payload);
  try {
    const res = yield axios.get(`${API_URL}/credit/my_credit`);
    console.log(res);
    const {data} = res.data;
    const total =
      data &&
      data
        .map(v => v?.credit_available)
        .reduce((sum, current) => sum + current, 0);
    yield put(getCreditAvailableSuccess(total));
  } catch (e) {
    console.log(e.response);
    yield put(getCreditAvailableError());
  }
}

function* getCreditAvailableSaga() {
  yield takeLatest(GET_CREDIT_AVAILABLE, getCreditAvailableData);
}

export default getCreditAvailableSaga;
