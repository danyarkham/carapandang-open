import axios from 'axios';
import {put, takeLatest, takeEvery} from 'redux-saga/effects';
import {API_URL, GET_MY_PRIVATE_CREDIT} from '../../../Constant';
import {
  getMyPrivateCreditError,
  getMyPrivateCreditLoading,
  getMyPrivateCreditSuccess,
} from '../../actions/creditAction';

// get all language
function* getMyPrivateCreditData({payload = 1}) {
  yield put(getMyPrivateCreditLoading());
  try {
    const res = yield axios.get(`${API_URL}/credit/all/1`);
    console.log(res);

    yield put(getMyPrivateCreditSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getMyPrivateCreditError());
  }
}

function* getMyPrivateCreditSaga() {
  yield takeEvery(GET_MY_PRIVATE_CREDIT, getMyPrivateCreditData);
}

export default getMyPrivateCreditSaga;
