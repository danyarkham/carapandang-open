import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_MY_PRIVATE_CREDIT_EXPIRED} from '../../../Constant';
import {
  getMyPrivateCreditExpiredError,
  getMyPrivateCreditExpiredLoading,
  getMyPrivateCreditExpiredSuccess,
} from '../../actions/creditAction';

// get all language
function* getMyPrivateCreditExpiredData({payload = 1}) {
  yield put(getMyPrivateCreditExpiredLoading());
  try {
    const res = yield axios.get(`${API_URL}/credit/all/2`);
    console.log(res);
    yield put(getMyPrivateCreditExpiredSuccess(res.data.data));
  } catch (e) {
    console.log(e.response);
    yield put(getMyPrivateCreditExpiredError());
  }
}

function* getMyPrivateCreditExpiredSaga() {
  yield takeLatest(
    GET_MY_PRIVATE_CREDIT_EXPIRED,
    getMyPrivateCreditExpiredData,
  );
}

export default getMyPrivateCreditExpiredSaga;
