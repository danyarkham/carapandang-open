import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, CREATE_CREDIT_TRANSACTION} from '../../../Constant';
import {navigate} from '../../../Function/Navigate';
import {
  createCreditTransactionError,
  createCreditTransactionLoading,
  createCreditTransactionSuccess,
} from '../../actions/creditAction';
import {getMyTransaction} from '../../actions/getMyTransactionAction';

// get all language
function* createCreditTransactionData({payload}) {
  yield put(createCreditTransactionLoading());
  try {
    const res = yield axios.post(`${API_URL}/credit/buy`, payload?.data);
    console.log(res);
    yield put(createCreditTransactionSuccess(res.data.data));
    yield put(getMyTransaction());

    navigate('PaymentMethodDetailCredit', {
      item: res?.data?.data,
      item_id: payload?.data?.credit_id,
      transaction: true,
      type: 'credit',
      message: res?.data?.data?.message,
    });
    Snackbar.show({
      text: res?.data?.message,
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(createCreditTransactionError());
  }
}

function* createCreditTransactionSaga() {
  yield takeLatest(CREATE_CREDIT_TRANSACTION, createCreditTransactionData);
}

export default createCreditTransactionSaga;
