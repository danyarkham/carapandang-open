import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, CANCEL_SCHEDULE_REQUEST} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {getScheduleRequest} from '../actions/getScheduleRequestAction';
import {
  cancelScheduleRequestError,
  cancelScheduleRequestLoading,
  cancelScheduleRequestSuccess,
} from '../actions/submitScheduleRequestAction';

// get all language
function* cancelScheduleRequestData({payload}) {
  yield put(cancelScheduleRequestLoading());
  try {
    const res = yield axios.post(`${API_URL}/confirm_payment/cancel`, payload);
    yield put(cancelScheduleRequestSuccess(res.data.data));
    yield put(getScheduleRequest());
    navigate('ScheduleRequest', {screen: 'Request List'});
    Snackbar.show({
      text: 'Schedule request has been canceled',
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#27ae60',
    });
  } catch (e) {
    console.log(e.response);
    yield put(cancelScheduleRequestError());
  }
}

function* cancelScheduleRequestSaga() {
  yield takeLatest(CANCEL_SCHEDULE_REQUEST, cancelScheduleRequestData);
}

export default cancelScheduleRequestSaga;
