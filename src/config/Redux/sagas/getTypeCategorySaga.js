import axios from 'axios';
import Snackbar from 'react-native-snackbar';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_TYPE_CATEGORY} from '../../Constant';
import {navigate} from '../../Function/Navigate';
import {
  getTypeCategoryError,
  getTypeCategoryLoading,
  getTypeCategorySuccess,
} from '../actions/getTypeCategoryAction';

// get all language
function* getTypeCategory() {
  yield put(getTypeCategoryLoading());
  try {
    const res = yield axios.post(
      `${API_URL}/schedule_request/get_type_category`,
      {},
    );
    if (res.status === 200) {
      yield put(getTypeCategorySuccess(res.data.data));
    }
    navigate('ScheduleRequest');
  } catch (e) {
    console.log(e.response);
    Snackbar.show({
      text: 'Something when wrong, please try again',
      duration: Snackbar.LENGTH_LONG,
      backgroundColor: '#E2494B',
    });
    yield put(getTypeCategoryError());
  }
}

function* getTypeCategorySaga() {
  yield takeLatest(GET_TYPE_CATEGORY, getTypeCategory);
}

export default getTypeCategorySaga;
