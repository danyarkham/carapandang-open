import axios from 'axios';
import {put, takeLatest} from 'redux-saga/effects';
import {API_URL, GET_MY_TRANSACTION_DETAIL} from '../../Constant';
import {
  getMyTransactionDetailError,
  getMyTransactionDetailSuccess,
} from '../actions/getMyTransactionAction';
import {
  getPurchaseDetail,
  getPurchaseDetailLoading,
  getPurchaseDetailSuccess,
} from '../actions/getPurchaseDetailAction';

// get all language
function* getMyTransactionDetailData({payload}) {
  yield put(getPurchaseDetailLoading());
  try {
    const res = yield axios.get(`${API_URL}/my_transaction/detail/${payload}`);
    yield put(getMyTransactionDetailSuccess(res.data.data));
    console.log(res);
    yield put(getPurchaseDetail(res?.data?.data?.group_item?.id));
    // console.log(res);
  } catch (e) {
    console.log(e.response);
    yield put(getMyTransactionDetailError());
  }
}

function* getMyTransactionDetailSaga() {
  yield takeLatest(GET_MY_TRANSACTION_DETAIL, getMyTransactionDetailData);
}

export default getMyTransactionDetailSaga;
