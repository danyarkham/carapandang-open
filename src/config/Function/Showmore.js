import React, {useCallback, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Poppins from '../../Component/Poppins';

export default function Showmore({children}) {
  const [textShown, setTextShown] = useState(false); //To show ur remaining Text
  const [lengthMore, setLengthMore] = useState(true); //to show the "Read more & Less Line"
  const toggleNumberOfLines = () => {
    //To toggle the show text or hide it
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(e => {
    setLengthMore(e.nativeEvent.lines.length >= 4); //to check the text is more than 4 lines or not
    // console.log(e.nativeEvent);
  }, []);
  return (
    <View>
      <Poppins
        size={12}
        onTextLayout={onTextLayout}
        numberOfLines={textShown ? undefined : 4}
        style={{lineHeight: 21}}>
        {children}
      </Poppins>

      {lengthMore ? (
        <Poppins
          type="Medium"
          color="#367CFF"
          size={12}
          onPress={toggleNumberOfLines}
          style={{lineHeight: 21, marginTop: 10}}>
          {textShown ? 'Show Less' : 'Show More'}
        </Poppins>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({});
