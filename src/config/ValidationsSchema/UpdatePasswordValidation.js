import * as yup from 'yup';

export const updatePasswordSchema = yup.object().shape({
  old_password: yup.string().required('Password should be filled in'),
  new_password: yup
    .string()
    .min(6, 'password must be 6 character')
    .required('New Password should be filled in'),
  confirm_password: yup
    .string()
    .oneOf([yup.ref('new_password'), null], 'Password Not Valid')
    .required('Confirm New Password should be filled in'),
});
