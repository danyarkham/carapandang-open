import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {Appearance} from 'react-native';
import BottomTab from '../component/BottomTab';
import Bookmark from '../screen/Bookmark/Bookmark';
import Home from '../screen/Home/Home';
import NewsDetail from '../screen/NewsDetail/NewsDetail';

const scheme = Appearance.getColorScheme();

const sampleLightTheme = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: '#FAFAFA',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};

const sampleDarkTheme = {
  dark: true,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(55,57,58)',
    card: 'rgb(28, 28, 30)',
    text: 'rgb(255, 45, 85)',
    // border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};

const Stack = createNativeStackNavigator();
export default function Route() {
  return (
    <NavigationContainer
      theme={scheme === 'dark' ? sampleDarkTheme : sampleLightTheme}>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="BottomTab">
        <Stack.Screen name="BottomTab" component={BottomTab} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Bookmark" component={Bookmark} />
        <Stack.Screen name="NewsDetail" component={NewsDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
