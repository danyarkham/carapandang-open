import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {NotificationCard} from '../../component/CardReusable';
import FocusAwareStatusBar from '../../component/FocusAwareStatusBar';
import Header from '../../component/Header';

export default function Notification() {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    containerScroll: {
      paddingVertical: hp(2),
      paddingHorizontal: wp(6),
    },
  });
  return (
    <View style={styles.container}>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
      <Header title="Notification" backgroundColor="white" />
      <ScrollView
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}>
        <NotificationCard />
        <NotificationCard />
        <NotificationCard />
        <NotificationCard />
        <NotificationCard />
      </ScrollView>
    </View>
  );
}
