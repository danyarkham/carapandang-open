import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {NewsCard} from '../../component/CardReusable';
import Poppins from '../../component/Poppins';

export default function AllNews({navigation}) {
  console.log(navigation, 'navigation news');
  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}>
        <Poppins size={18} type="Bold">
          Latest News
        </Poppins>
        <NewsCard />
        <NewsCard />
        <NewsCard />
        <NewsCard />
        <NewsCard />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: 'blue',
  },
  containerScroll: {
    paddingHorizontal: wp(6),
    paddingVertical: hp(2),
  },
});
