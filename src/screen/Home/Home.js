import React, {useEffect, useRef, useState} from 'react';
import {
  Animated,
  Dimensions,
  FlatList,
  LayoutAnimation,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {NewsCard} from '../../component/CardReusable';
import FocusAwareStatusBar from '../../component/FocusAwareStatusBar';
import Poppins from '../../component/Poppins';
import Searchbar from '../../component/Searchbar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const {width, height} = Dimensions.get('window');

const headers = [
  'Semua Berita',
  'berita 1',
  'berita 2',
  'berita 3',
  'berita 4',
  'berita 5',
  'berita 6',
];

let animationActive = true;
let animationActiveRef;

export default function Home({navigation}) {
  console.log(navigation, 'Navigation home');
  const [headerWidths, setWidths] = useState([]);
  const [active, setActive] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const barTranslate = Animated.multiply(scrollX, -1);
  const barTranslate1 = useRef(new Animated.Value(0)).current;
  const headerScrollView = useRef();
  const itemScrollView = useRef();
  useEffect(() => {
    let leftOffset = 0;
    for (let i = 0; i < active; i += 1) {
      leftOffset += headerWidths[i];
    }
    headerScrollView.current.scrollToIndex({index: active, viewPosition: 0.5});
    Animated.spring(barTranslate1, {
      toValue: leftOffset,
      useNativeDriver: true,
      bounciness: 0,
    }).start();
  }, [active]);
  const onPressHeader = index => {
    if (animationActiveRef) {
      clearTimeout(animationActiveRef);
    }
    if (active != index) {
      animationActive = false;
      animationActiveRef = setTimeout(() => {
        animationActive = true;
      }, 400);
      itemScrollView.current.scrollToIndex({index});
      LayoutAnimation.easeInEaseOut();
      setActive(index);
    }
  };
  const onScroll = e => {
    const x = e.nativeEvent.contentOffset.x;
    let newIndex = Math.floor(x / width + 0.5);
    if (active !== newIndex && animationActive) {
      LayoutAnimation.easeInEaseOut();
      setActive(newIndex);
    }
  };
  const onHeaderLayout = (width, index) => {
    let newWidths = [...headerWidths];
    newWidths[index] = width;
    setWidths(newWidths);
  };

  const Screen = ({item, index}) => (
    <View key={item} style={styles.mainItem}>
      <View style={styles.container}>
        <ScrollView
          contentContainerStyle={styles.containerScroll}
          showsVerticalScrollIndicator={false}>
          <Poppins size={18} type="Bold">
            Latest News
          </Poppins>
          <NewsCard onPress={e => navigation.navigate('NewsDetail')} />
          <NewsCard />
          <NewsCard />
          <NewsCard />
          <NewsCard />
          <NewsCard />
        </ScrollView>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <View>
        <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
        <Searchbar />
        <FlatList
          data={headers}
          ref={headerScrollView}
          keyExtractor={item => item}
          horizontal
          style={styles.headerScroll}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
          showsHorizontalScrollIndicator={false}
          ListFooterComponent={() => <View style={[styles.headerBar, {}]} />}
          renderItem={({item, index}) => (
            <View style={{overflow: 'hidden'}}>
              <TouchableOpacity
                onLayout={e =>
                  onHeaderLayout(e.nativeEvent.layout.width, index)
                }
                onPress={() => onPressHeader(index)}
                key={item}
                style={[
                  styles.headerItem,
                  {backgroundColor: active == index ? 'white' : '#fafafa'},
                ]}>
                <Poppins size={12} type="Medium">
                  {item}
                </Poppins>
              </TouchableOpacity>
            </View>
          )}
        />

        <Animated.View
          style={[
            styles.headerBar,
            {
              width: headerWidths[active],
              transform: [
                {translateX: barTranslate},
                {translateX: barTranslate1},
              ],
            },
          ]}
        />
      </View>
      <FlatList
        data={headers}
        ref={itemScrollView}
        keyExtractor={item => item}
        horizontal
        pagingEnabled
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        onScroll={onScroll}
        viewabilityConfig={{viewAreaCoveragePercentThreshold: 50}}
        renderItem={Screen}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // width: '100%',
  },
  containerScroll: {
    paddingHorizontal: wp(6),
    paddingTop: hp(2),
    paddingBottom: hp(30),
  },
  headerScroll: {
    flexGrow: 0,
  },
  headerItem: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  mainItem: {
    width: width,
    height: height,
    borderWidth: 5,
    borderColor: '#fff',
  },
  headerBar: {
    height: 3,
    backgroundColor: 'rgb(255, 45, 85)',
    position: 'absolute',
    bottom: 1,
  },
});
