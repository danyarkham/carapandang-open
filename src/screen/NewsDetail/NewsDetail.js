import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FocusAwareStatusBar from '../../component/FocusAwareStatusBar';
import Header from '../../component/Header';

export default function NewsDetail({navigation}) {
  return (
    <View style={styles.container}>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
      <Header
        title="News Detail"
        back
        backPress={e => navigation.goBack()}
        backgroundColor="white"
      />
      <Text>News Detail</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
