import React from 'react';
import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Flag from '../../component/Flag';
import Header from '../../component/Header';
import Poppins from '../../component/Poppins';
import ArrowRight from '../../assets/Icon/arrowRightGrey.svg';
import LogoutIcon from '../../assets/Icon/signout.svg';
import {moderateScale} from 'react-native-size-matters';
import FocusAwareStatusBar from '../../component/FocusAwareStatusBar';
import {navigate} from '../../config/Function/Navigate';

export default function Profile({navigation}) {
  console.log(navigation, 'Navigation Profile');
  const MenuContainer = ({title = 'Menu Title', children}) => (
    <>
      <Poppins type="Medium">{title}</Poppins>
      <View style={{backgroundColor: 'white', marginVertical: hp(2)}}>
        <View style={{paddingVertical: hp(2), paddingHorizontal: wp(4)}}>
          {children}
        </View>
      </View>
    </>
  );

  const MenuItem = ({title = 'Menu Item', onPress}) => (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: hp(2),
      }}>
      <Poppins size={12}>{title}</Poppins>
      <ArrowRight />
    </TouchableOpacity>
  );

  return (
    <>
      <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
      <Header title="Profile" backgroundColor="white">
        <View style={{paddingHorizontal: wp(6), paddingVertical: hp(2)}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Flag
              size={50}
              url={
                'https://www.seekpng.com/png/full/356-3562377_personal-user.png'
              }
              style={{marginRight: wp(4)}}
            />
            <View>
              <Poppins>Carapandang User 1</Poppins>
              <Poppins size={12}>user1@mail.com</Poppins>
            </View>
            <TouchableOpacity
              style={{
                position: 'absolute',
                right: 0,
                width: wp(12),
                height: wp(12),
                borderRadius: wp(6),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <LogoutIcon />
            </TouchableOpacity>
          </View>
        </View>
      </Header>
      <ScrollView
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <MenuContainer title="Account">
            <MenuItem
              title="Edit Account"
              onPress={e => navigation.navigate('NewsDetail')}
            />
            <MenuItem title="Change Password" />
            <MenuItem title="Language" />
          </MenuContainer>
          <MenuContainer title="Account">
            <MenuItem title="Edit Account" />
            <MenuItem title="Change Password" />
            <MenuItem title="Language" />
          </MenuContainer>
          <MenuContainer title="Account">
            <MenuItem title="Edit Account" />
            <MenuItem title="Change Password" />
            <MenuItem title="Language" />
          </MenuContainer>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: wp(6),
  },
  containerScroll: {
    paddingVertical: hp(2),
  },
});
