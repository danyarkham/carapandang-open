import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {NewsCard} from '../../component/CardReusable';
import FocusAwareStatusBar from '../../component/FocusAwareStatusBar';
import Header from '../../component/Header';

export default function Bookmark({navigation}) {
  return (
    <View style={styles.container}>
      <Header title="Bookmark" backgroundColor="white" />
      <ScrollView
        contentContainerStyle={styles.containerScroll}
        showsVerticalScrollIndicator={false}>
        <FocusAwareStatusBar barStyle="dark-content" backgroundColor="white" />
        <NewsCard onPress={() => navigation.navigate('NewsDetail')} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerScroll: {
    paddingHorizontal: wp(6),
    paddingVertical: hp(2),
  },
});
